var mode = 'live';
var baseURL = 'https://www.chp-kinesis.com/';
var webServicePath = 'chp.ws.mobile';
function setServiceMode(_mode){
	switch(_mode){
		case 'dev':
			mode = 'dev';
			baseURL = 'https://sandbox.chp-kinesis.com/';
			webServicePath = 'chp.ws.mobile';		
			break;
		case 'test':
			mode = 'test';
			baseURL = 'https://sandbox.chp-kinesis.com/';
			webServicePath = 'chp.ws.mobile.test';		
			break;
		case 'live':
			mode = 'live';
			baseURL = 'https://www.chp-kinesis.com/';
			webServicePath = 'chp.ws.mobile';		
			break;
	}
}
setServiceMode('test');
var UserID = 3;
var UserSessionID = 'TonySessionTest';
var PatientID = 4;
function doProxyRequest(_ProxyRequest){
	$.ajax({      
		dataType: 'text',      
		url: "http://localhost/Proxy/Proxy.php",      
		cache: false,      
		type: 'GET',      
		async: true,      
		data: 'json='+JSON.stringify(_ProxyRequest),      
		timeOut: 1,      
		error: function(jqXHR, textStatus, errorThrown){         
			//XHR_errorHandler(jqXHR, textStatus, errorThrown);      
	    },      
		success: function(data, textStatus, jqXHR){          
			console.log("success: "+textStatus+", data: "+data);
		}
	});
}
function getUserSettings(){
	var ProxyRequest = {
	  method: 'GET',
	  userAgent: 'Mozilla/5.0 (iPhone; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/75',
	  params: ""
	};
	ProxyRequest.url = baseURL+webServicePath+"/services/security/getUserSettings/userid/"+UserID+"/usersessionid/"+UserSessionID+"/format/json";
	doProxyRequest(ProxyRequest);
}
function savePatientPhotoFormPost(){
	var ProxyRequest = {
	  method: 'POST',
	  userAgent: 'Mozilla/5.0 (iPhone; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/75',
	  params: {
		userid : UserID,
		usersessionid : UserSessionID,
		patientid : PatientID,
		filename : "nothing",
		filesize : _imageBlob.length,
		image : _imageBlob
	  }
	};
	ProxyRequest.url = baseURL + webServicePath + '/services/Document.svc/savePatientPhotoFormPost';
	doProxyRequest(ProxyRequest);
}





