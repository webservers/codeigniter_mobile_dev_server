

var app = {
	//These are really here to document upload Parameters
	stores: {
		_Log: {
			session_id: null, //this shouldn't be set as a post parameter
			message: null,
			data: null
		},
		_Access: {
			ipAddress: null, //$_SERVER['REMOTE_ADDR']; //"192.168.1.1";
			userAgent: null, //$_SERVER['HTTP_USER_AGENT']; //"Mozilla";
			requestMethod: null //$_SERVER['REQUEST_METHOD']; //"GET";
		},
		_Pagination: {
			start: 0,
			total_rows: 0,
			per_page: 0
			
		},
		_File: {
			file_name: ""
		}
	},
	controllers: {
		base: {}
	},
	run_tests: function(){		
		for (key in app.controllers){
			//loop testing each controller
			console.log("controller name: "+key);
			for(_key in app.controllers[key]){
				//loop through the methods
				console.log("method name: "+_key+" value: "+app.controllers[key]);
				//app.controllers[key][_key]();
			}
		}
	},
	widget: {}
};

    $(function(){
    	//JSONeditor.treeBuilder.path = AssetDir+'JSONeditor2/treeBuilderImages/'; //tried to override but it doesn't seem to work???
    	//$('#UploadSubit').button();
    	//$('#AddFiles').button();
    	//$('#UploadBlock').show();
    	app.widget.getSessions();
    	app.widget.createMobileAppView();
    });