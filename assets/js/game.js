/*

	code to process historical game data

*/

//var game = {"bets": [[{"ratio": 2, "value": 1, "affected": [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 9, "spinCount": 22}], [{"ratio": 2, "value": 2, "affected": [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 35, "spinCount": 23}], [{"ratio": 3, "value": 0.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 15, "spinCount": 33}, {"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 15, "spinCount": 33}], [{"ratio": 2, "value": 1, "affected": [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35], "numberHit": 0, "spinCount": 42}], [{"ratio": 2, "value": 2, "affected": [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35], "numberHit": 17, "spinCount": 43}, {"ratio": 2, "value": 1, "affected": [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36], "numberHit": 17, "spinCount": 43}], [{"ratio": 2, "value": 2, "affected": [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36], "numberHit": 34, "spinCount": 44}], [{"ratio": 3, "value": 0.5, "affected": [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36], "numberHit": 33, "spinCount": 52}, {"ratio": 3, "value": 0.5, "affected": [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35], "numberHit": 33, "spinCount": 52}], [{"ratio": 3, "value": 0.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 11, "spinCount": 53}, {"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 11, "spinCount": 53}], [{"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 30, "spinCount": 54}, {"ratio": 3, "value": 0.5, "affected": [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 30, "spinCount": 54}], [{"ratio": 3, "value": 0.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 15, "spinCount": 55}, {"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 15, "spinCount": 55}], [{"ratio": 3, "value": 0.5, "affected": [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 4, "spinCount": 66}], [{"ratio": 2, "value": 1, "affected": [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 13, "spinCount": 67}], [{"ratio": 2, "value": 2, "affected": [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 24, "spinCount": 68}], [{"ratio": 3, "value": 0.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 3, "spinCount": 76}, {"ratio": 3, "value": 0.5, "affected": [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 3, "spinCount": 76}], [{"ratio": 3, "value": 0.5, "affected": [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35], "numberHit": 26, "spinCount": 77}, {"ratio": 3, "value": 0.5, "affected": [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34], "numberHit": 26, "spinCount": 77}], [{"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 2, "spinCount": 80}, {"ratio": 3, "value": 0.5, "affected": [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 2, "spinCount": 80}], [{"ratio": 3, "value": 1.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 32, "spinCount": 81}, {"ratio": 3, "value": 1.5, "affected": [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36], "numberHit": 32, "spinCount": 81}], [{"ratio": 3, "value": 0.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 30, "spinCount": 83}, {"ratio": 3, "value": 0.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 30, "spinCount": 83}], [{"ratio": 3, "value": 1.5, "affected": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], "numberHit": 2, "spinCount": 84}, {"ratio": 3, "value": 1.5, "affected": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "numberHit": 2, "spinCount": 84}], [{"ratio": 2, "value": 1, "affected": [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35], "numberHit": 1, "spinCount": 86}]], "nums": [23, 10, 0, 25, 14, 22, 33, 30, 0, 27, 21, 37, 9, 20, 8, 1, 8, 9, 17, 14, 0, 9, 35, 20, 7, 12, 28, 31, 37, 35, 25, 26, 15, 1, 36, 8, 10, 20, 22, 4, 37, 0, 17, 34, 9, 22, 4, 17, 23, 31, 34, 33, 11, 30, 15, 6, 34, 14, 23, 1, 18, 13, 9, 1, 37, 4, 13, 24, 31, 21, 8, 35, 35, 18, 19, 3, 26, 29, 4, 2, 32, 32, 30, 2, 32, 1, 16, 35, 18, 17]};
//game = JSON.parse(game);

function getAmountWagered(game){
	var amountWagered = 0;
	for(var i=0; i<game.bets.length; i++){
		for(var j =0; j<game.bets[i].length; j++){
			amountWagered += game.bets[i][j].value;
		}
	}
	return amountWagered;
}

function getTotalProfit(game){
	var profit = 0;
	for(var i=0; i<game.bets.length; i++){
		for(var j =0; j<game.bets[i].length; j++){
			if(game.bets[i][j].affected.indexOf(game.bets[i][j].numberHit) != -1)
				profit += (game.bets[i][j].value * game.bets[i][j].ratio) - game.bets[i][j].value;
			else
				profit -= game.bets[i][j].value;
		} 
	}
	return profit;
}


//made to calc from game data
function printGameData(){
	app.controllers.game.getGames(function(o){ 
	    console.log(o);
		var id = 0;
		var aw = 0;
		var tp = 0;
		for(i=0; i<o.data.length; i++){
			try{
				o.data[i].data = JSON.parse(o.data[i].data);
				id = o.data[i].id;
				aw = getAmountWagered(o.data[i].data);
				tp = getTotalProfit(o.data[i].data);
				console.log("id: "+id+" amountWagered: "+parseFloat(aw.toPrecision(2))+" totalProfit: "+parseFloat(tp.toPrecision(2)));
			} catch(e){
				console.log(e);
				console.log(o.data[i]);
			}
		}
	});
}

function totalWinLoss(){
	app.controllers.game.getGames(function(o){ 
	    console.log(o);
		var total = 0;
		for(i=0; i<o.data.length; i++){
			var win_loss = o.data[i].win_loss/1;
			total += win_loss;
		}
		console.log(total);
	});	
}