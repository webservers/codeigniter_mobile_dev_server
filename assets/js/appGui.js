function fromCartesian(x,y,w,h){
	return {x: (w/2)+x, y: (h/2)-y};
}
function positionCenter(el){
	//var w = el.style.width;
	var h = el.style.height;
	var sw = window.srceen.availWidth;
	var sh = window.screen.availHeight;
	el.style.top = (sh/2)-(h/2);
	el.style.left  (sw/2) - (sw/2);
	el.style.position = 'absolute';
}
function hide(el){
	el.style.desplay = 'none';
}
function show(el){
	el.style.display = 'block';
}
function move(el, top, left){
	el.style.top = top;
	el.style.left = left;
}

function rotateElement(el, degrees) {
	var elem = document.getElementById(el);
	//alert(navigator.userAgent);
	if (navigator.userAgent.match("Chrome")) {
		//elem.style.WebKitTransform = "rotate(" + degrees + "deg)";
		elem.style.transform = "rotate(" + degrees + "deg)";
	} else if (navigator.userAgent.match("Firefox")) {
		elem.style.MozTransform = "rotate(" + degrees + "deg)";
	} else if (navigator.userAgent.match("MSIE")) {
		elem.style.msTransform = "rotate(" + degrees + "deg)";
	} else if (navigator.userAgent.match("Opera")) {
		elem.style.OTransform = "rotate(" + degrees + "deg)";
	} else {
		elem.style.transform = "rotate(" + degrees + "deg)";
	}

	/*degrees++;
	 if(degrees > 359){
	 degrees = 0;
	 }*/
}

//create tag for selecct option
function createOptionTag(txt){
	var option = document.createElement("option");
	option.text = txt;
	return option;
}

function window_resize(){
	//var l = ((device.screen.width/2) - (640/2));
	//var t = ((device.screen.height/2) - (432/2));
	//debug(device.screen+":"+t+":"+l);
	//center popup
	//$("#tab").css("top", t+"px");
	//$("#tab").css("left", l+"px");
	centerStatTable();
}

function centerStatTable(){
	var stat_table = document.getElementById("stat_table");
	stat_table.offsetLeft = (device.screen.width/2) - stat_table.offsetWidth;
}

/*
function updatePaytable(){
	document.getElementById("single").innerHTML = "Single: "+odds.single;
	document.getElementById("split").innerHTML = "Split: "+odds.double;
	document.getElementById("street").innerHTML = "Street: "+odds.triple;
	document.getElementById("corner").innerHTML = "Corner: "+odds.quad;
	document.getElementById("six_line").innerHTML = "Six Line: "+odds.double_triple;
}*/

function paytableListener(mEvent){
/*
 * 		odds: {
			single: 35,
			double: 17,
			quad: 8,
			triple: 11,
			double_triple: 5,
			dozen: 2,
			column: 2,
			black_red: 1,
			odd_even: 1,
			high_low: 1	
		},
 */
	if(mEvent.target.id == 'single'){
		
	}
	if(mEvent.target.id == 'split'){
		
	}
	if(mEvent.target.id == 'street'){
		
	}
	if(mEvent.target.id == 'corner'){
		
	}
	if(mEvent.target.id == 'six_line'){
		
	}	
}

function getColor(n) {
	if (n == 0 | n == 37) {
		return "#0f0";
	} else if (rand_set.groups.red.indexOf(n) != -1) {
		return "#f00";
	} else {
		return "#777";
	}
}

function pushNum(n) {
	var c = getColor(n);
	var n1 = document.createElement("div");
	n1.style.color = c;
	n1.style.width = "12px";
	n1.style.float = "left";
	n1.style.margin = "6px";

	if (n == 37) {
		n1.innerHTML = "00";
	} else {
		n1.innerHTML = n;
	}
	document.getElementById("last10").appendChild(n1);
	var cn = document.getElementById("last10").childNodes;
	if (cn.length > 10) {
		document.getElementById("last10").removeChild(cn[0]);
	}
}

function loadStatTable(){
     //debug("stat table loaded"); 
     add_event("rand_number","click", rand_number);
     add_event("add_number", "click", add_number);
	 add_event("upload_session", "click", function(){
		//debug("upload_session");	
		uploadGameData();
	 });
	 add_event("betBox1_1", "click", function(){
		$("#1_1").toggle();
		chartLossCount([ "red", "black", "even", "odd", "high", "low" ], "chartContainer");		
	 });
	 add_event("betBox2_1", "click", function(){
		$("#2_1").toggle();
		chartLossCount([ "r1", "r2", "r3", "g1_12", "g13_24", "g25_36" ], "chartContainer1");		
	 });
	 add_event("betBox5_1", "click", function(){
		$("#5_1").toggle();
		chartLossCount([ "ds1", "ds2", "ds3", "ds4", "ds5", "ds6", "ds7", "ds8", "ds9", "ds10", "ds11" ], "chartContainer2");		
	 });
	 add_event("betBox8_1", "click", function(){
		$("#8_1").toggle();	
		chartLossCount([ "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12", "c13", "c14", "c15", "c16", "c17", "c18", "c19", "c20", "c21", "c22" ], "chartContainer4");		
	 });	
	 add_event("betBox11_1", "click", function(){
		$("#11_1").toggle();
		chartLossCount([ "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12" ], "chartContainer3");
	 });
	 add_event("betBox35_1", "click", function(){
		$("#35_1").toggle();
	 });
     centerStatTable();
}

//canvas js ------------------------------------------------------------------------
function setChartCountData(data, id){
	var dataPoints = [];
	for (var i = 0; i < data.length; i++) {
		dataPoints.push({
			x: data[i].count,
			y: data[i].units
		});
	}
	var chart = new CanvasJS.Chart(id, {
		animationEnabled: true,
		theme: "light2",
		title: {
			text: "Loss Count Befor Hit"
		},
		axisY: {    
			title: "#Times Hit",
			titleFontSize: 18
		},
		data: [{
			type: "column",
			yValueFormatString: "Hit #,### Times",
			dataPoints: dataPoints
		}]
	});	
	chart.render();  
}

function chartLossCount(keys, id){
	var countTotal = {};

	for(var i=0;i<keys.length;i++){
		var keyOffset = rand_set.keys.indexOf(keys[i]);
		var keyCounts = Object.keys(rand_set.numInRowLoss[keyOffset]); //keys are num losses in row
		for(var j=0;j<keyCounts.length;j++){
			if( typeof(countTotal[keyCounts[j]]) == "undefined" ){
				countTotal[keyCounts[j]] = rand_set.numInRowLoss[keyOffset][keyCounts[j]].length;
			} else {
				countTotal[keyCounts[j]] += rand_set.numInRowLoss[keyOffset][keyCounts[j]].length;
			}
		}
	}
	var data = [];
	for(key in countTotal){
		data.push({
			count: key/1,
			units: countTotal[key]
		});
	}
	if(data.length > 0)
		$("#"+id).show();
	setChartCountData(data, id);	
}

/* stat functions -------------------------------------- */
function rand_number() {
	var r = rand_set.rand();
	// debug(r);
	if (r == 37)
		r = "00";
	document.getElementById("num_input").value = r;
}

function getFreindlyGroupName(name){
	if (name == 'g1_12')
		return 'first 12 group';
	if (name == 'g13_24')
		return 'second 12 group';
	if (name == 'g25_36')
		return 'third 12 group';
	if (name == 'r1')
		return 'top row';
	if (name == 'r2')
		return 'middle row';
	if (name == 'r3')
		return 'bottom row';
	if(name.indexOf("ds") != -1)
		return "double street "+name.substr(2, name.length-1);
	if(name.indexOf("s") != -1)
		return "street "+name.substr(1, name.length-1);
	if(name.indexOf("c") == 0)
		return "corner "+name.substr(1, name.length-1);
	return name;
}

function getShouldProcessRatio(ratio){
	var id = "process"+ratio+"_1";
	try{
		return document.getElementById(id).checked;
	} catch(e){
		debug("error "+e);
	}
}

function sayProbLosses(o){
	for (key in o){
		if (100 - o[key] > probabilityTrigger & o[key] > 0){
			var say_str = getFreindlyGroupName(key) + " "+(100 - o[key]).toPrecision(4) + " win";

			if(typeof(key) == "string"){			
				if( getShouldProcessRatio(getPayoutRatio(rand_set.groups[key])) )
					speak(say_str);
				//else debug("can't speak for "+key);
			} else {
				if( getShouldProcessRatio(35) )
					speak(say_str);				
			}
			debug(say_str);
		}
	}
	debug("place bets");
	//speak("place your bets"); this is annoying!!!
}

function getProbLosses(){
	var rVal = {};
	for (var i = 0; i < rand_set.keys.length; i++) {
		rVal[rand_set.keys[i]] = rand_set.probabilityNextLoss(i) * 100;
	}
	return rVal;
}

function addBetTable(groupName, profit, probStop){
	//if( !getShouldProcessRatio(getPayoutRatio(rand_set.groups[groupName])) ){ debug("can't do tables for "+groupName); return -1; }
	//percentOfBalance //global var holding how much balance to risk per bet
	var tableHtml = "";	
	var numbersAffected = rand_set.groups[groupName].length;
	var payoutRatio = getPayoutRatio(rand_set.groups[groupName]);
//	var tableMax = getTableMaxForMultiple(payoutRatio); //use tableMax to determine bet amount
	var prob = 	(37-numbersAffected)/37;
	if(app.data.table_type == "american")
		prob = (38-numbersAffected)/38;	
	var numLosses = getLossCountAtProb(prob, probStop);
	if(numLosses <= 0) return -1;
	var numSpinsLoss = rand_set.numSpinsLoss(rand_set.keys.indexOf(groupName));
	var lossAmount = 0;
	for(var i=numSpinsLoss; i<numLosses; i++){
		var bet = getBetAmount(profit, lossAmount, payoutRatio, 1);
		if(bet < app.data.chip_values[0]){
			debug("min calculated bet of "+bet+ " is less than table chip minimum. starting bets at min value of "+app.data.chip_values[0]);
			 bet = app.data.chip_values[0]; //has to be at least min chip value
		} else
			bet = roundToMinChip(bet);
		var probLoss = (getProbAtLossCount(prob, i) * 100).toFixed(3);
		tableHtml += "<tr><td>"+i+"</td><td>"+bet.toFixed(2)+"</td><td>"+lossAmount.toFixed(2)+"</td><td>"+probLoss+"</td></tr>";
		lossAmount += bet;
	}
	tableHtml += "</table></div>";
/*	if(tableMax < lossAmount){
		alert("Table max exceeded "+max+" for bet "+groupName);
	}*/
	var percentReturn = (profit/lossAmount).toFixed(2);
	debug(groupName+" - profit: "+profit.toFixed(2)+" lossTotal: "+lossAmount.toFixed(2)+" %ret: "+percentReturn);
	var html = '<div id="'+groupName+'"><h3>'+groupName+"  %r:"+percentReturn+" profit:"+profit.toFixed(2)+'</h3>';
	html += '<table><tr><th>#loss</th><th>bet</th><th>loss total</th><th>loss prob</th></tr>';
	html += tableHtml;	
	$("#bet_tables").append(html);
	$("#bet_tables").show();
}

function checkBetTableExists(groupName){
	var el =  document.getElementById(groupName);
	if(el != null)
		return true;
	return false;
}

function addTableProbTrigger(o){
	for (key in o){	
		if(typeof(key) == "string"){
			if (100 - o[key] > probabilityTrigger & o[key] > 0){
				var payoutRatio = getPayoutRatio(rand_set.groups[key]);
				if(getShouldProcessRatio(payoutRatio)){	
					//figure out if bank roll is big enough, then add table
					var lossAmount = percentOfBalance * balance;
					var tableMax = getTableMaxForMultiple(payoutRatio); //use tableMax to determine bet amount
					var perct = percentOfBalance;
					if(lossAmount > tableMax){			//was generating tables whos bets were exceeding tablemax
						 perct = tableMax / balance;	//use table max to calc profit where last bet <= tableMax @ probStop 
					}								
					if(typeof(key) == "string" & !checkBetTableExists(key)){
						var profit = betListProfitAtPercentOfBalance(key, perct, probabilityStop);
						if(profit > 0)
							addBetTable(key, profit, probabilityStop);
					} else {
						//debug("balance too low for group bet "+key+" need "+profit);
					}
				} else {
					//debug("not processing for group "+key);
				}
			}	
		}
	}
}

//remove table if group hit
function removeBetTable(numHit){
	var children = $("#bet_tables").children();
	for(var i=0;i<children.length;i++){
		var id = children[i].id;
		if(rand_set.groups[id].indexOf(numHit) != -1)
			$( "#"+id ).remove();		
	}
	if($("#bet_tables").children().length == 0)
		$("#bet_tables").hide();
}

function updateStatGUI(r){
	removeBetTable(r);	
	if (bets.getTotalValue() > 0) {
		//debug("bets.getTotalValue: "+bets.getTotalValue());
		totalAmountWagered += bets.getTotalValue();				//sum of all bets made
		totalAmountLostSinceLastWin -= bets.getTotalValue();
		//debug("totalAmountLostSinceLastWin: "+totalAmountLostSinceLastWin);
		var payout = bets.getPayOut(r);
		if(payout > 0){
			balance += payout;
			totalAmountLostSinceLastWin = payout + totalAmountLostSinceLastWin;
			//debug("totalAmountLostSinceLastWin: "+totalAmountLostSinceLastWin);
			if(totalAmountLostSinceLastWin > 0) //reset to 0 no losses! if positive 
				totalAmountLostSinceLastWin = 0;
		}
		
		//update GUI
		updateBalance();
		updateAmountLostSinceLastWin();
		updatePayout(payout);
		document.getElementById("profitOnWin").innerHTML = "";	
		bets.pushHistory();					   
		bets.save();
		et.save(); et.removeAll();
	}
	
	document.getElementById("numsInSeries").innerHTML = rand_set.numHistory.length;

	// update loss hit chart rand_set.numInRowLoss
	// chart even money loss count
	if(getShouldProcessRatio(1))
		chartLossCount([ "red", "black", "even", "odd", "high", "low" ], "chartContainer");
	if(getShouldProcessRatio(2))
		chartLossCount([ "r1", "r2", "r3", "g1_12", "g13_24", "g25_36" ], "chartContainer1");
	if(getShouldProcessRatio(5))
		chartLossCount([ "ds1", "ds2", "ds3", "ds4", "ds5", "ds6", "ds7", "ds8", "ds9", "ds10", "ds11" ], "chartContainer2");
	if(getShouldProcessRatio(11))
		chartLossCount([ "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12" ], "chartContainer3");
	if(getShouldProcessRatio(8))
		chartLossCount([ "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12", "c13", "c14", "c15", "c16", "c17", "c18", "c19", "c20", "c21", "c22" ], "chartContainer4");	

	// update all stats
	for (var i = 0; i < rand_set.keys.length; i++) {
		// gets stats ...
		var statId = "hitPercent" + rand_set.keys[i];
		var npslhId = "spinsSince" + rand_set.keys[i];
		var probLossId = "probLoss" + rand_set.keys[i];
		var hottnessId = "hottness" + rand_set.keys[i];
		var winlossCount = rand_set.getNumWinLossCurrent(rand_set.keys[i]);
		document.getElementById(probLossId).innerHTML = (rand_set
				.probabilityNextLoss(i) * 100).toPrecision(3);
		document.getElementById(statId).innerHTML = (rand_set
				.getStat(rand_set.keys[i]) * 100).toPrecision(3);

		if (typeof (rand_set.keys[i]) == "string")
			document.getElementById(hottnessId).innerHTML = rand_set.numSpinsTillHit(i);
		document.getElementById(npslhId).innerHTML = winlossCount;
		// was num losses
		// rand_set.numSpinsSinceHit(rand_set.keys[i]).toPrecision(3);

		var numHitsId = "numHits" + rand_set.keys[i];
		var el = document.getElementById(numHitsId);
		if (el != null)
			el.innerHTML = rand_set.offsets[i].length; // = #hits

		document.getElementById("sd").innerHTML = rand_set.standard_deviation.sd;
		document.getElementById("mean").innerHTML = rand_set.standard_deviation.mean;
	}
	sentences = []; //erase previous speech quene
	var lossArray = getProbLosses();
	addTableProbTrigger(lossArray);
	sayProbLosses(lossArray);	
}

// add number that hit and update all stats
function add_number(num) {
	// debug("add_number");
	if (document.getElementById("num_input").value == "" & typeof(num) == 'undefined') {
		alert("No Number Input");
		return;
	}
	var r = -1;
	if (typeof(num) != 'number'){
		var r = document.getElementById("num_input").value;
		if (r == "00")
			r = 37;
		else
			r = r / 1;
		if (typeof (r) != 'number' | r < rand_set.min | r > rand_set.max) {
			alert("Invalid Input");
			return;
		}
	} else {
		r = num/1;
	}
	pushNum(r);
	rand_set.generateNext(r); // add the number to rand_set
	updateStatGUI(r);
}

/* session functions -------------------------------------------- */
function load_session(mEvent) {
	// var index = document.getElementById("session_select").selectedIndex;
	var name = document.getElementById("session_select").value;
	var sessions = localStorage.getItem("sessions");
	if (sessions != null) {
		sessions = JSON.parse(sessions);
		if (typeof (sessions[name]) == 'object') {
			for (var i = 0; i < sessions[name].length; i++) {

			}
		}
	}
}

function addSessionOption(name) {
	document.getElementById("session_select").add(createOptionTag(name));
}

function list_sessions() {
	/*
	 * saving to localStorage
	 * 
	 * localStorage.sessions is a key store for rand_set.numHistory arrays.
	 * 
	 * the contents are computed in a for loop to get to current values.
	 */
	try {
		var sessions = localStorage.getItem("sessions");
		if (sessions != null) {
			sessions = JSON.parse(sessions);
			for (keys in sessions) {
				addSessionOption(key);
			}
		}
	} catch (e) {
		alert(e);
	}
}

function save_session() {
	var name = document.getElementById("save_session_name").value;
	if (name != "") {
		var sessions = localStorage.getItem("sessions");
		sessions[name] = rand_set.numHistory;
		localStorage.setItem("sessions", sessions);
	}
}
