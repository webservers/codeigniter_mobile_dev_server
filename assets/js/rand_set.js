/*
"[9,20,8,35,28,23,16,7,2,25,29,5,11,31,13,20,27,33,17,20,1,12,35,8,18,15,34,36,33,22,29,34,31,24,11,14,0,6,0,23,4,5,2,8,27,1,16,16,32,26,10,34,3,7,29,11,14,23,12,7,36,25,18,36,31,18,11,17,25,1,11,20,4,7,26,25,34,33,16,17,8,6,31,1,9,22,11,33,24,1,24,22,19,33,33,3,36,1,33,23,3,34,23,29,29,36,1,10,26,15,10,30,34,2,36,33,17,22,5,0,8,27,8,9,5,0]"
netint european roulette

"[36,15,31,21,11,36,8,2,8,24,17,36,7,5,0,10,15,26,18,19,23,11,13,10,35,35,33,8,36,33,14,22,4,16,18,20,31,32,26,37,21,37,3,11,20,2,33,30,11,6,8,36,2,23,35,27,22,14,25,35,22,34,11,18,18,15,2,5,13,9,36,10,21,25,11,25,9,27,7,36,13,21,20,20,6,10,21,8,27,4,19,19,20,18,22,6,14,5,4,22,34,20,19,31,8,5,29,31,1,9,6,1,23,35,14,15,25,36,34,16,29,3,5,20,16,9,20,16,1,33,0,8,29,9,14,17,10,9,6,29,32,6,20,15,12,19,2,30,24,32,32,21,14,5,1,35,36,16,3,27,1,4,32,33,16,5,34,9,21,6,33,12,3]"
roulette american walmart game
 */
 if(typeof(debug) == "undefined"){
   function debug(s){
 	    console.log(s);
   }
 }

Array.prototype.sum = function(start, end){
	var _start = 0; var _end = this.length; var sum = 0;
	if(typeof(start)!='undefined') _start = start;
	if(typeof(end)!='undefined') _end = end;
	for(;_start < _end; _start++){
		sum += this[_start];
	}
	return sum;
};
Array.prototype.offset_Diffs = function(){
	var a = this[0];
	for(var i=1;i<this.length;i++){
		a.push(this[i] - this[i-1]+1);
	}
	return a;
};
Array.prototype.lengths = function(){
	var lengths = [];
	for(var i=0;i<this.length;i++){
		lengths.push(this[i].length);
	}
	return lengths;
};
Array.prototype.allIndexOf = function(n){
	var offset = this.indexOf(n);
	while(offset.last() != -1){
		offset.push(this.indexOf(n, offset.last()+1));
	}
	offset.pop();
	return offset;
};
Array.prototype.assoc = function(){
	var keys = this[0]; var offsets = this.allIndexOf(this[0]);
	for(var i=0;i<this.length;i++){
		if(keys.indexOf(this[i]) == -1)
			keys.push(this[i]);
		offsets.push(this.allIndexOf(this[i]));
	}
	return { keys: keys, offsets:offsets };
};
Array.prototype.last = function(){
	if(this.length > 0) return this[this.length-1];
	return 0;
};
Array.prototype.popIndex = function(i){
	return this.splice(i, 1);
};
Array.prototype.next = function(){
	if(this.length == 0) return -1;
	if(typeOf(this.i) == 'undefined'){
		this.i = 0; return this[0];
	} else {
		this.i++;
		if(this.i > this.length-1) this.i=0;
		return this[this.i];
	}
};

function getBaseLog(base, x){
	//if(typeof(math.log) == "function") return math.log(x, base);
	return Math.log(base) / Math.log(x);
}

//returns loss count for given fraction at desired probability
function getLossCountAtProb(fract, prob){
	return Math.round(getBaseLog(prob, fract));
}

function getProbAtLossCount(fract, lossCount){
	return Math.pow(fract, lossCount+1);
}

function standard_deviation(a, n, end){
	if(typeof(a) != 'object') return -1;
	if(typeof(n) != 'number') n = a.length;
	var start = 0;	
	if(typeof(end) == 'number' & (end-n) < 0){
		return -1;
	} else {
		end = a.length;
	}
	start = end-n;	
	// debug("start: "+start+" end: "+end);
	var _a = a.slice(start, end);
	// debug(_a);
	var mean = _a.sum() / _a.length;
	// debug("mean: "+mean);
	var sum = 0;
	for(var i=0;i<_a.length;i++){
		sum += Math.pow((_a[i] - mean), 2);
	}
	return {mean: mean, sd:Math.sqrt( sum / (n-1))};
}

function wheel_number_order(){
	if(app.data.table_type == "american")
		return [0, 28, 9, 26, 30, 11, 7, 20, 32, 17, 5, 22, 34, 15, 3, 24, 36, 13, 1, 37, 27, 10, 25, 29, 12, 8, 19, 31, 18, 6, 21, 33, 16, 4, 23, 35, 14, 2];
	else
		return [0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26];
}

//get distance between numbers
function getNumberDistance(a, b, clockwise){
	var numOrder = wheel_number_order();
	if(clockwise){
		//if(app.data.table_type == "american"){
			var indexA = numOrder.indexOf(a);
			var indexB = numOrder.indexOf(b);
			if(indexA > indexB)
				return indexA - indexB; 
		//}
	}
}

var rand_set = {
	  checkHottness: true,
	  checkStats: true,
	  checkGroups: true,
	  checkNumInRowWin: true,
	  checkNumInRowLoss: true,
	  init: function(o){
	 
		if(typeof(o.max) == "undefined"){
			alert("rand_set.init max is undefined");
			alert(arguments.callee.caller);
			return;
		}
	  
		rand_set.keys = [];
		rand_set.offsets = []; // [offset_end ...]
		rand_set.numsHit = [];
		rand_set.min = 0;   // min number in set
		rand_set.max = 0;   // max number in set
		rand_set.numsTotal = 0; // how many numbers
		rand_set.numHistory = []; // rand numbers that came in
		rand_set.statPeriod = 38; // period used for stastics
		rand_set.stats = {};
		rand_set.spinCounts = [];  // this arry is needed to calc sd of spaces
									// between numbers
		

		rand_set.standard_deviation = {
		  sd: 0, 
		  mean:0
		};
		
		rand_set.countObjWin = { 
		  numHit: 0, 
		  numTimes: 0
		}; // tracks win numInRow
		
		rand_set.numInRowWin = [];
		rand_set.numInRowLoss = [];
		
		for(key in o){ // override default settings
			rand_set[key] = o[key]; //this instantiates groups and other overridable vars
		}
		rand_set.numsTotal = rand_set.max - rand_set.min;
		if(rand_set.min == 0) rand_set.numsTotal++;
		if(rand_set.numsTotal <= 0 | typeof(rand_set.numsTotal) != "number"){
		  debug("rand_set numstotal error: "+rand_set.numsTotal);
		  return -1; // error condition
		}
		// debug("init-rand_set.numsTotal:" +rand_set.numsTotal);
		
		for(var i=rand_set.min; i<=rand_set.max; i++){
			rand_set.keys.push(i);
			rand_set.offsets.push([]);
			rand_set.numInRowWin.push({});
			rand_set.numInRowLoss.push({});
		}
		
		if(typeof(rand_set.groups) == 'object'){
			rand_set.groupHottness = {};
			for(groupName in rand_set.groups){
			  rand_set.keys.push(groupName);
			  rand_set.offsets.push([]);
			  rand_set.numInRowWin.push({});
		      rand_set.numInRowLoss.push({});
		      rand_set.groupHottness[groupName] = 0;
			}
		}
		
		if(rand_set.checkStats){ // init stats
			rand_set.stats.avgs = {};
			for(var i =0;i<rand_set.keys.length;i++){
				rand_set.stats.avgs[rand_set.keys[i]] = [];
			}
		}		
		return rand_set;
	},
	rand: function(){
		var r = Math.floor( Math.random() / (1/rand_set.numsTotal));
		// debug("rand: "+r+" numsTotal: "+rand_set.numsTotal);
		return r;
	},
	numSpinsWin: function(keyOffset){
		var key = rand_set.keys[keyOffset];
		if(keyOffset >= 37){
			return rand_set.countObjWin[key];
		}
		if(rand_set.countObjWin.numHit == key){
			return rand_set.countObjWin.numTimes + 1;
		}
		return 0;
	},
	numSpinsLoss: function(keyOffset){ // return nums spins loss current for
										// key
		var npslh = rand_set.numSpinsSinceHit(rand_set.keys[keyOffset]);
		var numSpinsLoss = rand_set.numHistory.length;
		if(npslh != -1) numSpinsLoss = npslh; // -1 = never hit
		//debug("npslh: "+npslh+" numSpinsLoss: "+numSpinsLoss);
		return numSpinsLoss;
	},
	probabilityNextWin: function(i){
	  // calculate winning streak
		var numsInvolved = rand_set.numsInvolved(i);
		
		var numInRowWin = 0;
		if(numsInvolved > 1){
		  // group
			numInRowWin = rand_set.countObjWin[groupName];
		} else if(rand_set.countObjWin.numHit == rand_set.keys[i]){
			numInRowWin = rand_set.countObjWin.numTimes;
		}
		var probWin = 0;
		if(app.data.table_type == "american"){ // totalNums-numsInvolved/totalNums =
										// prob for 1 loss ^n = prob for n
										// losses
			probWin = Math.pow(numsInvolved/38, numInRowWin + 1);
		} else {
			probWin = Math.pow(numsInvolved/37, numInRowWin + 1);
		}
		return probWin;
	},
	probabilityNextLoss: function(keyOffset){
	  // calculate loosing streak
		var numSpinsLoss = rand_set.numSpinsLoss(keyOffset);
		var numsInvolved = rand_set.numsInvolved(keyOffset);
		//debug(" key: "+keyOffset+" probabilityNextLoss:numSpinsLoss: "+numSpinsLoss+" numsInvolved"+numsInvolved);
		var probLoss = 0;
		if(numSpinsLoss == 0){
			var numSpinsWin = rand_set.numSpinsWin(keyOffset);
			//debug("numSpinsWin: "+numSpinsWin);
			if(app.data.table_type == "american"){
				probLoss = 1 - Math.pow(numsInvolved/38, numSpinsWin);
			} else {
				probLoss = 1 - Math.pow(numsInvolved/37, numSpinsWin);
			}			
		} else {
			if(app.data.table_type == "american"){ // totalNums-numsInvolved/totalNums =
											// prob for 1 loss ^n = prob for n
											// losses
				probLoss = Math.pow((38 - numsInvolved)/38, numSpinsLoss + 1);
			} else {
				probLoss = Math.pow((37 - numsInvolved)/37, numSpinsLoss + 1);
			}
		}
		return probLoss;
	},
	numsInvolved: function(i){
/*		var numsInvolved = 1;
		if(typeof(rand_set.keys[i]) == "string"){		
			if((rand_set.keys[i] == "red") | (rand_set.keys[i] == "black") |(rand_set.keys[i] == "even") |(rand_set.keys[i] == "odd") |(rand_set.keys[i] == "high") |(rand_set.keys[i] == "low")){
				numsInvolved = 18;
			} else if((rand_set.keys[i] == "r1") | (rand_set.keys[i] == "r2") |(rand_set.keys[i] == "r3") | (rand_set.keys[i] == "g1_12") | (rand_set.keys[i] == "g13_24") | (rand_set.keys[i] == "g25_36")){
				numsInvolved = 12;
			} else if(rand_set.keys[i].indexOf("ds") != -1){
				numsInvolved = 6;
			} else if(rand_set.keys[i].indexOf("s") != -1){
				numsInvolved = 3;
			} else if(rand_set.keys[i].indexOf("c") != -1){
				numsInvolved = 4;
			}		
		}
		return numsInvolved;*/
		if(typeof(rand_set.keys[i]) == "string"){
			return rand_set.groups[rand_set.keys[i]].length; //now new keys added will automatically work
		}
		return 1;
	},
	numSpinsTillHit: function(i){ // return num spins till prob loss < .1%
		var npslh = rand_set.numSpinsSinceHit(rand_set.keys[i]);
		var numSpinsLoss = rand_set.numsHit.length;
		if(npslh != -1) numSpinsLoss = npslh; // -1 = never hit
		var numsInvolved = rand_set.numsInvolved(i);
		
		var point1percent = 0;
		if(numsInvolved == 18){
			if(app.data.table_type == "american"){
				point1percent = 11;
			} else {
				point1percent = 11;
			}			
		} else if(numsInvolved == 12){
			if(app.data.table_type == "american"){
				point1percent = 19;
			} else {
				point1percent = 18;
			}			
		} else if(numsInvolved == 6){
			if(app.data.table_type == "american"){
				point1percent = 41;
			} else {
				point1percent = 40;
			}			
		} else if(numsInvolved == 3){
			if(app.data.table_type == "american"){
				point1percent = 84;
			} else {
				point1percent = 82;
			}			
		} else if(numsInvolved == 4){
			if(app.data.table_type == "american"){
				point1percent = 62;
			} else {
				point1percent = 60;
			}			
		}		
				
		if(rand_set.keys[i] == "g0_00"){
			if(app.data.table_type == "american"){
				point1percent = 128;
			} else {
				point1percent = 253;
			}
		}

		return point1percent - numSpinsLoss;
	},
	numsHitLength: function(){
		return rand_set.numsHit.length;
	},
	numInRowLossCheck: function(r){
	  // only update loss counts for numbers
	  
	  var keyOffset = rand_set.keys.indexOf(r);
	  var lastHitOffset = 0;
	  if(rand_set.offsets[keyOffset].length >= 1)
		  lastHitOffset = rand_set.offsets[keyOffset][rand_set.offsets[keyOffset].length-1];
	  var numsHitLastOffset = rand_set.numHistory.length - 1;
	  var lossCount = numsHitLastOffset - lastHitOffset;
	  if( (lastHitOffset == numsHitLastOffset) & lossCount >= 2){
	    // num just hit, is it 2 or more in a row?
	    if(countObjWin.numsHit == r & countObjWin.numTimes == 1){
	      // it just won, and is counting wins
	      if( numInRowLoss[keyOffset][lossCount] == "undefined" ){
	    	  numInRowLoss[keyOffset][lossCount] = {};
	      }
          numInRowLoss[keyOffset][lossCount].push(numsHitLastOffset);
	    }
	  }
	  
	},
	numInRowWinCheck: function(r){
		if(rand_set.numsHit.indexOf(r) != -1){ // num already hit
			if(r == rand_set.countObjWin.numHit){ 
				rand_set.countObjWin.numTimes++; 
			} else if( rand_set.countObjWin.numTimes > 0){
				var offset = rand_set.keys.indexOf(r);
				if(rand_set.countObjWin.numTimes >= 2){
					if(typeof(rand_set.numInRowWin[offset][rand_set.countObjWin.numTimes]) == 'undefined') rand_set.numInRowWin[offset][rand_set.countObjWin.numTimes] = [rand_set.numHistory.length - 1];
					else rand_set.numInRowWin[offset][rand_set.countObjWin.numTimes].push(rand_set.numHistory.length - 1);
				}				
				rand_set.countObjWin.numHit = r;
				rand_set.countObjWin.numTimes = 0;
			} else {
				rand_set.countObjWin.numHit = r;
				rand_set.countObjWin.numTimes = 0;				
			}
		}
	},
	getNumWinLossCurrent: function(key){
		// wins are counted by countobjWin
		// losses are determined by offsets array
		var keyOffset = rand_set.keys.indexOf(key);
		var lossCount = rand_set.numSpinsLoss(keyOffset);
		  
		if(typeof(key) == "number"){
			if(rand_set.countObjWin.numHit == key){
				return rand_set.countObjWin.numTimes+1;
			} else {
				return lossCount * -1;
			}
		} else {
			if(rand_set.countObjWin[key] > 0){
				return rand_set.countObjWin[key];
			} else{
				return lossCount * -1;
			}
		}
	},
	generateNext: function(r){
		// if(typeof(r) == 'number' & r >= rand_set.min & r <= rand_set.max) r = rand_set.rand();
		rand_set.numHistory.push(r);
		var offset = rand_set.keys.indexOf(r);
		rand_set.offsets[offset].push(rand_set.numHistory.length-1);
		if(rand_set.offsets[offset].length > 1){
			// ... get spincount and push to spin counts array, then do standard
			// deviation of statPeriod
			var spinCount = rand_set.offsets[offset][rand_set.offsets[offset].length-1] - rand_set.offsets[offset][rand_set.offsets[offset].length-2];
			rand_set.spinCounts.push(spinCount); // this arry is needed to
													// calc sd of space
			rand_set.standard_deviation = standard_deviation(rand_set.spinCounts);
			// debug("sd: "+sd.sd+" mean: "+sd.mean);
		}
		
		if(rand_set.numsHit.indexOf(r) == -1){  // num r didn't hit
			rand_set.numsHit.push(r);
		}
	    if(rand_set.checkNumInRowWin) 
	      rand_set.numInRowWinCheck(r);
		if(rand_set.checkNumInRowLoss) 
		  rand_set.numInRowLossCheck(r);
		if(rand_set.groupsCheck)
		  rand_set.groupsCheck(r);
		if(rand_set.checkStats)
		  rand_set.statsCheck(r);
		return r;
	},
	generateTrial: function(callBack){
		while(rand_set.numsHit.length < rand_set.numsTotal){
			var r = rand_set.rand();
			rand_set.generateNext(r);
			if(typeof(callBack) == 'function'){
				callBack(r);
			}
		}
	},
	groupsCheckStats: function(r){
	  // its built into checkStats
	  // if group keys exist in keys array
	  // its processed the same
	},
	groupsCheckNumInRowLoss: function(r, groupName, keyOffset){
		  var keyOffset = rand_set.keys.indexOf(groupName);
/*		  var lastHitOffset = rand_set.offsets[keyOffset][rand_set.offsets[keyOffset].length-1];
		  if(typeof(lastHitOffset) == "undefined") lastHitOffset = -1;

		  var lossCount = rand_set.numSpinsLoss(keyOffset);*/
		  var lastOffset = rand_set.numHistory.length-1;
		  var lossCount = 0;

		  if(rand_set.offsets[keyOffset].length >= 1){
			  var lastHitOffset = rand_set.offsets[keyOffset][rand_set.offsets[keyOffset].length-1];
			  lossCount = lastHitOffset;
			  if(rand_set.offsets[keyOffset].length >= 2){			 
				  var previousHitOffset = rand_set.offsets[keyOffset][rand_set.offsets[keyOffset].length-2];
				  lossCount = (lastHitOffset - previousHitOffset)-1;
			  }
			  //debug("groupsCheckNumInRowLoss: "+groupName+" lossCount: "+lossCount);
			    
			  if( lossCount >= 2 & lastOffset == lastHitOffset){ //need to make sure it ended with a win to record the number of losses it took to get there
			    // loss count shouldnt be > 12 for even money
			      //debug("saving groupName: "+groupName+" losscount: "+lossCount);
			      if( typeof(rand_set.numInRowLoss[keyOffset][lossCount]) == "undefined" ){
			    	  rand_set.numInRowLoss[keyOffset][lossCount] = [];
			      }
			      rand_set.numInRowLoss[keyOffset][lossCount].push(lastHitOffset); // push
																					// offset
																					// end
			  }			  
		  } //else{ lossCount = lastOffset+1; }  
 
	},
	groupsCheckNumInRowWin: function(r, groupName, offset){
		  if(rand_set.groups[groupName].indexOf(r) != -1){ // is part of group
			  if(typeof(rand_set.countObjWin[groupName]) == "undefined")
				  rand_set.countObjWin[groupName] = 0;
			  rand_set.countObjWin[groupName]++;
		  } else { 
		    // not part of group, push offset reset group count
		    if(rand_set.countObjWin[groupName] >= 2){
		        // debug("groupName: "+rand_set.countObjWin[groupName]);
		        var offset = rand_set.keys.indexOf(groupName);
			    if(typeof(rand_set.numInRowWin[offset][rand_set.countObjWin[groupName]]) == 'undefined'){
			        rand_set.numInRowWin[offset][rand_set.countObjWin[groupName]] = [rand_set.numHistory.length - 1]; 
			        // add new key and empty array
		    } else {
		    	rand_set.numInRowWin[offset][rand_set.countObjWin[groupName]].push(rand_set.numHistory.length - 1);
		    }
	      }
		    rand_set.countObjWin[groupName] = 0; // reset count
		  }
	},
	groupsCheckHottness: function(r, groupName, offset){
		// process hotness
		rand_set.groupHottness[groupName] = 0; // reset hottness sum
		
		// get all spincounts for group and avg them.
		for(var i=0; i<rand_set.keys.length;i++){
			// loop through all numbers sum all spincounts in group
			if(typeof(rand_set.keys[i]) == 'number'){
				if((rand_set.groups[groupName].indexOf(rand_set.keys[i]) != -1) & rand_set.numSpinsSinceHit(rand_set.keys[i]) != -1){
					rand_set.groupHottness[groupName] += rand_set.numSpinsSinceHit(rand_set.keys[i]);
				}
			}
		}
		// debug("groupName: "+groupName+" sum: "+rand_set.groupHottness[groupName] + " length: "+rand_set.groups[groupName].length);
		rand_set.groupHottness[groupName] = rand_set.groupHottness[groupName] / rand_set.groups[groupName].length;
		// debug(" hottness: " + rand_set.groupHottness[groupName]);
	},
	groupsCheck: function(r){
	  for(groupName in rand_set.groups){
		  var offset = rand_set.keys.indexOf(groupName);
			
		  // update offsets 1st
		  if(rand_set.groups[groupName].indexOf(r) != -1){ // is part of group
			  rand_set.offsets[offset].push(rand_set.numHistory.length-1);
		  }
		  // if(rand_set.checkStats){ groupsCheckStats(r, groupName, offset); }
	      if(rand_set.checkNumInRowWin){
	    	  rand_set.groupsCheckNumInRowWin(r, groupName, offset);
	      }
	      if(rand_set.checkNumInRowLoss){
	    	  rand_set.groupsCheckNumInRowLoss(r, groupName, offset);
	      }
	      if(rand_set.checkHottness){
	    	  rand_set.groupsCheckHottness(r, groupName, offset);
	      }
	  }
	},
	statsCheck: function(r){ 
		// groupsCheck must be called 1st, otherwise groupStats won't work
		if(rand_set.numsHit.length < rand_set.statPeriod){
			for(var offset=0;offset<rand_set.keys.length;offset++){
				var key = rand_set.keys[offset];
				//debug("key: "+key+" length: "+rand_set.offsets[offset].length+" numsHit.length: "+rand_set.numHistory.length);
				if(rand_set.offsets[offset].length > 0){
					rand_set.stats.avgs[key].push(rand_set.offsets[offset].length / rand_set.numHistory.length);
				} else {
					rand_set.stats.avgs[key].push(0);
				}
			}
		} else {		
			var stopOffset = 0;
			if(rand_set.numHistory.length > rand_set.statPeriod){
				stopOffset = rand_set.numHistory.length - rand_set.statPeriod;
			}
			for(var offset=0;offset<rand_set.keys.length;offset++){
				var key = rand_set.keys[offset];
				var hitCount = 0;
				for(var j=0; j<rand_set.offsets[offset].length;j++){ 
					// loop through offset arrays checking offset against stopOffset
					// debug("stopOffset: "+stopOffset+"rand_set.offsets[offset][j]:"+rand_set.offsets[offset][j]);
					if(rand_set.offsets[offset][j] > stopOffset) hitCount++;
				}
				if(hitCount > 0){
					rand_set.stats.avgs[key].push(hitCount / rand_set.statPeriod);
				} else {
					rand_set.stats.avgs[key].push(0);
				}
			}
		}
	},
	getStat: function(k){
		return rand_set.stats.avgs[k][rand_set.stats.avgs[k].length - 1];
	},
	numSpinsSinceHit: function(k){
		var offset = rand_set.keys.indexOf(k);
		if(rand_set.offsets[offset].length > 0){
			// debug("key: "+k+" #Spins: "+rand_set.numHistory.length+" #lastOffset: "+rand_set.offsets[offset][rand_set.offsets[offset].length-1]);
			return (rand_set.numHistory.length - rand_set.offsets[offset][rand_set.offsets[offset].length-1]) - 1; // #spins - lastOffset
		}
		return -1; // -1 = number never hit
	},
	getData: function(){
		return {
			keys: rand_set.keys,
			offsets: rand_set.offsets,
			statPeriod: rand_set.statPeriod,
			stats: rand_set.stats,
			numHistory: rand_set.numHistory,
			hitCount: rand_set.hitCount,
			min: rand_set.min,
			max: rand_set.max,
			numsTotal: rand_set.numsTotal,
			groups: rand_set.groups,
			numInRowWin: rand_set.numInRowWin,
			numInRowLoss: rand_set.numInRowLoss
		};
	}
};


/*
 * Might need to change stats to numHitInPeriod: { 19: { hitCounts: [], Avg: [],
 * StandardDeviation: [] } }
 */
var rand_dist = {
		trials: [],
		numTrials: 1,
		rand_init: {},
		keys: [],
		numInRowWin:[],
		numInRowLoss: [],
		offsetLengths: [],
		trialLengths: {
			min: 1e16,
			max: -1e-16,
			avg: 0,
			sum: 0
		},
		addTrial: function(){
			try {
				var data = JSON.parse(rand_dist.rand_init);
				rand_dist.trials.push(data);
				rand_set.init(rand_dist.trials[rand_dist.trials.length-1]);
				
				rand_set.generateTrial();
			} catch(e){
				debug("error: "+e);
			}
		},
		generateAll: function(callBack){
			while(rand_dist.trials.length < rand_dist.numTrials){
				rand_dist.addTrial();
				
				// update numbers
				rand_dist.checkTrialLengths();
				rand_dist.checkOffsetLengths();
					
				if(rand_set.checkNumInRowWin)
					rand_dist.numInRowWinCheck();
				if(rand_set.checkNumInRowLoss)
					rand_dist.numInRowLossCheck();
					
				if(typeof(callBack) == 'function'){
					callBack(rand_dist.getData());
				}
			}
			return rand_dist;
		},
		init: function(o){
		  // copy rand_dist vars from o
		  if(typeof(o.numTrials) == "number")
		    rand_dist.numTrials = o.numTrials;
		  if(typeof(o.min) == "number")
		    rand_dist.min = o.min;
		  if(typeof(o.max) == "number")
		    rand_dist.max = o.max;
		  
			rand_dist.numsTotal = rand_dist.max - rand_dist.min;
			if(rand_dist.min == 0) rand_dist.numsTotal++;
			
			// fill keys for data
			rand_dist.keys = [];
			for(var i=rand_dist.min; i<=rand_dist.max; i++){
				rand_dist.keys.push(i);
			}
			if(typeof(o.groups) == 'object'){
				for(key in o.groups){
					rand_dist.keys.push(key);
				}
			}
			
			// addmin max avg objectsfor all keys
			for(var i=0; i<rand_dist.keys.length; i++){
				rand_dist.offsetLengths.push({
						max: -1e-16,
						min: 1e16,
						sum: 0,
						avg: 0
				});
				rand_dist.numInRowWin.push([]);
				rand_dist.numInRowLoss.push([]);
			}
			o.offsets = []; // these are rand_set data vars that will be passed
							// by ref
			o.numInRowWin = [];
			o.numInRowLoss = [];
			o.numsHit = [];
			o.stats = {};
			o.numHistory = [];
			rand_dist.rand_init = JSON.stringify(o);
			return rand_dist;
		},
		checkOffsetLengths: function(){
		  // how many times a number or group appeared isthe offset length
			var t = rand_dist.trials.length-1;
			
			for(var k=0;k<rand_dist.keys.length;k++){
			  if(typeof(rand_dist.trials[t].offsets[k]) != "object"){ 
			    debug("error: rand_dist.trials["+t+"].offsets for key "+k+" is undefined");
			    return -1;
			  }
				if(rand_dist.trials[t].offsets[k].length > rand_dist.offsetLengths[k].max)
					rand_dist.offsetLengths[k].max = rand_dist.trials[t].offsets[k].length;
				if(rand_dist.trials[t].offsets[k].length < rand_dist.offsetLengths[k].min)
					rand_dist.offsetLengths[k].min = rand_dist.trials[t].offsets[k].length;
				rand_dist.offsetLengths[k].sum += rand_dist.trials[t].offsets[k].length;
				if(t > 0)
					rand_dist.offsetLengths[k].avg = rand_dist.offsetLengths[k].sum / rand_dist.trials.length;
			}
		},
		checkTrialLengths: function(){
		  // how many numbers hit after thelastnumber came in
			var t = rand_dist.trials.length-1;
			 // for(var t=0;t<rand_dist.trials.length;t++){
			 	 if(rand_dist.trials[t].numHistory.length > rand_dist.trialLengths.max)
					rand_dist.trialLengths.max = rand_dist.trials[t].numHistory.length;
				if(rand_dist.trials[t].numHistory.length < rand_dist.trialLengths.min)
					rand_dist.trialLengths.min = rand_dist.trials[t].numHistory.length;
				rand_dist.trialLengths.sum += rand_dist.trials[t].numHistory.length;
				if(t > 0)
					rand_dist.trialLengths.avg = rand_dist.trialLengths.sum / rand_dist.trials.length;
			 // }
		},
		numInRowWinCheck: function(){
		  // winning streak counts
			var t = rand_dist.trials.length-1;
			for(var k=0;k<rand_dist.keys.length;k++){
				for(iNIR in rand_dist.trials[t].numInRowWin[k]){
					if(typeof(rand_dist.numInRowWin[k][iNIR]) != 'object'){
						rand_dist.numInRowWin[k][iNIR] = {
								max: -1e-16,
								min: 1e16,
								sum: 0,
								avg: 0
						};
					}// now object exists
					if(rand_dist.trials[t].numInRowWin[k][iNIR].length > rand_dist.numInRowWin[k][iNIR].max)
						rand_dist.numInRowWin[k][iNIR].max = rand_dist.trials[t].numInRowWin[k][iNIR].length;
					if(rand_dist.trials[t].numInRowWin[k][iNIR].length < rand_dist.numInRowWin[k][iNIR].min)
						rand_dist.numInRowWin[k][iNIR].min = rand_dist.trials[t].numInRowWin[k][iNIR].length;
					rand_dist.numInRowWin[k][iNIR].sum += rand_dist.trials[t].numInRowWin[k][iNIR].length;
					rand_dist.numInRowWin[k][iNIR].avg = rand_dist.numInRowWin[k][iNIR].sum / this.trials.length;
				}
			}
		},
		numInRowLossCheck: function(){
		  // loosing streak counts
			var t = rand_dist.trials.length-1;
			for(var k=0;k<rand_dist.keys.length;k++){
				for(iNIR in rand_dist.trials[t].numInRowLoss[k]){// numInRowLoss
																	// is
																	// anarray
																	// of
																	// objests
					if(typeof(rand_dist.numInRowLoss[k][iNIR]) != 'object'){
						rand_dist.numInRowLoss[k][iNIR] = {
								max: -1e-16,
								min: 1e16,
								sum: 0,
								avg: 0
						};
					}// now object exists
					if(rand_dist.trials[t].numInRowLoss[k][iNIR].length > rand_dist.numInRowLoss[k][iNIR].max)
						rand_dist.numInRowLoss[k][iNIR].max = rand_dist.trials[t].numInRowLoss[k][iNIR].length;
					if(rand_dist.trials[t].numInRowLoss[k][iNIR].length < rand_dist.numInRowLoss[k][iNIR].min)
						rand_dist.numInRowLoss[k][iNIR].min = rand_dist.trials[t].numInRowLoss[k][iNIR].length;
					rand_dist.numInRowLoss[k][iNIR].sum += rand_dist.trials[t].numInRowLoss[k][iNIR].length;
					rand_dist.numInRowLoss[k][iNIR].avg = rand_dist.numInRowLoss[k][iNIR].sum / this.trials.length;
				}
			}
		},
		numSpinsSinceLastHit: function(keyOffset){
			if(keyOffset >= rand_dist.keys.length | keyOffset < 0) return -1;
			if(rand_dist.trials.length > 1){
				var t = rand_dist.trials.length-1;
				var lastOffset = rand_dist.trials[t-1].offsets[keyOffset].length-1;
				var first = rand_dist.trials[t].offsets[keyOffset][0];
				var trialLength = rand_dist.trials[t-1].trialLength;
				var spinCount = trialLength-last+1; // #spins tillEnd
				spinCount += first; // +#spins till next
				return spinCount;
			} else {// return offset diff
				var lastOffset = rand_dist.trials[t].offsets[keyOffset].length-1;
				if(lastOffset >= 1){ // this trial has 2 entries
					var previous = rand_dist.trials[t].offsets[keyOffset][lastOffset-1];
					var last = rand_dist.trials[t].offsets[keyOffset][lastOffset];
					var spinCount = last-previous+1;
					return spinCount;					
				}
			}
		},
		getData: function(){
			var d = {
			  numTrials: rand_dist.numTrials,
				keys: rand_dist.keys,
			  offsetLengths: rand_dist.offsetLengths,
			  trialLengths: rand_dist.trialLengths
			};
			if(rand_set.checkNumInRowWin)
			  d["numInRowWin"] = rand_dist.numInRowWin;
			if(rand_set.checkNumInRowLoss)
			  d["numInRowLoss"] = rand_dist.numInRowLoss;
			return d;
		}		
/*
 * pass an keyOffset for trial.keys & get spincount since last hit prob% =
 * (NIR+1) x trials[t].numInRowWin[k][iNIR].length / trials[t].length offsetEnd -
 * (NIR+1) = offsetStart offsetStart - offsetEnd = SpinCount
 */		
};

// --------------------------------------------------------------------------------------------
function getLows(){
  return  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
}

function getHighs(){
  return  [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];
}

function getGreens(){
  return [0, 37];
}

function getReds() {
	return [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
}

function getBlacks() {
	return [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];
}

function getOdds() {
	return [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35];
}

function getEvens() {
	return [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36];
}

function getRow1() {
	return [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36];
}

function getRow2() {
	return [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35];
}

function getRow3() {
	return [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34];
}

function get1_12() {
	return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
}

function get13_24() {
	return [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
}

function get25_36() {
	return [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];
}

function getDS1() {
	return [1, 2, 3, 4, 5, 6];
}

function getDS2() {
	return [4, 5, 6, 7, 8, 9];
}

function getDS3() {
	return [7, 8, 9, 10, 11, 12];
}

function getDS4() {
	return [10, 11, 12, 13, 14, 15];
}

function getDS5() {
	return [13, 14, 15, 16, 17, 18];
}

function getDS6() {
	return [16, 17, 18, 19, 20, 21];
}

function getDS7() {
	return [19, 20, 21, 22, 23, 24];
}

function getDS8() {
	return [22, 23, 24, 25, 26, 27];
}

function getDS9() {
	return [25, 26, 27, 28, 29, 30];
}

function getDS10() {
	return [28, 29, 30, 31, 32, 33];
}

function getDS11() {
	return [31, 32, 33, 34, 35, 36];
}

//street bets
function getS1() {
	return [1, 2, 3];
}

function getS2() {
	return [4, 5, 6];
}

function getS3() {
	return [7, 8, 9];
}

function getS4() {
	return [10, 11, 12];
}

function getS5() {
	return [13, 14, 15];
}

function getS6() {
	return [16, 17, 18];
}

function getS7() {
	return [19, 20, 21];
}

function getS8() {
	return [22, 23, 24];
}

function getS9() {
	return [25, 26, 27];
}

function getS10() {
	return [28, 29, 30];
}

function getS11() {
	return [31, 32, 33];
}

function getS12() {
	return [34, 35, 36];
}


//corner bets
function getC1() {
	return [1, 2, 4, 5];
}

function getC2() {
	return [2, 3, 5, 6];
}

function getC3() {
	return [4, 5, 7, 8];
}

function getC4() {
	return [5, 6, 8, 9];
}

function getC5() {
	return [7, 8, 10, 11];
}

function getC6() {
	return [8, 9, 11, 12];
}

function getC7() {
	return [10, 11, 13, 14];
}

function getC8() {
	return [11, 12, 14, 15];
}

function getC9() {
	return [13, 14, 16, 17];
}

function getC10() {
	return [14, 15, 17, 18];
}

function getC11() {
	return [16, 17, 19, 20];
}

function getC12() {
	return [17, 18, 20, 21];
}

function getC13() {
	return [19, 20, 22, 23];
}

function getC14() {
	return [20, 21, 23, 24];
}

function getC15() {
	return [22, 23, 25, 26];
}

function getC16() {
	return [23, 24, 26, 27];
}

function getC17() {
	return [25, 26, 28, 29];
}

function getC18() {
	return [26, 27, 29, 30];
}

function getC19() {
	return [28, 29, 31, 32];
}

function getC20() {
	return [29, 30, 32, 33];
}

function getC21() {
	return [31, 32, 34, 35];
}

function getC22() {
	return [32, 33, 35, 36];
}

var roulette_groups = {
		red: getReds(),
		black: getBlacks(),
		odd: getOdds(),
		even: getEvens(),
		low: getLows(),
		high: getHighs(),		
		r1: getRow1(),
		r2: getRow2(),
		r3: getRow3(),
		g1_12: get1_12(),
		g13_24: get13_24(),
		g25_36: get25_36(),
		g0_00: getGreens(),
		ds1: getDS1(),
		ds2: getDS2(),
		ds3: getDS3(),
		ds4: getDS4(),
		ds5: getDS5(),
		ds6: getDS6(),
		ds7: getDS7(),
		ds8: getDS8(),
		ds9: getDS9(),
		ds10: getDS10(),
		ds11: getDS11(),
		s1: getS1(),
		s2: getS2(),
		s3: getS3(),
		s4: getS4(),
		s5: getS5(),
		s6: getS6(),
		s7: getS7(),
		s8: getS8(),
		s9: getS9(),
		s10: getS10(),
		s11: getS11(),
		s12: getS12(),
		c1: getC1(),
		c2: getC2(),
		c3: getC3(),
		c4: getC4(),
		c5: getC5(),
		c6: getC6(),
		c7: getC7(),
		c8: getC8(),
		c9: getC9(),
		c10: getC10(),
		c11: getC11(),
		c12: getC12(),
		c13: getC13(),
		c14: getC14(),
		c15: getC15(),
		c16: getC16(),
		c17: getC17(),
		c18: getC18(),
		c19: getC19(),
		c20: getC20(),
		c21: getC21(),
		c22: getC22()
};

rand_set.reset = function(){
	//rand_set American roulette test for rand_set init
	var rsMax= 37;
	if(app.data.table_type != "american"){
	   rsMax = 36;  
	}
	
	rand_set.init({
		min:0,
		max:rsMax,
		groups: roulette_groups,
		checkGroups: true,
		checkStats: true
	});
}

function generateRouletteDistribution(numTrials, type){
	var max = 36;
	if(typeof(type) == "string" & type == "american")
		max = 37;
	
	rand_dist.init({
		min:0,
		max:max,
		groups: roulette_groups,
		numTrials: numTrials,
		checkNumInRowWin: false,
		checkNumInRowLoss: true,
		checkHottness: false,
		checkGroups: false,
		checkStats: false
	}).generateAll();
	
	return rand_dist.getData();
}
// var d = generateRouletteDistribution(1, "european"); debug(d);

function test_randset(){
  rand_set.init({
		min:0,
		max:36,
		groups: roulette_groups
	}).generateTrial();
	return rand_set.getData();
}
// debug(test_randset().numInRowLoss);
// debug(test_randset().numInRowWin);
// debug(test_randset().offsets);
