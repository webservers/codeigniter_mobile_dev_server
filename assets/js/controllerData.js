
var DomainURL = window.location.protocol + "//" +window.location.hostname + "/";
//var DomainURL = "http://wishbone-interactive.com/";
if(window.location.port != "")  DomainURL += ":"+window.location.port;
var DomainPath = window.location.pathname.split("/");
var routerURL = DomainURL+"index.php/";
if(DomainPath[1] == "index.php")
	routerURL = DomainURL+DomainPath[1]+"/";

function XHR_errorHandler(jqXHR, textStatus, errorThrown) {
	alert("ajax error: status"+textStatus+"\n jqHXR: "+jqXHR+"\n error: "+errorThrown);
}
function _urlEncode(params) {
	var url = ''; // '?'
	for (key in params) {
		// if(isString(params[key])) url += key+'="'+params[key]+'"&'; else
		url += key + '=' + params[key] + '&';
	}
	url = url.substring(0, url.length - 1); // removes the last &
	return url;
}
//

function jsonPostXHR(o) {
	if (isObject(o)) {
		var ajx = {
			dataType : 'json',
			url : o.url,
			cache : false,
			type : 'POST',
			async : true,
			error : XHR_errorHandler,
			success : function(data, textStatus, jqXHR) {
				// debug("success: "+textStatus+", data: "+data);
				if (isObject(data) && isFunction(o.callBack))
					o.callBack(data);
			}
		};
		if (typeof (o.params) != "undefined") {
			ajx.data = _urlEncode(o.params);
		}
		$.ajax(ajx);
	}
}

if (typeof (app) == 'undefined') {
	app = {};
}
if(typeof(app.controllers) == 'undefined'){
	app.controllers = {};
}


app.controllers.base = {
	// ------------------ index.php/upload/
	// --------------------------------------
	//
	// The Backend Reads the Directory on This
	/*
	 * getUploadDetail() localhost/BMTmobile/index.php/base/getUploadDetail
	 * :false
	 */
	getGraphicUploadDetailFromFileName : function(_fileName, callBack) {
		jsonPostXHR({
			url : routerURL + "base/getUploadDetail",
			callBack : callBack,
			params : {
				file_name : _fileName
			}
		});
	},
	deleteUploadFileName : function(_fileName, callBack) {
		jsonPostXHR({
			url : routerURL + "base/delete_upload",
			callBack : callBack,
			params : {
				file_name : _fileName
			}
		});
	},
	// The Upload generates data in the Database,
	// the fileName is one of the parameters
	/*
	 * getImages() localhost/BMTmobile/index.php/base/getImages
	 * :{"success":true, "message":"Image Files", "status":1,
	 * "images":{"path":"\/Users\/user_bss\/Documents\/Titanium_Studio_Workspace\/htdocs\/BMTmobile\/uploads\/graphics",
	 * "total":0, "children":[]}, "paging":""}
	 */
	DelImage : function(index) {
		debug("Deleteing File in Index: " + index);
		app.controllers.base.deleteUploadFileName(getFileNameFromIndex(index),
				function(e) {
					// debug("Callback Data:"+e);
					// should remove the image from the zoom view, and load the
					// next one in the array
					// and update the info view to match the image

					// for now not very efficiant but it'll work on a fast
					// connection
					app.widget.getGraphics();
				});
	},
	getImages : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/getImages",
			callBack : function(d) {
				debug(d); // d should = binary json
				callBack(d);
			},
			params : {
				start : 0
			}
		});
	},
	/*
	 * getApps() localhost/BMTmobile/index.php/base/getApps :{"success":true,
	 * "message":"Project Directories", "status":1, "apps":{
	 * "path":"http:\/\/localhost\/BMTmobile\/uploads\/Mobile_APPs",
	 * "dir_name":"webview", "children":[ "bootstrap", "remote_load_apps",
	 * "titanium302_Blank", "titanium302_FormTest", "webview"], "total":5},
	 * "paging":""}
	 */
	getApps : function() {
		jsonPostXHR({
			url : routerURL + "base/getApps",
			callBack : function(d) {
				debug(d); // d should = binary json
			},
			params : {
				start : 0
			}
		});
	},

	// getFiles() localhost/BMTmobile/index.php/base/getFiles
	// :{"files":null}
	getFiles : function() {
		jsonPostXHR({
			url : routerURL + "base/getFiles",
			callBack : function(d) {
				debug(d); // d should = binary json
			},
			params : {
				start : 0
			}
		});
	},

	/*
	 * getArchives() localhost/BMTmobile/index.php/base/getArchives
	 * :{"success":true, "message":"Archived Files", "status":1, "archives":{
	 * "path":"\/Users\/user_bss\/Documents\/Titanium_Studio_Workspace\/htdocs\/BMTmobile\/uploads\/archives",
	 * "total":0, "children":[] }, "paging":"" }
	 */
	getArchives : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/getArchives",
			callBack : function(d) {
				debug(d); // d should = binary json
			},
			params : {
				start : 0
			}
		});
	},
	// ------------------ index.php/Log/ --------------------------------------
	//
	// jsonLogsBySessionsActivity()
	// localhost/BMTmobile/index.php/base/jsonLogsBySessionsActivity
	// :error LogsFromInactiveSession base.php line 564
	LogsBySessionsActivity : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/jsonLogsBySessionsActivity/",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},
	/*
	 * jsonActiveSessions()
	 * localhost/BMTmobile/index.php/base/jsonActiveSessions :{"Sessions":[{
	 * "id":"86d140113e24abbbc8f7cd88fe061f81", "ip_address":"127.0.0.1",
	 * "user_agent":"Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:30.0)
	 * Gecko\/20100101 Firefox\/30.0", "last_activity":"1404357374",
	 * "user_id":"0", "platform":"Windows 7", "browser":"Firefox",
	 * "mobile":false}], "success":true, "message":"", "status":1}
	 */
	jsonActiveSessions : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/jsonActiveSessions/",
			callBack : callBack
		});
	},

	/*
	 * jsonAllSessions() localhost/BMTmobile/index.php/base/jsonAllSessions
	 * :{"Sessions":null, "success":true, "message":"", "status":1}
	 * 
	 * or
	 * 
	 * :{"Sessions":[{ "id":"86d140113e24abbbc8f7cd88fe061f81",
	 * "ip_address":"127.0.0.1", "user_agent":"Mozilla\/5.0 (Windows NT 6.1;
	 * WOW64; rv:30.0) Gecko\/20100101 Firefox\/30.0",
	 * "last_activity":"1404357374", "user_id":"0", "platform":"Windows 7",
	 * "browser":"Firefox", "mobile":false}], "success":true, "message":"",
	 * "status":1}
	 */
	jsonAllSessions : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/jsonAllSessions/",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	/*
	 * jsonAccess() localhost/BMTmobile/index.php/base/jsonAccess :{"Access":[
	 * {"id":"86d140113e24abbbc8f7cd88fe061f81", "ip_address":"127.0.0.1",
	 * "user_agent":"Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:30.0)
	 * Gecko\/20100101 Firefox\/30.0", "last_activity":"1404357374",
	 * "user_id":"0"} ]}
	 */
	jsonAccess : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/jsonAccess/",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	/*
	 * yourSession() localhost/BMTmobile/index.php/base/yourSession
	 * :{"session_id":"791bb3716e90e79499614ead1dc7570c",
	 * "ip_address":"127.0.0.1", "user_agent":"Mozilla\/5.0 (Windows NT 6.1;
	 * WOW64; rv:30.0) Gecko\/20100101 Firefox\/30.0",
	 * "last_activity":1404556261, "user_data":"", "validated":false,
	 * "username":"guest", "type":0, "user_id":4}
	 */
	yourSession : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/yourSession/",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	// get_unused_sessions()
	// localhost/BMTmobile/index.php/base/get_unused_sessions
	// :""
	get_unused_sessions : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/get_unused_sessions",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	/*
	 * reset_database() localhost/BMTmobile/index.php/base/reset_database
	 * :{"success":true, "message":"Successfully cleared Database", "status":1}
	 */
	reset_database : function() {
		jsonPostXHR({
			url : routerURL + "base/reset_database",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},
	/*
	 * delete_log() localhost/BMTmobile/index.php/base/delete_log
	 * :{"success":false, "message":"id not set or already deleted", "status":0}
	 */
	delete_log : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/delete_log",
			params : {
				session_id : o.session_id,
				message : o.message,
				data : o.data
			},
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	/*
	 * jsonClearLogs() localhost/BMTmobile/index.php/base/jsonClearLogs
	 * :{"success":false, "message":"error clearing Logs", "status":0}
	 */
	clearLogs : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/jsonClearLogs/",
			callBack : function(d) {
				debug(d); // d should = binary json
			}
		});
	},

	/*
	 * log()
	 * localhost/BMTmobile/index.php/base/log?message=""&session_id=""&data=""
	 * {"success":false, "message":", message not set!, data not set!",
	 * "status":0}
	 *  : for multipart form encoded uploads 'filecontent' holds binary data
	 * that gets sorted as archive,sound or image.
	 */
	Log : function(o) {
		if (isObject(o)) {
			jsonPostXHR({
				url : routerURL + "base/log",
				params : {
					session_id : o.session_id,
					message : o.message,
					data : o.data
				},
				callBack : o.callBack
			});
		}
	},

	// getLogs() localhost/BMTmobile/index.php/base/getLogs
	// : {"logs":null} //return json object with logs for 'session_id' if
	// specified
	getLogs : function(o) {
		if (isObject(o)) {
			jsonPostXHR({
				url : routerURL + "base/getLogs",
				params : {
					session_id : o.session_id
				},
				callBack : o.callBack
			});
		}
	},
	createTestLogs : function(num) {
		var o = {
			str : "hello work",
			bool : true,
			integer : 1,
			floating : 0.5,
			array : [ 0, 1, 2 ],
			object : {
				o : "the work"
			}
		};
		if (typeof (num) == 'undefined')
			num = 10;
		for (var i = 0; i < num; i++) {
			this.Log({
				message : "test" + i,
				data : JSON.stringify(o)
			});
		}
	},
	// isValidated() localhost/BMTmobile/index.php/base/isValidated
	// : ""
	isValidated : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/isValidated",
			callBack : callBack
		});
	},
	/*
	 * login()
	 * localhost/BMTmobile/index.php/base/login?name="user"&password="007157"
	 * :{"success":false, "message":"Incorrect username or password!",
	 * "status":0}
	 */
	login : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/isValidated",
			callBack : function(d, x, t) {
				debug("isValidated: " + d);
				if (isFunction(callBack))
					callBack();
			}
		});
	},

	/*
	 * logout() localhost/BMTmobile/index.php/base/logout : login HTML screen
	 */
	logout : function() {
		jsonPostXHR({
			url : routerURL + "base/logout",
			callBack : function(d, x, t) {
				debug("logging out: " + d);
			}
		});
	},
	getMobileApps : function(callBack) {
		jsonPostXHR({
			url : routerURL + "base/getApps",
			callBack : function(d, x, t) {
				if(typeof(callBack) == 'function'){					
					callBack(d);
				} else {
					debug("getApps: " + d);
				}
			}
		});
	}
};

/*
 * BMTMobile REST interface: base.php : JSON webservices
 * 
 * base.php->base_view.php index() localhost/BMTmobile/index.php/base_view/
 * nonValidatedContent($data)
 * localhost/BMTmobile/index.php/base_view/nonValidatedContent login():
 * localhost/BMTmobile/index.php/base_view/login?name="user"&password="007157"
 * signup() localhost/BMTmobile/index.php/base_view/signup :generates form
 * upload_form($output = null)
 * localhost/BMTmobile/index.php/base_view/upload_form gCrudUploads($output =
 * null) localhost/BMTmobile/index.php/base_view/gCrudUploads
 * gCrudAccess($output = null)
 * localhost/BMTmobile/index.php/base_view/gCrudAccess gCrudLog($output = null)
 * localhost/BMTmobile/index.php/base_view/gCrudLog gCrudUser($output = null)
 * localhost/BMTmobile/index.php/base_view/gCrudUser test()
 */

app.controllers.game = {
	newGameData : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({
				url : routerURL + "gameview/newGameData",
				params : {
				    game_id : o.game_id,
					casino_id: o.casino_id,
				    data : JSON.stringify(o.data),
					win_loss: o.win_loss,
					amount_wagered: o.amount_wagered
				},			
				callBack : function(d, x, t) {
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug("data: " + d);
					}
				}
			});
		}
	},
	getTimestamps: function(callBack){
		jsonPostXHR({
			url : routerURL + "gameview/getTimestamps",
			callBack : function(d, x, t) {
				if(typeof(callBack) == 'function'){
					callBack(d);
				} else {
					debug("data: " + d);
				}
			}
		});		
	},	
	getGames: function(callBack){
		jsonPostXHR({
			url : routerURL + "gameview/getGames",
			callBack : function(d, x, t) {
				if(typeof(callBack) == 'function'){
					callBack(d);
				} else {
					debug("data: " + d);
				}
			}
		});		
	},
	getGame : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({
				url : routerURL + "gameview/getGame",
				params : {
					id : o.id
				},			
				callBack : function(d, x, t) {
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug("data: " + d);
					}
				}
			});
		}
	}
};

app.controllers.casinos = {
	newCasino : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({ //needs multipart for thumbnail upload??
				url : routerURL + "gameview/newCasino",
				params : {
				    name : o.name,
				    state: o.state,
				    thumbnail: o.thumbnail,
				    website: o.website
				},			
				callBack : function(d, x, t) {
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug("data: " + d);
					}
				}
			});
		}
	},
	getCasinos : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({ //needs multipart for thumbnail upload??
				url : routerURL + "gameview/getCasinos",
				params : {
				    state: o.state
				},			
				callBack : function(d, x, t) {					
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug(d);
					}
				}
			});
		}
	}
};

app.controllers.games = {
	newGame : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({ //needs multipart for thumbnail upload??
				url : routerURL + "gameview/newGame",
				params : {
				    name : o.name,
				    casino_id: o.casino_id,
				    thumbnail: o.thumbnail,
			    	vendor: o.vendor,
			    	table_type: o.table_type,
			    	wheel_type: o.wheel_type,
			    	free_to_spin: o.free_to_spin, //0 or 1
			    	min_bet: o.min_bet,
			    	chip_values: o.chip_values,
			    	max_bet1: o.max_bet1,
			    	max_bet2: o.max_bet2,
			    	max_bet5: o.max_bet5,
			    	max_bet6: o.max_bet6,
			    	max_bet8: o.max_bet8,
			    	max_bet11: o.max_bet11,
			    	max_bet17: o.max_bet17,
			    	max_bet35: o.max_bet35					    
				},			
				callBack : function(d, x, t) {
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug("data: " + d);
					}
				}
			});
		}
	},
	getGamesFromCasino : function(o, callBack) {
		if (isObject(o)) {
			jsonPostXHR({ //needs multipart for thumbnail upload??
				url : routerURL + "gameview/getGamesFromCasino",
				params : {
				    casino_id: o.casino_id			    
				},			
				callBack : function(d, x, t) {
					if(typeof(callBack) == 'function'){
						callBack(d);
					} else {
						debug("data: " + d);
					}
				}
			});
		}
	}
};

function uploadGameData(){
	var game = {
		bets: bets.betHistory,
		nums: rand_set.numHistory
	};
	win_loss = getTotalProfit(game);
	amount_wagered = getAmountWagered(game);
	var o = {
		game_id: app.data.game_id,
		casino_id: app.data.casino_id,
		data: game,
		win_loss: win_loss,
		amount_wagered: amount_wagered
		//user_id: session.userId
	};
	console.log(o);
	app.controllers.game.newGameData(o);
	alert("game saved");
	reset();
}
