//--------- table chip code --------------------------
var selected_chip = null;		 // tracks current chip value
function chip_select(value) {
    // debug("chip_select");
	for(var i=0;i<app.data.chip_values.length;i++){
		if(value == app.data.chip_values[i])
			selected_chip = "chip"+i;
	}
}

function getChipValue(id) {
	if(typeof(id) == "undefined") id = selected_chip;
	var chip_id = id.split("p")[1]/1;
	if(chip_id >=0 & chip_id < app.data.chip_values.length)
		return app.data.chip_values[chip_id];	
}

function loadChips(){
	$("#chips_container").html("");
	var html = "";
	for(var i=0;i<app.data.chip_values.length;i++){
		html += '<div id="chip'+i+'" class="chips_small chip'+i+'_small chips_align border_black unselected_chip">'+app.data.chip_values[i]+'</div>';
	}
	$("#chips_container").html(html);
	for(var i=0;i<app.data.chip_values.length;i++){
		document.getElementById("chip"+i).addEventListener("click", chipListener);		
	}
}


function unselectChips(){
	if (selected_chip != null){
		document.getElementById(selected_chip).className = "chips_small "+selected_chip+"_small chips_align border_black unselected_chip";
		selected_chip = null;
	}
}

function chipListener(mevent) {
	if(betting_enabled & mevent.target.id.indexOf("chip") != -1){
		unselectChips();
		selected_chip = mevent.target.id
		// debug("selected: " + elem_id + " arg: " + mevent.target);
		document.getElementById(selected_chip).className = "chips_small "+selected_chip+"_small chips_align border_black selected_chip";
	} else {
		document.getElementById(mevent.id).removeEventListener("click", chipListener);		
	}
}


function updateProfitOnWin(){
	var payouts = bets.getPayOuts();
	var el = document.getElementById("profitOnWin");
	var label = "Profit on Win: ";
	var totalBetAmount = bets.getTotalValue();	
	if(typeof(payouts) == 'number'){
		var profit = payouts - (totalAmountLostSinceLastWin*-1 + totalBetAmount);		
		el.innerHTML = label + profit.toFixed(2);
	} else {	  
	  for(var i=0; i<payouts.length;i++){
		var profit = payouts[i].toFixed(2) - (totalAmountLostSinceLastWin*-1 + totalBetAmount);
	    label = label + profit.toFixed(2) +" ";
	  }
	 // alert(label);
    el.innerHTML = label;
	}
}

function updateBalance(){
	 document.getElementById("balance").innerHTML = "$Balance: "+balance.toFixed(2);
}
function updateAmountLostSinceLastWin(){
	document.getElementById("amountLostSinceLastWin").innerHTML = "$Lost since win: "+parseFloat(totalAmountLostSinceLastWin.toPrecision(2));
}
function updatePayout(payout){
	document.getElementById("payout").innerHTML = "Payout: "+payout;
}
function alertMaxRatioExceeded(max, ratio){
	alert("Table Max Exceeded "+max+" For Payout Ratio "+ratio);
}
function alertTableMaxExceeded(){
	alert("Table Max Exceeded");
}
	
function element_tracker() {
	var elem_ids = new Array();
	var _elem_ids = null;

	function remove_elements() {
		/*var s = document.getElementById("roulette_table_container"); //this was erasing the table
		if(s != null){
			if(s.children != null){
				while( s.children.length > 0) {
					s.removeChild(s.children[0]);
				}
			}
		}*/
		var el = document.getElementById("roulette_table_container");
		for(var i=0;i<elem_ids.length;i++){
			el.removeChild(document.getElementById(elem_ids[i].elem_id));
		}
		elem_ids = new Array();
		bets.removeAllBets();
	}

	function clearBets(){
		if (elem_ids != null & elem_ids.length > 0) {
			for (var i = 0; i < elem_ids.length; i++) {
				var elem_id = elem_ids[i].elem_id;
				document.getElementById("roulette_table_container").removeChild(document.getElementById(elem_id));
				// restore value to bank roll
				var v = bets.data[elem_id].value;
				bets.removeBet(elem_id);
				balance += v;
				// hide_all_bet_labels();
				// updateBetTotal(0);
			}
		}
		elem_ids = [];
    	updateBalance();
		updateProfitOnWin();	
	}
	
	function remove_last(postBack) {
		if (elem_ids.length == 0 | !betting_enabled)
			return -1;
		try {
			var elem_id = elem_ids[elem_ids.length - 1].elem_id;
			if(typeof(elem_id) == 'undefined') return -1;
			var el = document.getElementById(elem_ids[elem_ids.length - 1].elem_id);
			try{
				if(typeof(el) != null)
					document.getElementById("roulette_table_container").removeChild(el);
			} catch(e){
				debug("removeLast el: "+el+" doesn't exist"+" error: "+e);
				// don't try to remove something that doesn't exist just display error
				return -1;
			}
			if(postBack){
			  	var v = bets.getBetValue(elem_id);
			  	balance += v;
		   		// debug(" len:" + elem_ids.length);
        		updateBalance();
			}
			bets.removeBet(elem_id);
			elem_ids = elem_ids.slice(0, elem_ids.length - 1);

			// debug(" len:" + elem_ids.length);
      
			updateProfitOnWin();
		} catch(e) {
			debug("error: "+e);
		}
		return elem_ids;
	}

	function elemExists(x, y) {
		for (var i = 0; i < elem_ids.length; i++) {
			if (elem_ids[i].x == x & elem_ids[i].y == y)
				return i;
		}
		return -1;
	}
	function subtractBalance(v){
		  if((balance - v) >= 0){
			  balance = balance - v;
			  updateBalance();
		  } else {
			alert("Insufficient funds");
			return false;
		  }		
	}
	function create_element(x, y, ratio, affected) {
		if(!betting_enabled) return;		
		var parent_id = selected_chip;	
		if (parent_id != null) {
			var v = getChipValue(parent_id);
			var elem_offset = elemExists(x, y);
			if (elem_offset == -1) {
				//debug("creating elem: "+v);
				var elem_id = "el_" + elem_ids.length;
				//have to check if bet exceeds tableMax before create
				var max = getTableMaxForMultiple(ratio);
				if(v > max){
					alertMaxRatioExceeded(max, ratio);
					return false;
				}
				if(v+bets.getTotalValue() > getTableMax()){
					alertTableMaxExceeded();
					return false;
				}
				if(subtractBalance(v) == false) return false;
				elem_ids.push({
					elem_id : elem_id,
					parent_id : parent_id,
					x : x,
					y : y
				});
				
				bets.addBet(elem_id, {
					value : v,
					ratio : ratio,
					affected : affected
				});

				//debug("et.create - x: "+x+" y: "+y+" ratio: "+ratio+" affected: "+affected+" value: "+v);
			
				var elem = copyElem(parent_id, elem_id);
				elem.className = "tchip chips_small "+document.getElementById(selected_chip).id+"_small border_black unselected_chip";						
				elem.style.top = x + "px"; //top shouldnt be negative!!
				elem.style.left = y + "px";				
				document.getElementById("roulette_table_container").appendChild(elem);
				//document.getElementById(elem_id).removeEventListener("click", chipListener);
			} else {
				//check if bet exceeds tableMax before update
				var max = getTableMaxForMultiple(ratio);
				var val = v+bets.getBetValue(elem_ids[elem_offset].elem_id);
				if(val > max){
					alertMaxRatioExceeded(max, ratio);
					return false;
				}
				if(val+v+bets.getTotalValue() > getTableMax()){
					alertTableMaxExceeded();
					return false;
				}
				if(subtractBalance(v) == false) return false;
				// update value
				//debug("updating value: "+v);
				bets.addBetValue(elem_ids[elem_offset].elem_id, v);
				document.getElementById(elem_ids[elem_offset].elem_id).innerHTML = bets.getBetValue(elem_ids[elem_offset].elem_id).toFixed(1); 
				// update bet total on chip
			}
		}
		updateProfitOnWin();
		return elem;
	}

	this.save = function() {
		_elem_ids = elem_ids;
	};
	this.recall = function() {
		// debug("et.recall: "+betting_enabled+" : "+_elem_ids);
		if(!betting_enabled) return;
		if (_elem_ids == null) return;
		
		// if recall while there's a existig bet clear the existig bet 1st.
		clearBets();

		var v = bets.lastBetTotalValue();
		// add bet if value < balance
		if ((balance - v) >= 0 & v != 0) {
			bets.recall(); // restore previous data
			for (var i = 0; i < _elem_ids.length; i++) { // recreate elements
															// and place where
															// they were
				// debug("i: "+i);
				var elem_id = "el_" + elem_ids.length;
				var elem = copyElem("chip0", elem_id);
				elem.className = "tchip chips_small "+_elem_ids[i].parent_id+"_small border_black unselected_chip";					
				elem.style.top = _elem_ids[i].x + "px";
				elem.style.left = _elem_ids[i].y + "px";				
				elem_ids.push(_elem_ids[i]);
				document.getElementById("roulette_table_container").appendChild(elem);
				document.getElementById(_elem_ids[i].elem_id).innerHTML = bets.data[elem_id].value;
			}

			// new display the labels
			/*
			 * updateBetTotal(bets.getTotalValue()); try {
			 * update_boxLabel(elem_ids[elem_ids.length - 1], elem_ids); }
			 * catch(e) { debug("error: "+e); }
			 */
		} else {
			alert("Insufficient funds");
			return false;
		}
	    updateBalance();
	    updateProfitOnWin();
		return true;
	};
	this.create = create_element;
	this.removeAll = remove_elements;
	this.removeLast = remove_last;
	this.reset = function(){
		remove_elements();
		elem_ids = new Array();
		_elem_ids = null;
	}
}
var et = new element_tracker();


function loadTable(){
	document.getElementById("roulette_table_container").innerHTML = "";
	// document.getElementById("roulette_stat_table_container").innerHTML = "";
	if(app.data.table_type == "american"){
          $("#roulette_table_container").load("/assets/html/american_table.htm", function (){
              // debug("american table loaded...");
		      // the event is attached to the container not the contents
		      if(typeof(window.cordova) == "undefined"){
			      add_event("roulette_table_container", "click", rListener);								
		      } else {
			      add_event("roulette_table_container", "touchstart", rListener);
		      }
		      $("#stat00").css("green_numbers stat_text");
		      $("#stat00").show();
          		updateBalance();
          });
		      // $("#roulette_stat_table_container").load("american_stat_table.htm");
	} else {
      $("#roulette_table_container").load("/assets/html/european_table.htm", function (){
    	  // debug("european table loaded...");
    	  // the event is attached to the container not the contents
		  if(typeof(window.cordova) == "undefined"){
				add_event("roulette_table_container", "click", rListener);	    
		  } else {
				add_event("roulette_table_container", "touchstart", rListener);
		  }
		  $("#stat00").hide();
       		updateBalance();
		  // $("#roulette_stat_table_container").load("european_stat_table.htm");
		});
	}
}

function hide_all_bet_labels() {
	var nums_on_table = 38;
	if(app.data.table_type != "american"){
		nums_on_table = 37
	}
	for (var i = 0; i < nums_on_table; i++) {
		$("#" + getBetNumId(i)).css("opacity", 0);
		$("#" + getBetNumId(i)).html("");
	}
	$("#box_info1_12").css("opacity", 0);
	$("#box_info13_24").css("opacity", 0);
	$("#box_info25_36").css("opacity", 0);
	$("#box_infoR1").css("opacity", 0);
	$("#box_infoR2").css("opacity", 0);
	$("#box_infoR3").css("opacity", 0);
	$("#box_info1_18").css("opacity", 0);
	$("#box_info19_36").css("opacity", 0);
	$("#box_infoEven").css("opacity", 0);
	$("#box_infoRed").css("opacity", 0);
	$("#box_infoBlack").css("opacity", 0);
	$("#box_infoOdd").css("opacity", 0);
}

function update_boxLabel(elem_ids) {
	/*
	 * { elem_id : elem_id, parent_id : parent_id, x : x, y : y, value : v,
	 * ratio : ratio, affected : affected }
	 */
	var values = {
		0 : "",
		1 : "",
		2 : "",
		3 : "",
		4 : "",
		5 : "",
		6 : "",
		7 : "",
		8 : "",
		9 : "",
		10 : "",
		11 : "",
		12 : "",
		13 : "",
		14 : "",
		15 : "",
		16 : "",
		17 : "",
		18 : "",
		19 : "",
		20 : "",
		21 : "",
		22 : "",
		23 : "",
		24 : "",
		25 : "",
		26 : "",
		27 : "",
		28 : "",
		29 : "",
		30 : "",
		31 : "",
		32 : "",
		33 : "",
		34 : "",
		35 : "",
		36 : "",
		37 : ""
	};
	for (key in elem_ids) {
		for (var i = 0; i < elem_ids[key].affected.length; i++) {
			// n.value*n.ratio
			var v = elem_ids[key].value;
			var r = elem_ids[key].ratio;
			// debug(elem_ids[key].affected[i]);
			values[elem_ids[key].affected[i]] = (values[elem_ids[key].affected[i]] / 1) + (v * r);
		}
	}
	for (key in values) {
		if (values[key] != "") {
			var id = getBetNumId(key);
			// debug(id+":"+values[key]);
			$("#" + id).css("opacity", .99);
			$("#" + id).html(values[key]);
		}
	}
}

function getBetNumId(n) {
	if (n >= 0 & n < 37)
		return "box_info" + n;
	return "box_info00";
}


function getRtableCoords() {
	var coords = {
	  offsetLeft: 0,
	  offsetTop: 0
	};
	coords.table = {
		t : document.getElementById("roulette_table").offsetTop,
		l : document.getElementById("roulette_table").offsetLeft
	};
	coords.table.t += document.getElementById("roulette_table").getBoundingClientRect().y;
	coords.table.l += document.getElementById("roulette_table").getBoundingClientRect().x;
	coords.z_zz = {
		h : document.getElementById("z_zz").offsetHeight,
		w : document.getElementById("z_zz").offsetWidth,
		t : document.getElementById("z_zz").offsetTop,
		l : document.getElementById("z_zz").offsetLeft,
		b0 : {
			h : document.getElementById("b0").offsetHeight,
			w : document.getElementById("b0").offsetWidth,
			t : document.getElementById("b0").offsetTop,
			l : document.getElementById("b0").offsetLeft
		}
	};

	if(app.data.table_type == "american"){
		coords.z_zz.b00 = {
			h : document.getElementById("b00").offsetHeight,
			w : document.getElementById("b00").offsetWidth,
			t : document.getElementById("b00").offsetTop,
			l : document.getElementById("b00").offsetLeft
		};		
	}
	
	coords.l_table = {
		h : document.getElementById("l_table").offsetHeight,
		w : document.getElementById("l_table").offsetWidth,
		t : document.getElementById("l_table").offsetTop,
		l : document.getElementById("l_table").offsetLeft,
		twelve_group : {
			h : document.getElementById("twelve_group").offsetHeight,
			w : document.getElementById("twelve_group").offsetWidth,
			t : document.getElementById("twelve_group").offsetTop,
			l : document.getElementById("twelve_group").offsetLeft,
			b1_12 : {
				h : document.getElementById("b1_12").offsetHeight,
				w : document.getElementById("b1_12").offsetWidth,
				t : document.getElementById("b1_12").offsetTop,
				l : document.getElementById("b1_12").offsetLeft
			}
		},
		rrow1 : {
			h : document.getElementById("rrow1").offsetHeight,
			w : document.getElementById("rrow1").offsetWidth,
			t : document.getElementById("rrow1").offsetTop,
			l : document.getElementById("rrow1").offsetLeft
		},
		rrow2 : {
			h : document.getElementById("rrow2").offsetHeight,
			w : document.getElementById("rrow2").offsetWidth,
			t : document.getElementById("rrow2").offsetTop,
			l : document.getElementById("rrow2").offsetLeft
		},
		rrow3 : {
			h : document.getElementById("rrow3").offsetHeight,
			w : document.getElementById("rrow3").offsetWidth,
			t : document.getElementById("rrow3").offsetTop,
			l : document.getElementById("rrow3").offsetLeft
		},
		b_row_1 : {
			h : document.getElementById("b_row_1").offsetHeight,
			w : document.getElementById("b_row_1").offsetWidth,
			t : document.getElementById("b_row_1").offsetTop,
			l : document.getElementById("b_row_1").offsetLeft,
			b1_18 : {
				h : document.getElementById("b1_18").offsetHeight,
				w : document.getElementById("b1_18").offsetWidth,
				t : document.getElementById("b1_18").offsetTop,
				l : document.getElementById("b1_18").offsetLeft
			},
			b19_36 : {
				h : document.getElementById("b19_36").offsetHeight,
				w : document.getElementById("b19_36").offsetWidth,
				t : document.getElementById("b19_36").offsetTop,
				l : document.getElementById("b19_36").offsetLeft
			}			
		},
		b_row_2 : {
			h : document.getElementById("b_row_2").offsetHeight,
			w : document.getElementById("b_row_2").offsetWidth,
			t : document.getElementById("b_row_2").offsetTop,
			l : document.getElementById("b_row_2").offsetLeft,
			bEven : {
				h : document.getElementById("bEven").offsetHeight,
				w : document.getElementById("bEven").offsetWidth,
				t : document.getElementById("bEven").offsetTop,
				l : document.getElementById("bEven").offsetLeft
			}
		}
	};

	coords.row = {
		b3 : {
			h : document.getElementById("b3").offsetHeight,
			w : document.getElementById("b3").offsetWidth,
			l : document.getElementById("b3").offsetLeft
		},
		b6 : {
			l : document.getElementById("b6").offsetLeft
		},
		b9 : {
			l : document.getElementById("b9").offsetLeft
		},
		b12 : {
			l : document.getElementById("b12").offsetLeft
		},
		b15 : {
			l : document.getElementById("b15").offsetLeft
		},
		b18 : {
			l : document.getElementById("b18").offsetLeft
		},
		b21 : {
			l : document.getElementById("b21").offsetLeft
		},
		b24 : {
			l : document.getElementById("b24").offsetLeft
		},
		b27 : {
			l : document.getElementById("b27").offsetLeft
		},
		b30 : {
			l : document.getElementById("b30").offsetLeft
		},
		b33 : {
			l : document.getElementById("b33").offsetLeft
		},
		b36 : {
			l : document.getElementById("b36").offsetLeft
		},
		br1 : {
			l : document.getElementById("bR1").offsetLeft
		}
	};
	// tchip, betting_chip
	coords.chip = {
		h : 28,
		w : 28
	};
	return coords;
}


// table buttons ------------------------
function enterBalance(){
	var b = prompt("Enter new balance", balance.toFixed(2));
	// alert(b+":"+typeof(b/1));
	if (b != null & typeof (b / 1) == "number") {
		balance = b / 1;
		updateBalance();
	}	
}
function twoX(){
	bets.doubleBets();
	updateProfitOnWin();	
}
function remove_all_bets(postBack){
	// debug("remove all bets");
	while( et.removeLast(postBack).length > 0){}
}

function remove_last_bet() {
	// debug("remove last bet");
	et.removeLast(true);
}

function repeat_last_bet() {
	// debug("repeat last bet");
	et.recall();
}


// table chip placement listener
function rListener(mevent) {
	//console.log(mevent);
	var numWidth = document.getElementById("z_zz").offsetWidth;
	function getColumn(x) {		
		// debug("numWidth: "+numWidth);
		for (var i = 0; i < 13; i++) {
			var r = {
			  low : numWidth + (i * numWidth)-2,
			  high : numWidth + ((i + 1) * numWidth)-2
			};
			r.mid = {
				low : r.high - (numWidth/4),
				high : r.high + (numWidth/4)
			};
			if (x > r.low & x < r.high) {
				//debug(">low "+r.low+" <high "+r.high);
				return i;
			} else if (x > r.mid.low & x < r.mid.high) {
				//debug(">low "+r.mid.low+" <high "+r.mid.high);
				return i + .5;
			}
		}
	}
	if(!betting_enabled) return;
	try {
		if ( typeof (mevent.touches) == "undefined") {
			var y = mevent.clientY;
			var x = mevent.clientX;
		} else {
			var x = mevent.touches[0].clientX;
			var y = mevent.touches[0].clientY;
			//debug("rListener: "+mevent.touches[0]);
		}
		//debug("mouse - " + x + ":" + y);
		var c = getRtableCoords();
		c.l_table.rrow1.t = c.l_table.twelve_group.t + c.l_table.twelve_group.h;
		c.l_table.rrow2.t = c.l_table.rrow1.h * 2;
		c.l_table.rrow3.t = c.l_table.rrow1.h * 3;
		c.l_table.b_row_1.t = c.l_table.rrow3.t + c.l_table.rrow3.h
		c.l_table.b_row_2.t = c.l_table.b_row_1.t + c.l_table.b_row_1.h;
		c.z_zz.l = 0;
		c.l_table.b_row_2.bEven.l = c.l_table.b_row_2.bEven.l;
		c.l_table.b_row_1.b1_18.l = c.l_table.b_row_1.b1_18.l;
		var offsetX = c.offsetLeft - c.table.l;
		var offsetY = c.offsetTop - c.table.t;
		x -= c.table.l;
		y -= c.table.t;
		
		//debug("adjusted - " + x + ":" + y);
		var l_tableL = (c.z_zz.l + c.z_zz.w);
		//debug("l_tableL: "+l_tableL);
		var chipMiddle = c.chip.w / 2;
		var twelve_group_b = c.l_table.twelve_group.t + c.l_table.twelve_group.h;
		/*
		 * var isChrome = false; if (navigator.userAgent.match("Chrome")) {
		 * isChrome = true; }
		 */
		
		if (x > c.z_zz.l & x < l_tableL) {
			debug("z_zz");
			//dump_properties(c.z_zz);
			/*if (y >= (c.z_zz.b0.t + (c.z_zz.h/2)) & y <= (c.z_zz.b0.t + (numWidth/2))) {
				debug("z_zz double");
				var _x = (c.z_zz.b0.w / 2) - chipMiddle;
				var _y = c.z_zz.b00.t + (c.z_zz.h/2) - chipMiddle;
				//debug("y: "+_y);
				if(app.data.table_type == "american")
					et.create(_y, _x, odds.double, [0, 37]);
				else {
					et.create(_y, _x, odds.single, [0]);
				}
			} else {*/
				if(app.data.table_type == "american"){
					if (y > c.z_zz.b00.t & y < c.z_zz.b0.t) {
						debug("zz");
						var _x = (c.z_zz.b0.w / 2) - chipMiddle;
						var _y = c.z_zz.b00.t + chipMiddle;
						et.create(_y, _x, odds.single, [37]);
					} else if (y > c.z_zz.b0.t & y < (c.z_zz.b0.t + c.z_zz.b0.h)) {
						debug("z");
						var _x = (c.z_zz.b0.w / 2) - chipMiddle;
						var _y = c.z_zz.b0.t + chipMiddle;
						et.create(_y, _x, odds.single, [0]);
					}
				} else {
					if (y > c.z_zz.b0.t & y < (c.z_zz.b0.t + c.z_zz.b0.h)) {
						debug("z");
						var _x = (c.z_zz.b0.w / 2) - chipMiddle;
						var _y = c.z_zz.b0.t + (c.z_zz.h/2);
						et.create(_y, _x, odds.single, [0]);
					}					
				}
			//}
		} else if (x > l_tableL & x < (c.l_table.l + c.l_table.w)) {
			//debug("l_table");

			if (y > c.l_table.twelve_group.t & y < twelve_group_b) {
				//debug("twelve_group");
				var _y = (c.l_table.twelve_group.b1_12.h - c.chip.h) / 2;
				if (x > l_tableL & x < l_tableL + c.l_table.twelve_group.b1_12.w + 2) {
					//debug("1-12");
					var _x = l_tableL + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, get1_12());
				} else if (x > l_tableL + (c.l_table.twelve_group.b1_12.w + 2) & x < (l_tableL + (c.l_table.twelve_group.b1_12.w + 2) * 2)) {
					//debug("13-24");
					var _x = l_tableL + c.l_table.twelve_group.b1_12.w + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, get13_24());
				} else if (x > (l_tableL + (c.l_table.twelve_group.b1_12.w) * 2) & x < (l_tableL + (c.l_table.twelve_group.b1_12.w) * 3)) {
					//debug("25-36");
					var _x = l_tableL + c.l_table.twelve_group.b1_12.w * 2 + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, get25_36());
				}
			} else if (y > c.l_table.rrow1.t & y < c.l_table.rrow2.t) {
				//debug("row1");
				var _c = getColumn(x);
				// place bet proper
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow1.t + c.table.t + offsetY+1;
				var r = getRow1();
				var n = 0;
				if (_c < 11.5) {
					if (Math.ceil(_c) > _c) {
						n = new Array();
						n.push(r[Math.floor(_c)]);
						n.push(r[Math.ceil(_c)]);
						et.create(_y, _x, odds.double, n);
					} else {
						n = r[_c];
						et.create(_y, _x, odds.single, [n]);
					}
				}
				if (_c == 12) {
					et.create(_y, _x, odds.column, r);
				}
				//debug("Column: "+_c+" min/max:"+n);
			} else if (y > (c.l_table.rrow2.t - (numWidth/2)) & y < (c.l_table.rrow2.t + (numWidth/2))) {
				//debug("row 1 & 2");
				var _c = getColumn(x);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow2.t + c.table.t - chipMiddle + offsetY;
				var r1 = getRow1();
				var r2 = getRow2();
				var n = new Array();
				if (_c < 11.5) {
					if (Math.ceil(_c) > _c) {
						n.push(r1[Math.floor(_c)]);
						n.push(r1[Math.ceil(_c)]);
						n.push(r2[Math.floor(_c)]);
						n.push(r2[Math.ceil(_c)]);
						et.create(_y, _x, odds.corner, n);
					} else {
						n.push(r1[_c]);
						n.push(r2[_c]);					
						et.create(_y, _x, odds.double, n);
					}
					//debug("min/max: " + n);
				}
			} else if (y > c.l_table.rrow2.t & y < c.l_table.rrow3.t) {
				//debug("row2");
				var _c = getColumn(x);
				// place bet proper
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow2.t + c.table.t + offsetY+2;
				var r = getRow2();
				var n = 0;
				if (_c < 11.5) {
					if (Math.ceil(c) > _c) {
						n = new Array();
						n.push(r[Math.floor(_c)]);
						n.push(r[Math.ceil(_c)]);
						//debug("min/max :"+n);
						// et.create(_y, _x, odds.double, n);
					} else {
						n = r[_c];
						et.create(_y, _x, odds.single, [n]);
					}
				}
				//debug("Column: "+_c+" min/max:"+n);
				if (_c == 12) {
					et.create(_y, _x, odds.column, r);
				}
			} else if (y > (c.l_table.rrow3.t - (numWidth/2)) & y < (c.l_table.rrow3.t + (numWidth/2))) {
				//debug("row 2 & 3");
				var _c = getColumn(x);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow3.t + c.table.t - chipMiddle + offsetY;
				var r1 = getRow2();
				var r2 = getRow3();
				var n = new Array();
				if (_c < 11.5) {
					if (Math.ceil(_c) > _c) {
						n.push(r1[Math.floor(_c)]);
						n.push(r1[Math.ceil(_c)]);
						n.push(r2[Math.floor(_c)]);
						n.push(r2[Math.ceil(_c)]);
						et.create(_y, _x, odds.corner, n);
					} else {
						n.push(r1[_c]);
						n.push(r2[_c]);
						et.create(_y, _x, odds.double, n);
					}
					//debug("min/max: " + n);
				}

			} else if (y > c.l_table.rrow3.t & y < c.l_table.b_row_1.t) {
				//debug("row3");
				var _c = getColumn(x);
				// place bet proper
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow3.t + c.table.t + offsetY+4;
				var r = getRow3();
				var n = 0;
				if (_c < 11.5) {
					if (Math.ceil(_c) > _c) {
						n = new Array();
						n.push(r[Math.floor(_c)]);
						n.push(r[Math.ceil(_c)]);
						et.create(_y, _x, odds.double, n);
					} else {
						n = r[_c];
						et.create(_y, _x, odds.single, [n]);
					}
				}
				//debug("Column: "+_c+" min/max:"+n);
				if (_c == 12) {
					et.create(_y, _x, odds.column, r);
				}
			} else if (y > (c.l_table.b_row_1.t - (numWidth/2)) & y < (c.l_table.b_row_1.t + (numWidth/2))) {
				//debug("row 3 and bottom line");
				var _c = getColumn(x);
				var _x = l_tableL + (_c * c.row.b3.w) + 5;
				var _y = c.l_table.b_row_1.t - chipMiddle + 5;
				var r1 = getRow1();
				var r2 = getRow2();
				var r3 = getRow3();
				var n = new Array();
				if (_c < 11.5) {
					if (Math.ceil(_c) > _c) {
						n.push(r1[Math.floor(_c)]);
						n.push(r1[Math.ceil(_c)]);
						n.push(r2[Math.floor(_c)]);
						n.push(r2[Math.ceil(_c)]);
						n.push(r3[Math.floor(_c)]);
						n.push(r3[Math.ceil(_c)]);
						et.create(_y, _x, odds.double_street, n);
					} else {
						n.push(r1[_c]);
						n.push(r2[_c]);
						n.push(r3[_c]);
						et.create(_y, _x, odds.street, n);
					}
					//debug("min/max: " + n);
				}
			} else if (y > c.l_table.b_row_1.t & y < c.l_table.b_row_2.t) {
				//debug("b_row_1");
				var _y = c.l_table.b_row_1.t + (c.l_table.b_row_1.b1_18.h - c.chip.h) / 2 + 6;
				if (x > l_tableL & x < (c.l_table.b_row_1.b1_18.l + c.l_table.b_row_1.b1_18.w)) {
					//debug("1-18");
					var _x = l_tableL + (c.l_table.b_row_1.b1_18.w / 2) - chipMiddle;
					et.create(_y, _x, odds.high_low, getLows());
				} else if (x > c.l_table.b_row_1.b19_36.l & x < (c.l_table.b_row_1.b19_36.l + c.l_table.b_row_1.b19_36.w)) {
					//debug("19-36");
					var _x = l_tableL + c.l_table.b_row_1.b1_18.w + (c.l_table.b_row_1.b1_18.w / 2) - chipMiddle;
					et.create(_y, _x, odds.high_low, getHighs());
				}
			} else if (y > c.l_table.b_row_2.t & y < (c.l_table.b_row_2.t + c.l_table.b_row_2.h)) {
				//debug("b_row_2");
				var _y = c.l_table.b_row_2.t + (c.l_table.b_row_2.bEven.h - c.chip.h) / 2 + 8;
				if (x > l_tableL & x < (c.l_table.b_row_2.bEven.l + c.l_table.b_row_2.bEven.w + 4)) {
					//debug("even");
					var _x = l_tableL + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.odd_even, getEvens());
				} else if (x > (c.l_table.b_row_2.bEven.l + c.l_table.b_row_2.bEven.w + 4) & x < (c.l_table.b_row_2.bEven.l + (c.l_table.b_row_2.bEven.w * 2) + 4)) {
					//debug("red");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.black_red, getReds());
				} else if (x > (c.l_table.b_row_2.bEven.l + (c.l_table.b_row_2.bEven.w * 2) + 4) & x < (c.l_table.b_row_2.bEven.l + (c.l_table.b_row_2.bEven.w * 3) + 4)) {
					//debug("black");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w * 2 + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.black_red, getBlacks());
				} else if (x > (c.l_table.b_row_2.bEven.l + (c.l_table.b_row_2.bEven.w * 3) + 4) & x < (c.l_table.b_row_2.bEven.l + (c.l_table.b_row_2.bEven.w * 4) + 4)) {
					//debug("odd");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w * 3 + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.odd_even, getOdds());
				}
			}
		}
	} catch(e) {
		debug("error: "+e);
	}
}

/*
{
	type: "red",
	numbers: []
}
*/
function placeBetScript(bet) {
	function getColumnFromBet(bet) {
		var r1 = getRow1();
		var r2 = getRow2();
		var r3 = getRow3();
		
		var i = -1;
		i = getMatch(r1, bet.numbers[0]);
		if(i == -1)
			i = getMatch(r2, bet.numbers[0]);
		if(i == -1)
			i = getMatch(r3, bet.numbers[0]);
		if(bet.type != "single" & bet.type != "street" & bet.type != "double")
			i += .5;
		if(bet.type == "double" & (bet.numbers[1] - bet.numbers[0] > 1 )){
			i += .5;
		}
		// return a number 0-12
		return i;
	}
	
	if(!betting_enabled) return;
	try {
		var c = getRtableCoords();
		c.l_table.rrow1.t = c.l_table.twelve_group.t + c.l_table.twelve_group.h;
		c.l_table.rrow2.t = c.l_table.rrow1.h * 2;
		c.l_table.rrow3.t = c.l_table.rrow1.h * 3;
		c.l_table.b_row_1.t = c.l_table.rrow3.t + c.l_table.rrow3.h
		c.l_table.b_row_2.t = c.l_table.b_row_1.t + c.l_table.b_row_1.h;
		c.z_zz.l = 0;
		c.l_table.b_row_2.bEven.l = c.l_table.b_row_2.bEven.l;
		c.l_table.b_row_1.b1_18.l = c.l_table.b_row_1.b1_18.l;
		var offsetX = c.offsetLeft - c.table.l;
		var offsetY = c.offsetTop - c.table.t;
		//x -= c.table.l;
		//y -= c.table.t;
		
		//debug("adjusted - " + x + ":" + y);
		var l_tableL = (c.z_zz.l + c.z_zz.w);
		var chipMiddle = c.chip.w / 2;
		//var twelve_group_b = c.l_table.twelve_group.t + c.l_table.twelve_group.h;
		
			if (bet.type == "double" & bet.numbers[0] == 0 & bet.numbers[1] == 37) {
				// debug("z_zz double");
				if(app.data.table_type == "american"){
					var _x = c.table.l + (c.z_zz.b0.w / 2) - chipMiddle;
					var _y = c.z_zz.b0.t - chipMiddle + c.table.t;
					et.create(_y, _x, odds.double, [0, 37]);					
				} else {
					//debug("betting on double 0_00 on european table");
				}
			} else if (bet.type == "single" & bet.numbers[0] == 37) {
				// debug("zz");
				if(app.data.table_type == "american"){
					var _x = c.table.l + (c.z_zz.b0.w / 2) - chipMiddle;
					var _y = c.z_zz.b00.t + chipMiddle + c.table.t;
					et.create(_y, _x, odds.single, [37]);
				} else {
					//debug("betting on single 00 on european table");
				}
			} else if (bet.type == "single" & bet.numbers[0] == 0) {
				// debug("z");
				var _x = c.table.l + (c.z_zz.b0.w / 2) - chipMiddle;
				var _y = c.z_zz.b0.t + chipMiddle + c.table.t;
				if(app.data.table_type != "american")
					_y += c.z_zz.b0.h/3;
				et.create(_y, _x, odds.single, [0]);			
			}
			else if ((bet.type == "single" | (bet.type == "double" && (bet.numbers[1] - bet.numbers[0] == 3)) | bet.type == "row") && getMatch(getRow1(), bet.numbers[0]) >= 0) { 
				// place bet on row1
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow1.t + c.table.t + 3;
				if (bet.type == "row") { // place bet on row
					_x = l_tableL + c.table.l + (12 * c.row.b3.w) + offsetX;
					_y = c.l_table.rrow1.t + c.table.t + offsetY+4;
					et.create(_y, _x, odds.column, bet.numbers);
				} else {
					if (bet.type == "double") { // horizontal double
						et.create(_y, _x, odds.double, bet.numbers);
					} else {
						n = r[_c];
						et.create(_y, _x, odds.single, bet.numbers);
					}
				}
			} else if ((bet.type == "single" | (bet.type == "double" && (bet.numbers[1] - bet.numbers[0] == 3)) | bet.type == "row") && getMatch(getRow2(), bet.numbers[0]) >= 0) {
				// place bet on 1 or 2 numbers on row 2
				// debug("row2");
				// place bet proper
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow2.t + c.table.t + 3;
				if (bet.type == "row") {
					_x = l_tableL + c.table.l + (12 * c.row.b3.w) + offsetX;
					_y = c.l_table.rrow2.t + c.table.t + offsetY+4;
					et.create(_y, _x, odds.column, bet.numbers); // place bet on row button
				}
				else if (bet.type == "double") { // horizontal double
					et.create(_y, _x, odds.double, bet.numbers);
				} else {
					n = r[_c];
					et.create(_y, _x, odds.single, bet.numbers);
				}
			} else if ((bet.type == "single" | (bet.type == "double" && (bet.numbers[1] - bet.numbers[0] == 3)) | bet.type == "row") && getMatch(getRow3(), bet.numbers[0]) >= 0) {
				// place bet on 1 or 2 numbers on row3
				// debug("row3");
				// place bet proper
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow3.t + c.table.t + 3;
				if (bet.type == "row") {
					_x = l_tableL + c.table.l + (12 * c.row.b3.w) + offsetX;
					_y = c.l_table.rrow3.t + c.table.t + offsetY+4;
					et.create(_y, _x, odds.column, bet.numbers);  // place bet on entire row
				}
				else if (bet.type == "double") { // horizontal double
					et.create(_y, _x, odds.double, bet.numbers);
				} else {
					et.create(_y, _x, odds.single, bet.numbers);
				}
			} else if ((bet.type == "double" && getMatch(getRow2(), bet.numbers[0]) >= 0) | (bet.type == "corner" && getMatch(getRow2(), bet.numbers[0]) >= 0)) {
				// place bet on 4 or 2 numbers, within row1 and row2 r1&r2
				// debug("r12");
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow2.t + c.table.t - chipMiddle + offsetY;
				if (bet.type == "corner") {
					et.create(_y, _x, odds.corner, bet.numbers);
				} else {
					et.create(_y, _x, odds.double, bet.numbers);
				}
			} else if ((bet.type == "double" && getMatch(getRow3(), bet.numbers[0]) >= 0) | (bet.type == "corner" && getMatch(getRow3(), bet.numbers[0]) >= 0)) { 
				// place bet on 2 or four numbers within row2 and row3
				// debug("r23");
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + c.table.l + (_c * c.row.b3.w) + offsetX;
				var _y = c.l_table.rrow3.t + c.table.t - chipMiddle + offsetY;
				if (bet.type == "corner") {
					et.create(_y, _x, odds.corner, bet.numbers);
				} else {
					et.create(_y, _x, odds.double, bet.numbers);
				}
			} else if ((bet.type == "street" | bet.type == "double_street") && getMatch(getRow3(), bet.numbers[0]) >= 0) { 
				// place beton 6 or 3 numbers at the bottom of row3
				// debug("r3b1");
				var _c = getColumnFromBet(bet);
				var _x = l_tableL + (_c * c.row.b3.w) + 5;
				var _y = c.l_table.b_row_1.t + c.table.t - chipMiddle + offsetY;
				if (bet.type == "double_street") {
					et.create(_y, _x, odds.double_street, bet.numbers);
				} else {
					// _c -= .5;
					et.create(_y, _x, odds.street, bet.numbers);
				}
			}
			else if (bet.type == "g1_12" | bet.type == "g13_24" | bet.type == "g25_36") {
				// debug("twelve_group");
				var _y = (c.l_table.twelve_group.b1_12.h - c.chip.h) / 2;
				if (bet.type == "g1_12") {
					// debug("1-12");
					var _x = l_tableL + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, bet.numbers);
				} else if (bet.type == "g13_24") {
					// debug("13-24");
					var _x = l_tableL + c.l_table.twelve_group.b1_12.w + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, bet.numbers);
				} else if (bet.type == "g25_36") {
					// debug("25-36");
					var _x = l_tableL + c.l_table.twelve_group.b1_12.w * 2 + (c.l_table.twelve_group.b1_12.w / 2) - chipMiddle;
					et.create(_y, _x, odds.dozen, bet.numbers);
				}
			}
			else if (bet.type == "low" | bet.type == "high") {
				// debug("b_row_1");
				var _y = c.l_table.b_row_1.t + (c.l_table.b_row_1.b1_18.h - c.chip.h) / 2 + 6;
				if (bet.type == "low") {
					// debug("1-18");
					var _x = l_tableL + (c.l_table.b_row_1.b1_18.w / 2) - chipMiddle;
					et.create(_y, _x, odds.high_low, bet.numbers);
				} else if (bet.type == "high") {
					// debug("19-36");
					var _x = l_tableL + c.l_table.b_row_1.b1_18.w + (c.l_table.b_row_1.b1_18.w / 2) - chipMiddle;
					et.create(_y, _x, odds.high_low, bet.numbers);
				}
			} else if (bet.type == "black" | bet.type == "red" | bet.type == "even" | bet.type == "odd") {
				// debug("b_row_2" + c.l_table.b_row_2.bEven.l);				
				var _y = c.l_table.b_row_2.t + (c.l_table.b_row_2.bEven.h - c.chip.h) / 2 + 8;				
				if (bet.type == "even") {
					// debug("even");
					var _x = l_tableL + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.odd_even, bet.numbers);
				} else if (bet.type == "red") {
					// debug("red");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.black_red, bet.numbers);
				} else if (bet.type == "black") {
					// debug("black");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w * 2 + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.black_red, bet.numbers);
				} else if (bet.type == "odd") {
					// debug("odd");
					var _x = l_tableL + c.l_table.b_row_2.bEven.w * 3 + (c.l_table.b_row_2.bEven.w / 2) - chipMiddle;
					et.create(_y, _x, odds.odd_even, bet.numbers);
				}
			}
	} catch(e) {
		debug("error: "+e);
	}
}

/*
{
	type: "red",
	amount: 20,
	numbers: []
}
*/
function placeBet(o){
	var _o = {
		type: o.type,
		numbers: o.numbers
	};
	
	var chip_values = app.data.chip_values;
	while(o.amount > 0){
		for(var i=chip_values.length-1;i!=0;i--){ //start with higheset value first
			// debug(chip_values[i]);
			if(o.amount >= chip_values[i]){
				o.amount -= chip_values[i];
				chip_select(chip_values[i]);
				break;
			}
		}
		placeBetScript(_o);
	}
}

function placeGroupNameBet(groupName, amount){
	var o = {
		type: "",
		amount: amount,
		numbers: rand_set.groups[groupName] //returns array of numbers
	};
	if(typeof(groupName) == "string"){		
		if((groupName == "red") | (groupName == "black") |(groupName == "even") |(groupName == "odd") |(groupName == "high") |(groupName == "low")){
			o.type = groupName;
		} else if((groupName == "r1") | (groupName == "r2") |(groupName == "r3") | (groupName == "g1_12") | (groupName == "g13_24") | (groupName == "g25_36")){
			o.type = "row";
		} else if(groupName.indexOf("ds") != -1){
			o.type = "double_street";
		} else if(groupName.indexOf("s") != -1){
			o.type = "street"
		} else if(groupName.indexOf("c") != -1){
			o.type = "corner";
		}
	}
	placeBet(o);	
}

