function getPoint(c1,c2,radius,angle){
    return [c1+Math.cos(angle)*radius,c2+Math.sin(angle)*radius];
}

function byte2Hex(n) {
  var nybHexString = "0123456789ABCDEF";
  return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
}

function RGB2Color(r,g,b) {
	return '#' + byte2Hex(r) + byte2Hex(g) + byte2Hex(b);
}

function getColorWheel(numbers, i) {
	if(numbers[i] == 0 | numbers[i] == 37)
		return RGB2Color(0,255,0);
	var num = numbers[i]/1;
	if(getReds().indexOf(num) != -1)
		return RGB2Color(255,0,0);
	return RGB2Color(0,0,0);
}

var wheel = {
	canvas: null,
	ctx: null,
	center: 0,
	arc: 0,
	insideRadius: 0,
	numberOrder: [],
	init: function(id){
		//<canvas id="canvas" width="250" height="250" class="wheel"></canvas>
		wheel.canvas = document.getElementById(id);
		wheel.center = wheel.canvas.width / 2;
		if (canvas.getContext)
			wheel.ctx = wheel.canvas.getContext("2d");
	    wheel.ctx.font = 'bold 12px Helvetica, Arial';
		wheel.numberOrder = wheel_number_order();
		wheel.arc = Math.PI / (wheel.numberOrder.length / 2);
		return this;
	},
	reset: function(){
		wheel.numberOrder = wheel_number_order();
		wheel.arc = Math.PI / (wheel.numberOrder.length / 2);
		wheel.center = wheel.canvas.width / 2;
		wheel.ctx.clearRect(0,0,wheel.canvas.width, wheel.canvas.height); // 0,0,w,h
		return this;
	},
	getAngle: function(i){ //i is index of numberOrder
		return (i * wheel.arc) - ((Math.PI * 2) / 360) * 95;
	},
	drawWillHit: function(numbers){
		for(var i = 0; i < wheel.numberOrder.length; i++) {
		  var angle = wheel.getAngle(i);
		
		  // draw numbers will hit wheel in yellow
		  wheel.ctx.beginPath();
		  wheel.ctx.moveTo(wheel.center, wheel.center);
		  var point = getPoint(wheel.center, wheel.center, wheel.insideRadius, angle);
		  wheel.ctx.lineTo(point[0], point[1]);
		  wheel.ctx.arc(wheel.center, wheel.center, wheel.insideRadius, angle + wheel.arc, angle, true);
		  point = getPoint(wheel.center, wheel.center, wheel.insideRadius, angle + wheel.arc);
		  wheel.ctx.lineTo(point[0], point[1]);
		  wheel.ctx.lineTo(wheel.center, wheel.center);
		  wheel.ctx.stroke();
		  wheel.ctx.fillStyle = RGB2Color(64, 64, 64);
		  if(numbers.indexOf(wheel.numberOrder[i]) != -1)
			  wheel.ctx.fillStyle = RGB2Color(255, 255, 102);
		  wheel.ctx.fill();
		  wheel.ctx.save();
		}
		return this;
	},
	draw: function(){
		var outsideRadius = wheel.center;
		var textRadius = outsideRadius - 18;
		wheel.insideRadius = textRadius - 8;

		wheel.ctx.clearRect(0,0,wheel.canvas.width, wheel.canvas.height); // 0,0,w,h
		wheel.ctx.strokeStyle = "black";
		wheel.ctx.lineWidth = 1;
		
		for(var i = 0; i < wheel.numberOrder.length; i++) {
		  var angle = wheel.getAngle(i);
			//console.log(angle);
		  wheel.ctx.beginPath();
		  wheel.ctx.arc(wheel.center, wheel.center, outsideRadius, angle, angle + wheel.arc, false);
		  wheel.ctx.arc(wheel.center, wheel.center, wheel.insideRadius, angle + wheel.arc, angle, true);
		  wheel.ctx.stroke();
		  wheel.ctx.fillStyle = getColorWheel(wheel.numberOrder, i);
		  wheel.ctx.fill();
		  wheel.ctx.save();
		  
		  // wheel.ctx.shadowOffsetX = -1;
		  // wheel.ctx.shadowOffsetY = -1;
		  // wheel.ctx.shadowBlur = 0;
		  // wheel.ctx.shadowColor = "rgb(220,220,220)";
		  wheel.ctx.fillStyle = "white";
		  wheel.ctx.translate(wheel.center + Math.cos(angle + wheel.arc / 2) * textRadius, 
				  wheel.center + Math.sin(angle + wheel.arc / 2) * textRadius);
		  wheel.ctx.rotate(angle + wheel.arc / 2 + Math.PI / 2);
		  if(wheel.numberOrder[i] == 37)
			  wheel.ctx.fillText("00", -wheel.ctx.measureText(wheel.numberOrder[i]).width / 2, 0);
		  wheel.ctx.fillText(wheel.numberOrder[i].toString(), -wheel.ctx.measureText(wheel.numberOrder[i]).width / 2, 0);
		  wheel.ctx.restore();
		}
		return this;
	}
};



