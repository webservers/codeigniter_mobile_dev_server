/* Log.js
 * 
 * Author: Brad Steffy
 * 
 * 
 * LogJs came from the need to toggle the display of certain log's
 *  I often use log statements to debug apps, and they tend to strangle each other.
 *  So I thought that why not have more log types even custom types
 *  and support multiple output formats
 * 
 *  I'd like to extend LogJS by creating
 *  LogJsAjax: upload logs to a server and flush cache
 * 
 *  and
 * 
 *  LogJsLocalStorage: save logs localy and flush cache
 * 
 *  these should be accomplished by adding callBack checkers in _echo
 */
function isSet(r){
	if(typeof r != "undefined")
		return true;
	return false;	
}
function isObject(r){
	if(typeof r == "object")
		return true;
	return false;
}
function isFunction(r){
	if(typeof r == 'function')
		return true;
	return false;	
}
function isArray(r){
	if( isObject(r) && isFunction(r.slice) )
		return true;
	return false;
}
var Log = null;
//Logs messages and optionally stores data to them
//displays in FIFO format and deletes displayed messages
var Log = {
	catagories: new Array(),  //array of objects
	outputMode: "console",
	LogCmd: Ti.API.log,
	disp: true,
	LogCallback: null,
	findInCatagory: function(k, v){
		if(!isArray(Log.catagories) || !isSet(k) || !isSet(v)) return null;
        var r = null;		
		for(var i=0;i<Log.catagories.length;i++){
		    //Log.LogCmd(i+a[i][k]);
		    if(Log.catagories[i]["args"][k] == v){
                if(r == null)
                    r = i;                           
                else if(! isArray(r)){ //convert to array
                    var t = new Array();
                    t.push(r);
                    r = t;
                }
				else if(isArray(r)){ //add to array
                    r.push(i);
                }                 
            }
		}
		return r;
	},
	addCatagory: function(o){
	    //array search catagories for name, add it if it's not there
	    if(!isSet(o.name)){ Log.LogCmd("[LOG] Error: You Must Specify Name{name: 'theName'}"); return false;}
	    if(Log.findInCatagory("name", o.name) == null){
	        var c = new Object(); //create catagory object
	        c.data = new Array();
	        c.args = o;
	        if(!isSet(c.args.disp))	
	        	c.args.disp = true;
	        Log.catagories.push(c);
	        Log[o.name] = function(msg){
		       //Log function is added dynamically
		       //name is the name of c.name && Log function name
		       //equivelant to log statement for each catagory
		       var ci = Log.findInCatagory("name", o.name); //try not to search every time
		       if(ci != null){ 
			    //alert("name: "+o.name+" id: "+ci);
		           var c = new Object();
		           c.time = new Date().getTime();
		           c.args = arguments;
		           Log.catagories[ci].data.push(c);
		       }
	           Log.clear = function(){
	           		//Log.LogCmd("clearing cache"+ci);
					Log.catagories[ci].data = new Array();
	           }
		       Log._echo(ci); //echo determines if disp == true
		       return Log;
	   		};
	    } else {
	        Log.LogCmd("[Log] Error: catagory already exists");
	    }
	    return this;
	},
	getMessages: function(ci){
		if(typeof ci != "number")
			ci = Log.findInCatagory("name", ci);
		if(ci < Log.catagories.length ){
			return Log.catagories[ci].data;
		}
		return false;
	},
	outputString: function(name, msg){
			if(Log.outputMode == "console"){
				return "["+name.toUpperCase()+"] "+msg;
			}
			else if(Log.outputMode == "html"){
				return "<br />["+name.toUpperCase()+"] "+msg;
			}
	},
	outputMsgs: function(name, Msgs){
        for(var i=0;i<Msgs.length;i++){
			Log.LogCmd(Log.outputString(name, Msgs[i].args[0]));
		}
	},
	outputLastMsg: function(name, Msgs){
		Log.LogCmd(Log.outputString(name, Msgs[(Msgs.length-1)].args[0]));			
	},	
	_echo: function(ci){
		if(typeof ci != 'number'){ //looksup all displayable logs and displays them
	    	var dl = Log.findInCatagory("disp", true); //returns an array of array offsets
        	if(isArray(dl)){
	       	    for(var i=0;i<dl.length;i++){
	       			Log.outputMsgs(Log.catagories[dl[i]].args.name, Log.catagories[dl[i]].data);
	        	}
	        }else{
			    Log.outputMsgs(Log.catagories[dl].args.name, Log.catagories[dl].data);
	        }
        }else if(typeof ci == 'number' && Log.disp){
        	Log.outputLastMsg(Log.catagories[ci].args.name, Log.catagories[ci].data);
        	//Log.catagories[ci].data = new Array();
        }
        if(isFunction(Log.LogCallback))
        	Log.LogCallback(ci);
	    return this;
	}
};

/* Create a catagory
 * display is automatically true, so no need for disp:true
 */
//Log.addCatagory({name: "info"});
//Log.addCatagory({name: "debug", disp: false});


/* Creating a catagory creates a function to log for that catagory
 * you can add data to the log statement
 * 
 * you can clear the cache also
 */
//Log.info("info1", {data: 34, school: "berkley"});
//Log.info().clear();


/* creating the callback ------
 * 
 * var i = Log.catagories offset
 * 
 * var f = function(i){
 *    alert(i);
 * }
 * Log.LogCallback = f;
 * 
 */

