
var balance = 0;		 //bank roll of current game
var betting_enabled = true;		 //disables betting while the wheel is spinning
var percentOfBalance = .5;
var probabilityTrigger = 99;
var probabilityStop = .000143;
var totalAmountWagered = 0;    //also known as bets wagered
var totalPayoutsRecieved = 0;  //
var totalAmountLostSinceLastWin = 0;
/* would liketo track all bets made, for a history of betting andoutcome*/

var odds = {
	single: 36,
	double: 18,
	corner: 9,
	street: 12,
	double_street: 6,
	dozen: 3,
	column: 3,
	black_red: 2,
	odd_even: 2,
	high_low: 2	
};

function getTableMax(){
	try{
		return app.data.games[app.data.game_id].table_max;
	} catch(e){
		debug(e);
	}	
}

function getTableMaxForMultiple(ratio){
	try{
		
		var game = app.data.games[app.data.game_id];
		if(ratio == 36) return game.max_bet35;
		if(ratio == 18) return game.max_bet17;
		if(ratio == 12) return game.max_bet11;		
		if(ratio == 9) return game.max_bet8;
		if(ratio == 6) return game.max_bet5;
		if(ratio == 3) return game.max_bet2;		
		if(ratio == 2) return game.max_bet1;
		//debug("getTableMaxForRatio: "+ratio);
	} catch(e){
		debug(e);
	}
}
/*
p = principal or initital balance
r = interest rate
t = periods
*/
function compoundInterest(p, r, t){	
	return p* Math.pow((1+r),t);
}

//n is the cycle number
function singleNumberBalanceNeeded(minBet, lossCount){	
	return minBet*36*(2^lossCount - 1); // returns balance needed
}

function getMultiple(groupName){
	if(typeof(groupName) == "string"){		
		if((groupName == "red") | (groupName == "black") |(groupName == "even") |(groupName == "odd") |(groupName == "high") |(groupName == "low")){
			return 2;
		} else if((groupName == "r1") | (groupName == "r2") |(groupName == "r3") | (groupName == "g1_12") | (groupName == "g13_24") | (groupName == "g25_36")){
			return 3;
		} else if(groupName.indexOf("ds") != -1){
			return 6;
		} else if(groupName.indexOf("s") != -1){
			return 12;
		} else if(groupName.indexOf("c") != -1){
			return 9;
		}
	}
	return 36;
}

function getPayoutRatio(numbersAffected){
	return ((36 - numbersAffected.length) / numbersAffected.length);	
}
//getPayoutRatio( getRow1().concat(getRow2()) );

function getBetAmount(profit, lossAmount, payoutRatio, numBets){
	return (profit+lossAmount) / (payoutRatio * numBets);
}

function minBetAmount(groupName){
	debug("minBetAmount for "+groupName);
	var numbersAffected = rand_set.groups[groupName];
	if(numbersAffected != null){
		var payoutRatio = getPayoutRatio(numbersAffected);
		debug("payoutRatio "+payoutRatio);
		var ratioMax = getTableMaxForRatio(payoutRatio+1);
		debug("ratioMax "+ratioMax);
		var numSpinsTillHit = rand_set.numSpinsTillHit(rand_set.keys.indexOf(groupName));
		debug("numSpinsTillHit "+numSpinsTillHit);
	}
}

function percentReturn(startVal, endVal){
	return (endVal - startVal) / startVal;
}

function getPercentVal(amount, percent){
	return amount * percent;
}

function roundToMinChip(val){
	var minChip = app.data.chip_values[0];
	var fract = (val / minChip) % 1;
	var int = Math.trunc(val / minChip);
	if(fract > 0){		
		val = int*minChip + minChip;
		//debug("rounding: "+fract.toFixed(2)+" to "+val.toFixed(2));
	}
	return val;
}
//print list of total lost and bet amount for ratio
function printBetList(groupName, profit, probStop){
	var numbersAffected = rand_set.groups[groupName].length;
	var payoutRatio = getPayoutRatio(rand_set.groups[groupName]);
//	var tableMax = getTableMaxForMultiple(payoutRatio);
	var prob = 	(37-numbersAffected)/37;
	if(app.data.table_type == "american")
		prob = (38-numbersAffected)/38;	
	var numLosses = getLossCountAtProb(prob, probStop);
	var lossAmount = 0;
	for(var i=0; i<numLosses; i++){
		var bet = getBetAmount(profit, lossAmount, payoutRatio, 1);
		if(bet < app.data.chip_values[0]){
			debug("min calculated bet of "+bet+ " is less than table chip minimum. starting bets at min value of "+app.data.chip_values[0]);
			bet = app.data.chip_values[0]; //has to be at least min chip value
		}
		bet = roundToMinChip(bet);
		debug("numLosses: "+i+" betAmount: "+bet.toFixed(4)+" totalLosses: "+lossAmount.toFixed(2)+" probLoss: "+getProbAtLossCount(prob, i).toFixed(3));
		lossAmount += bet;
	}
	return lossAmount;
}

//print list of total lost and bet amount for ratio
function printBetListAtCurrentLossCount(groupName, profit, probStop){
	var numbersAffected = rand_set.groups[groupName].length;
	var payoutRatio = getPayoutRatio(rand_set.groups[groupName]);
	var tableMax = getTableMaxForMultiple(payoutRatio); //use tableMax to determine bet amount
	var prob = 	(37-numbersAffected)/37;
	if(app.data.table_type == "american")
		prob = (38-numbersAffected)/38;	
	var numLosses = getLossCountAtProb(prob, probStop);
	if(numLosses <= 0) return -1;
	var numSpinsLoss = rand_set.numSpinsLoss(rand_set.keys.indexOf(groupName));
	var lossAmount = 0;
	for(var i=numSpinsLoss; i<numLosses; i++){
		var bet = getBetAmount(profit, lossAmount, payoutRatio, 1);
		if(bet < app.data.chip_values[0]){
			debug("min calculated bet of "+bet+ " is less than table chip minimum. starting bets at min value of "+app.data.chip_values[0]);
			 bet = app.data.chip_values[0]; //has to be at least min chip value
		} else
			bet = roundToMinChip(bet);
		debug("numLosses: "+i+" betAmount: "+bet.toFixed(4)+" totalLosses: "+lossAmount.toFixed(2)+" probLoss: "+getProbAtLossCount(prob, i).toFixed(3));
		lossAmount += bet;
	}
	if(tableMax < lossAmount){
		alert("Table max exceeded "+max+" for bet "+groupName);
	}
	return lossAmount;
}

//returns -1 on error, 0 if not possible
function betListProfitAtPercentOfBalance(groupName, perct, probStop){
	if(typeof(groupName) != "string") return -1;	
	var numbersAffected = rand_set.groups[groupName].length;
	var payoutRatio = getPayoutRatio(rand_set.groups[groupName]);	
	var tableMax = getTableMaxForMultiple(payoutRatio); //use tableMax to determine bet amount
	var lossAmountNeeded = balance * perct;
	if(lossAmountNeeded > tableMax){
		debug("percent of balance exceeds tableMax");
		return -1;
	}
	var minChipVal = app.data.chip_values[0];	
	var profit = minChipVal;
	
	var prob = 	(37-numbersAffected)/37;
	if(app.data.table_type == "american")
		prob = (38-numbersAffected)/38;	
	var numLosses = getLossCountAtProb(prob, probStop);
	var numSpinsLoss = rand_set.numSpinsLoss(rand_set.keys.indexOf(groupName));	
	function getLossTotal(){
		var lossAmount = 0;
		for(var i=numSpinsLoss; i<numLosses; i++){
			var bet = getBetAmount(profit, lossAmount, payoutRatio, 1);
			if(bet < minChipVal){
				 bet = minChipVal; //has to be at least min chip value
			} else
				bet = roundToMinChip(bet);
			lossAmount += bet;
		}
		return lossAmount;
	}
	var lossAmount = 0;
	var done = false;	
	while(!done){
		lossAmount = getLossTotal();
		if(lossAmount < lossAmountNeeded){
			profit += minChipVal;
		} else {
			done = true;
		}
		if((profit / payoutRatio) > balance) return -1; //hack to help figure out dead lock
	}
	return profit - minChipVal;	//returned 109
}

var bets = {
/*
elim_id: {
	value : v,
	ratio : ratio,
	affected : affected
}
 */
		data: {},
		_data: {},
		precision: 1,
		numsWithBets: [],   //all numbers covered are eventually in this array
		betHistory: [],
		getNumBets: function(){
			return bets.numsWithBets.length;
		},
		searchBetsForMatch: function(ratio, affected){
			for(var i=0;i<data.length;i++){
				if(ratio == data[i].ratio & affected.length == data[i].affected.length){
					if(JSON.stringify(data[i].affected)==JSON.stringify(affected)){
						return i;
					}
				}
			}
			return -1;
		},
		processNumsAffected: function(){
			bets.numsWithBets = [];
			for(key in bets.data){
				var b = bets.data[key];
				for(var i=0; i<b.affected.length; i++){
					if(bets.numsWithBets.indexOf(b.affected[i]) == -1)
						bets.numsWithBets.push(b.affected[i]);
				}	
			}
		},
		addBet: function(k, b){
			bets.data[k] = b;
			bets.processNumsAffected();
		},
		getBetValue: function(k){
			return bets.data[k].value;
		},
		addBetValue: function(k, v){
			bets.data[k].value += v;
			bets.data[k].value = (bets.data[k].value).toFixed(bets.precision)/1;
		},
		updateBetValue: function(k, v){
			bets.data[k].value = v;
		},
		getTotalValue: function () {
			var v = 0;
			for (key in bets.data) {
				v += bets.data[key].value;
			}
			return v.toFixed(bets.precision)/1;
		},
		lastBetTotalValue: function(){//add value of previous bet
			var v = 0;
			for (key in bets._data) {
				v += bets._data[key].value;   //debug("lastBetTotalValue: " + v);
			}
			return v;
		},
		removeAllBets: function(){
			bets.data = {};
			bets.numsWithBets = [];
		},
		removeBet: function(id){
			delete(bets.data[id]);
			bets.processNumsAffected();
		},
		recall: function(){
			for (key in bets._data) {
				bets.addBet(key, bets._data[key]);
			}
			balance -= bets.getTotalValue();			
			//updateBalance();
		},
		pushHistory: function(){
			var r = rand_set.numHistory[rand_set.numHistory.length-1];
			var s = JSON.stringify(bets.data); //have to copy object else objlit gets passed by ref
			var b = JSON.parse(s);
			var _bets = [];
			for(key in b){	
				var bet = {
					value : b[key].value,
					ratio : b[key].ratio,
					affected : b[key].affected,
					numberHit: r,
					spinCount: rand_set.numHistory.length
				};
				_bets.push(bet);
			}
			bets.betHistory.push(_bets);
			//debug("bet ary pushed to history");
		},
		save: function(){ //save Last Bet
			var s = JSON.stringify(bets.data); //have to copy object else objlit gets passed by ref
			bets._data = JSON.parse(s);
		},
		saveNclear: function(){
			bets.save();
			bets.data = {};
		},
		doubleBets: function(){
			var newBal = balance;
			for(key in bets.data){
				newBal -= bets.data[key].value * 2;
			}
			if(newBal >= 0){
				for(key in bets.data){
					var el = document.getElementById(key);					
					var v = bets.data[key].value;
					var val = (bets.data[key].value * 2).toFixed(bets.precision)/1;
					if(typeof(el) != null) el.innerHTML = val;					
					bets.data[key].value = val;
					balance -= v;
				}
				updateBalance();
			} else {
				alert("Insufficient funds");
			}
		},
		getPayOutKey: function(k){ //where key is elem_id
			if(typeof(bets.data[k]) == 'object')
				return (bets.data[k].value * bets.data[k].ratio).toFixed(bets.precision)/1;
			return 0;
		},
		getPayOut: function(n) {  //debug("getPayout: "+n);  get payout for number n hitting
			var p = 0;
			for (key in bets.data) {
				var m = getMatch(bets.data[key].affected, n);
				//debug("n:"+n+" ,"+elem_ids[i].affected+" m:"+m);

				if (m != -1) {
					p += (bets.data[key].value * bets.data[key].ratio).toFixed(bets.precision)/1;   
					//num match calc payout and sum
				}
			}
			return p;
		},
		getPayOuts: function(){
			var keys = Object.keys(bets.data);
			if(keys.length == 1){ //only 1 bet
				return bets.getPayOutKey(keys[0]);
			}
			if(keys.length == 0)
				return 0;
			var payouts = [];
			for(var i=0; i<keys.length;i++){
			  payouts.push(bets.getPayOutKey(keys[i]));
			}
			return payouts;
		},
		reset: function(){
			bets.data = {};
			bets._data = {};
			bets.numsWithBets = [];   //all numbers covered are eventually in this array
			bets.betHistory = [];			
		}
};

function reset() {
	try {
		//debug("reset ");
		betting_enabled = true;
		bets.reset();
		et.reset();		
		//remove all bets, and save balance! and update balance label for casino!!!
        $("#amountLostSinceLastWin").html("");
        $("#payout").html("");
        $("#betTotal").html("");
        $("#profitOnWin").html("");
        
		rand_set.reset();
		//$("#bet_table").show();
		//$("#stat_table").hide();
		//clear last #'s that came in
		//document.getElementById("last10").innerHTML = "";
		$("#stat_app").load("/assets/html/stat.htm", function(){
			loadStatTable();
		});
		loadTable();
		loadChips();
		selected_chip = null;
		$("#roulette_game").show();
		$("#bet_tables").html("");
		$("#bet_tables").hide();
		//hide_all_bet_labels();
	} catch(e) {
		debug("error: "+e);
	}
}



