function uploadUI(msg){
//	var msg = '';
	var label = '<div class="hd"><h2>File Upload</h2></div>';
    var ls  = '<div class="content"></div>';
    var form = 'Zip a file and upload here!<br /><form action="index.php" method="post" enctype="multipart/form-data"><input type="file" name="zip" id="file"/><br /><input type="submit" name="submit" value = "Submit"/></form>';
	var response = '<br /><div class="msg"><div class="hd"><h2>Response</h2></div><div class="content">'+msg+'</div></div>';
	var uploadUI = '<div class="msg" style="width: 320px; float:left;">'+label+form+response+'</div>';
	return uploadUI;
}	

if(typeof(app) == 'undefined'){
	app = {};
}

if(typeof(app.widget) == 'undefined'){
	app.widget = {};
}

app.widget.newDialog = function(o, _AddedCallback){
/*	var o = {
    	dialogID: "dialog",
    	dialogTitle: "JsonViewer",
    	content: "",
    	url: "",
    	autoOpen: true,
    	width: 810,
    	height: 600
	};*/
	var parentID = "";
	//var parentID = "Content";
	//var parentID = "LeftSidebar";
    var dialogID = "DefaultID";
    var dialogTitle = "Default Title";
    var content = "";
    var url = "";
    var autoOpen = true;
    var width = 810;
    var height = 600;
    var resizable = true;
    var contentID = "dialogContent";
	if( isObject(o) ){
		if( isDefined(o.width) ){
			width = o.width;
		}
		if( isDefined(o.height) ){
			height = o.height;
		}
		if( isString(o.id) ){
			dialogID = o.id;
		}
		if( isString(o.title) ){
			dialogTitle = o.title;
		}
		if( isString(o.url) ){
			url = o.url;
		}
		if( isDefined(o.content) ){
			content = o.content;
		} else {
			//content = '<iframe id="'+contentID+'" src="'+url+'" width="100%" height="90%"></iframe>';
			content = '<iframe name="'+dialogID+'" src="'+url+'" width="'+width+'" height="'+height+'"></iframe>';
		}
		if( isTrue(o.autoOpen)){
			autoOpen = true;
		}
		if( !isTrue(o.resizable)){
			resizable = false;
		}
	}
    var dialogHTML = '<div id="'+dialogID+'" title="'+dialogTitle+'">';
    dialogHTML += content;
    dialogHTML += '</div>';
	if( $('#'+dialogID).length > 0 ){
		console.log("Updating Existing Log");
		$('#'+dialogID).html(content).dialog().show();
	}else{
		console.log("Adding new Dialog");
	    $('body').append(dialogHTML);
	    if( isDefined(o.content) )
	    	$('#'+dialogID).dialog({autoOpen: autoOpen, width: width, height: height, resizable: resizable});
	    else
	       	$('#'+dialogID).dialog({autoOpen: autoOpen, width: 'auto', height: 'auto', resizable: resizable});
	}
	//The .load is for images I think but maybe a callback was needed
	//I think the operation is blocking until elements are added forthe most part.
	//
	//I suppose I could wait for open on the iFrame's content?
	if(isFunction(_AddedCallback)){
		console.log("Dialog Added");
		//$('#'+dialogID).load(_AddedCallback);
		_AddedCallback();
	}
};

app.widget.jsonView = function(_CallBack){
	var _content = "";
	_content += '<div style="position:absolute;left:10px;background-color: #155e8b; width: 360px;" id="tree"></div>';
	_content += '<div style="position:absolute;left:400px" id="jform"></div>';
	app.widget.newDialog({
		id: "JsonView",
		title: "Json Viewer",
		content: _content,
		autoOpen: true,
		width: 810,
		height: 600
	}, _CallBack);	
};

app.widget.jsonViewer = function(_data){
 	app.widget.jsonView(function(){
		var _json = _data;
		if(isString(_data)) _json = JSON.parse(_data);
		JSONeditor.start('tree','jform',_json,false,false);
	});	
};
app.widget.jsonEditor = function(_data){
 	app.widget.jsonView(function(){
		var _json = _data;
		if(isString(_data)) _json = JSON.parse(_data);
		if(!isDefined(_data)) _json = {};
		JSONeditor.start('tree','jform',_json,false,true);
	});	
};
app.widget.addTab = function(o){
	var tabHID = "#tabsHeader";
	var tabCID = "#tabs";
	
	//o = {title: "AddTab", id: "newTab1", content: "Tab Content"};
    var dialogID = "DefaultID";
    var dialogTitle = "Default Title";
    var content = "";
    var width = 810;
    var height = 600;
    var contentID = "dialogContent";
	if( isObject(o) ){
		if( isString(o.id) )
			dialogID = o.id;
		if( isString(o.title) )
			dialogTitle = o.title;
		if( isDefined(o.content) )
			content = o.content;
	}
	$(tabCID).tabs( "destroy" );
	$(tabHID).append('<li><a href="#'+dialogID+'">'+dialogTitle+'</a></li>');

	if( isString(o.url) ){
		//content = '<div id="'+dialogID+'"><iframe id="'+contentID+'" src="'+o.url+'" width="100%" height="90%"></iframe></div>'; //url must be on this server, cross domain policy
		content = '<div id="'+dialogID+'"><iframe id="'+contentID+'" src="'+o.url+'" width="100%" height="'+height+'"></iframe></div>'; //url must be on this server, cross domain policy
	}
	else
		content = '<div id="'+dialogID+'"><div id="'+contentID+'">'+content+'</div></div>';
	$(tabCID).append(content);
	$(tabCID).tabs();
	if(isObject(o) && isFunction(o.AddedCallback)){
		console.log("Dialog Added");
		//$('#'+dialogID).load(_AddedCallback); //use the load jquery callback.
		o.AddedCallback({containerID:dialogID, contentID: contentID});
	}
};

app.widget.createJqTabGroup = function(){
	if($('#Content').html() != ""){
		$('#Content').html('<div id="tabs"><ul id="tabsHeader"></ul><div class="cleared"></div></div>');
		$( "#tabs" ).tabs();
		$( "#tabs" ).click(function(e){
			//console.log(e.target); //<a id="ui-id-3" class="ui-tabs-anchor" href="#ImgView" role="presentation" tabindex="-1">
			if(e.target.href == "#ImgView"){
					$('#UploadBlock').show();
					$('#SessionBlock').hide();
			}
			if(e.target.href == "#LogView"){
					$('#UploadBlock').hide();
					$('#SessionBlock').show();
			}
			//console.log("Tab Clicked: "+e.target+ "type: "+typeof(e.target)); console.log(e.target.indexOf("#")); dump(e.target.toString().indexOf('#'));
			/*if(e.target.toString().indexOf('ImgView') != -1){
			}
			if(e.target.toString().indexOf('LogView') != -1){
			}*/
		});
		$('#UploadBlock').hide();
	}
	//Hide SideBar Blocks....
	//$('#UploadBlock').hide();
	//$('#SessionBlock').hide();
	
	/*app.widget.addTab({
	    id: "LogView",
	    title: "LogView",
	    content: "testing..."
	    //,url: "http://bluemountaintechnologies.com"
	});*/
	/*app.widget.addTab({
	    id: "ImgView",
	    title: "ImgView",
	    content: "testing..."
	    //,url: "http://bluemountaintechnologies.com"
	});*/
};



//var logs = null;
//var AssetDir = '/BMTmobile/assets/images';
//console.log(path[1]); //index.php
//console.log(path[2]); //controllerName
//console.log(path[2]); //controllerMethod
app.widget.createUserAgentImg = function(platform, browser, isMobile){
	var OS_IMG = '';
	switch(platform){
		case "Unknown Windows OS":
		case "Windows 7":
		case "Windows":
			OS_IMG = 'win.png';
			break;
		case "Linux":
			OS_IMG = 'lin.png';
			break;
		case "Android":
			OS_IMG = 'and.png';
			break;
		case "Mac OS X":
		case "Macintosh":		
		case "iOS":		
			OS_IMG = 'mac.png';
			break;
	}
	console.log("platform: "+platform);
	var BROWSER_IMG = "";
	switch(browser){
		case "Android":
			BROWSER_IMG = 'and.png';		
		case "Internet Explorer":
			BROWSER_IMG = "int.png";
			break;
		case "Opera":
			BROWSER_IMG = "opr.png";
			break;
		case "Safari":
			BROWSER_IMG = "saf.png";
			break;
		case "Chrome":
			BROWSER_IMG = "chr.png";
			break;
		case "Konkorer":
			BROWSER_IMG = "kon.png";
			break;
		case "Netscape":
			BROWSER_IMG = "net.png";
			break;
		case "Firefox":
			BROWSER_IMG = "fir.png";
			break;
	}
	
/*
.User_Agent_Img{
	width: 32px;
	height: 32px;
	background-repeat: no-repeat;
	display: block;
	position: relative;
}
	background-image: url("mac.png");
	background-image: url("win.png");
	background-image: url("lin.png");
	background-image: url("and.png");
	background-image: url("kon.png");
	background-image: url("net.png");
	background-image: url("int.png");
	background-image: url("saf.png");
	background-image: url("ope.png");
	background-image: url("chr.png");
	background-image: url("fir.png");
 */	
	//inherits box from User_Agent_Img  
	return '<img class="User_Agent_Img" src="'+AssetDir+'images/'+BROWSER_IMG+'" style="background-image: url('+AssetDir+'images/'+OS_IMG+')" />';	
};

app.widget.createLogTable = function(o){ 
    //$("#LogView").html("");
    if(typeof(o) == 'object'){
        var tbl = "<h2 class='LogViewTitle'>Log View</h2>";
        tbl += "<table class=\"LogViewTable\">";
        //tbl += '<tr class="LogViewHeadTable"><td>TimeStamp</td><td>Message</td><td>Data</td><td>id</td></tr>';
        tbl += '<tr class="LogViewHeadTable"><td>TimeStamp</td><td>Message</td><td>Data</td></tr>';
        for(var i=0;i<o.length;i++){
            if(isEven(i)){
                tbl += '<tr class="LogViewEvenTable">';
             } else {
                tbl += '<tr class="LogViewOddTable">';
             }
            tbl += "<td>"+getDateString(o[i].TimeStamp)+"</td>";
            tbl += "<td>"+o[i].Msg+"</td>";
            if(o[i].Data == null)
                tbl += "<td></td>";
            else{
                //var s = JSON.stringify(o[i].Data);
                tbl += '<td><a href="javascript:void(0)" onclick="viewLogData('+i+')">view</a></td>';
            }
            //tbl += "<td>"+o[i].id+"</td>";
            //tbl += "<td>"+o[i].Session_id+"</td>";
            tbl += "</tr>";             
        }
        tbl += "</table>";
        //$("#LogView").html(tbl);
        return tbl;
    }
    return false;
};


app.widget.getLogs = function(session_id){	
	app.controllers.base.getLogs({
		session_id: session_id,
		callBack: function(d){
			//app.widget.createLogTable(d);
			console.log("ready to view json");
			app.widget.jsonViewer(d);
		}
	});
};
app.widget.getSessions = function(session_id){
	app.controllers.base.jsonActiveSessions(function(data){
       app.widget.createSessionTable(data.Sessions);
	});
};
app.widget.createSessionTable = function(Sessions){
	$("#ActiveSessions").html("");
    var tbl = ""; //"<h2 class='LogViewTitle'>Active Sessions</h2>";
    tbl += '<table class="LogViewTable">';
    tbl += '<tr class="LogViewHeadTable"><td>Platform</td><td>IP Address</td></tr>';	 
    if( isObject(Sessions) ){
        for(var i=0;i<Sessions.length;i++){
        	//if( (unix_time() - Sessions[i].last_activity) <= 7200 ){ //<7200 = 2hrs
	            if(isEven(i)){
	                tbl += '<tr class="LogViewEvenTable">';
	             } else {
	                tbl += '<tr class="LogViewOddTable">';
	             }
	            tbl += '<td>'+app.widget.createUserAgentImg(Sessions[i].platform, Sessions[i].browser)+'</td><td><a href="javascript:void(0)" onclick="app.widget.getLogs(\''+Sessions[i].id+'\')">'+Sessions[i].ip_address+'</a></td>';             
	            tbl += "</tr>";
			//}
        }
    }
    tbl += "</table>";
	$("#ActiveSessions").html(tbl);        
    return false;
};

app.widget.AccessLogView = function(){
	app.widget.newDialog({
		id: "AccessLogView",
		title: "Access Log",
		url: routerURL+"base_view/gCrudAccess/",
		autoOpen: true,
		width: 810,
		height: 600	
	});	
};
app.widget.LogView = function(){
	app.widget.newDialog({
		id: "LogView1",
		title: "Logs",
		url: routerURL+"base_view/gCrudLog/",
		autoOpen: true,
		width: 810,
		height: 600
	});	
};
app.widget.UserView = function(){
	app.widget.newDialog({
		id: "LogView1",
		title: "Logs",
		url: routerURL+"/base_view/gCrudUser/",
		autoOpen: true,
		width: 810,
		height: 600
	});	
};
app.widget.viewLogData = function(_i){
 	app.widget.jsonView(function(){
		console.log("Dialog Callback!");
		var _json = JSON.parse(logs[_i].Data);
		JSONeditor.start('tree','jform',_json,false,false);
	});
};

app.widget.createLogTableWithFileLogs = function(){ 
    //$("#LogView").html("");
    if(typeof(logs) == 'object'){
        var tbl = "<h2 class='LogViewTitle'>Log View</h2>";
        tbl += "<table class=\"LogViewTable\">";
        //tbl += '<tr class="LogViewHeadTable"><td>TimeStamp</td><td>Message</td><td>Data</td><td>id</td></tr>';
        tbl += '<tr class="LogViewHeadTable"><td>TimeStamp</td><td>Message</td><td>Data</td></tr>';
        for(var i=0;i<logs.length;i++){
            if(isEven(i)){
                tbl += '<tr class="LogViewEvenTable">';
             } else {
                tbl += '<tr class="LogViewOddTable">';
             }
            tbl += "<td>"+getDateString(logs[i].TimeStamp)+"</td>";
            tbl += "<td>"+logs[i].Message+"</td>";
            if(logs[i].Data == null)
                tbl += "<td></td>";
            else{
                //var s = JSON.stringify(logs[i].Data);
                tbl += '<td><a href="javascript:void(0)" onclick="app.widget.viewLogData('+i+')">view</a></td>';
            }
            //tbl += "<td>"+logs[i].id+"</td>";
            //tbl += "<td>"+logs[i].Session_id+"</td>";
            tbl += "</tr>";             
        }
        tbl += "</table>";
        //$("#LogView").html(tbl);
        return tbl;
    }
    return false;
};
var graphics = null;
function updateZoomImage(index){
    $('#zoom_image').attr('src', graphics.images[index].url);
    $('#zoom_image').click(function(){ getGraphicUploadDetail(index); });    
}
function getGraphicUploadDetail(_index){
    var fName = graphics.images[_index].url.slice(graphics.images[_index].url.lastIndexOf('/')+1);	
	app.controllers.base.getGraphicUploadDetailFromFileName(fName, jsonViewer);
}
app.widget.createGraphicsView = function(){
    function addImage(index){
        try{
            return '<img class="tn" src="'+graphics.images[index].thumb_url+'" onclick="updateZoomImage('+index+')"/>';
        } catch(e){
            console.error("Error processing index: "+index);
            //console.error("Error: "+e);
        }
    }
    var zoom_image = '<img id="zoom_image" width="480" height="360" src="'+graphics.images[0].url+'"/>';
    $('#zoom_box').html(zoom_image);
    $('#zoom_image').click(function(e){
        console.log(e.target.src); //echo src property of image
        //getGraphicUploadDetail(0); 
    });
    var img = '';
    for(var i=0;i<graphics.images.length;i++){
        img += addImage(i);
    }
    $('#tn_box').html(img);
};


//TODO need to clear the view if data == null 
app.widget.getGraphics = function(){		
	app.controllers.base.getImages(app.widget.createGraphicsView);	
};
function getFileNameFromIndex(_index){
	return graphics.images[_index].url.slice(graphics.images[_index].url.lastIndexOf('/')+1);
}
function getFileNameFromImgSrc(img_src){
	return img_src.slice(img_src.lastIndexOf('/')+1);
}

app.widget.update_graphicDetails = function(index){
    function getLabel(k, v){
        return '<label>'+k+': '+v+'</label><br/>';
    }
    //'.ImgInfoBox' class name of details box
    //var s = '';
	var s = '<img src="'+AssetDir+'images/del.png" alt="Delete Image" onclick="DelImage('+index+')"/>';
	var o = graphics.images[index];
    for( key in o ){
        s += getLabel(key, o[key]);
    }
    $('#info_box').html(s);
    $('#zoom_image').attr('src', graphics.images[index].url);
};


app.widget.tiMobileFrame = function (_url, _title, _id, type){
	var id = "tiAndroid";
	var width = 480;
	var height = 800;
	if(typeof(type) == "string"){
		if(type == "ipad"){
			id = 'tiIpad';
			width = 768;
			height = 1024;			
		}
		if(type == "iphone"){
			id = 'tiIphone';
			width = 320;
			height = 480;			
		}
	}
	if(typeof(id) != 'undefined')  	
		id = _id;
		
	if(typeof(_url) == 'string'){
		app.widget.newDialog({
			id: _id,
			title: _title,
			url: _url,
			autoOpen: true,
			width: width,
			height: height,
			resizable: false
		});
		console.log("Adding TiDialog");
		console.log('to run code call evalIFrame("'+_id+'", "code");');
	}		
}

var appData = null;
function appLinkClicked(e){
	try{
		alert("click event data: "+e.id); //click event data: javascript:void(0);
		var id = e.id;
		if(typeof(id) == 'string'&& typeof(appData) == 'object'){ 
			var patt = /[0-9][0-9]*/g;
			var _offset = patt.exec(id);
			var _url = appData.apps.path + "/" + appData.apps.children[_offset];
			var _title = appData.apps.children[_offset];
			var _id = 'Ti' + id;
			console.log("url: "+_url);
			app.widget.tiMobileFrame(_url, _title, _id, "iphone");
		}
	} catch(e){
		console.error("Error: "+e);
	}
}
app.widget.createMobileAppView = function(){
 	app.controllers.base.getMobileApps(function(data){
 		appData = data;
 		//app.widget.jsonViewer(appData);
 		//alert("message: "+appData.message);
		try{ 		
	 		$('#AppBox').html('');
 			if(appData.apps.total != appData.apps.children.length){ 
 				console.log("Server error data.total != children.length"); 
 				return false; 
 			} else{
 				//if( typeof(appData.apps.paging) != '') { // }
				var s = '';
				for(var $i=0;$i<appData.apps.total; $i++){						
					var click_label = appData.apps.children[$i];
					//console.log("app label "+click_label);
					s += '<a href="JavaScript:void(0);" onclick="appLinkClicked(this);" id="appDir'+$i+'" >'+click_label+'</a></br>';
				}
				//alert("TiDemos: "+s);
				$('#AppBox').html(s);
			}
		} catch(e){
			console.error("Error: "+e);
		}
 	});
};