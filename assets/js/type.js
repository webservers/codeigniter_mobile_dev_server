Array.prototype.inArray = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}
if (typeof($.inArray()) != 'function'){	
   alert('JQuery not availible prototyping');   
}
var types = {
	Number: 1,
	String: 2,
	Function: 3,
	Object: 4,		
	Window: 5,
	Array: 6,
	Null: 0,
	Undefined: -1,
}
var type = function(o){
	var typeS = null;
	var typeI = null;
	
	var asInt = function(t){
		//info(typeS, 92);
		typeS = typeof(t);
		switch (typeS){
			case "object": 
				typeI = types.Object;
				break;			
			case "function": 
				typeI = types.Function;
				break;			
			case "number":
				typeI = types.Number;
				break;			
			case "string":
				typeI = types.String;
				break;			
			case "undefined":
				typeI = types.Undefined;
				break;			
			default: 
				typeI = -1; //means unIdentified
				break;
        }
		//support JS object names using type
		if(typeI > types.Function){
			typeS = getObjectType(t); //returns a string
			//convert from string to int in types enum
			if(typeI > types.Function){
				//info("Type is "+typeS+" for "+t.toString(), 114);
				var a = getKeys(types); //a = array of strings
				for(var i=0;i<a.length;i++){
					//if(t == i)
					if(typeS == a[i]){
						//info("Match: "+a[i],108);	//return the string if == to property name
						typeI = types[a[i]];
					} else {
						//info("Skipping type: "+a[i]+" val: "+types[a[i]], 122);
					}
				}
				if(i == (a.length -1))
					info("Type is "+typeI+" for "+t, 126);			
			}		
		}
		//info(typeI, 129);
		return typeI;
	}

	//converts from type.types int to string
	var asString = function(t){
		typeI = asInt(t); 
		//asInt updates typeS var
		return typeS;
	}
		
	var Thingtype = function(t){
		if(t===null)return "[object Null]"; // special case
		return Object.prototype.toString.call(t);
	}
	var getObjectType = function(t){

		//info(Thingtype, 28);
		var tt = Thingtype(t);
		var r = /^\[[object\s]+([A-z0-9]+)\]$/;	
		var tst = r.test(tt);
		if(tst){
			var v = r.exec(tt);
			if(typeof(v[1]) != "undefined"){
				//info(v[1], 156);
				return v[1];
			}
		}
		return false;
	}
	
	//verbose variable print out
	var type_info = function(t, key){
		if((asInt(key) == types.object) && (asInt(t) == types.object))
			info("type:"+asString(t.key)+", value: "+t.key+", name: "+key, 194);
		else if(type(t) == types.object){
			info("type:"+t.toString()+", value: "+t, 196);
		}
		else
			info("type:"+type(t)+", value: "+t, 200);
	}

	var getTypeFromObject = function(t, key){
		info("GetTypeFromObject", 204);
		var ty = asInt(t);
		if((ty == types.Object || ty == types.Function)){
			type_info(ty);
		
			if(asInt(key) != types.Undefined){
				switch (ty){
					case types.Object: 
						info("object: "+t.key, 212);
						return "object";
					case types.Function: 
						info("function: "+t.key, 215);
						return "function";
					default: 
						//info("type:"+type(o.key)+", value: "+o[key]+", name: "+key, 102);
						break;        
				}
			}
		}
	}	
	//try to support public->private calls via this keywork.
	this.get = function(t){
		//if(asInt(t) == types.Function)
		if(this.prototypeFunctions.inArray(t))
			return t();
		else
			return t;
		return this;
	}
	this.set = function(t, v){
		if((this.prototypeFunctions.inArray(t)) && (asInt(v) > types.Null))
			t(v);
		else if(asInt(v) > types.Null)
			t = v;
		return this;
	}
	//alert(getKeys(this));
	var testOverRide = "not Overriden";
	
	this.getTypes = function(b, c){
		if(c)
			var a = getKeys(types);
		else
			var a = getKeys(this.types);
		if(a.length > 0 && b){
			for(var i=0;i<a.length;i++){
				info("type: "+a[i]+" val: "+types[a[i]], 215);
			}
			if(c && b)
				info("types: "+asString(types), 218);			
			return a;
		} else {
			if(c && b)
				info("types: "+asString(types), 222);
			else if(b)
				info("this.types: "+asString(this.types), 224);
			
		}
		return this;
	}
	
	
	/*
	this.prototypeVars = new Object();
	this.prototypeVars.types = types;
	this.prototypeVars.typeS = typeS;
	this.prototypeVars.typeI = typeI;
	*/
/*	this.prototypeFunctions = new Object();		
	this.prototypeFunctions.asInt = asInt;	
	this.prototypeFunctions.asString = asString;	
	this.prototypeFunctions.Thingtype = Thingtype;	
	this.prototypeFunctions.getObjectType = getObjectType;	
	this.prototypeFunctions.type_info = type_info;	
	this.prototypeFunctions.getTypeFromObject = getTypeFromObject;*/
	
	//return asInt(o); //return int as default
	//return typeS; //return typeof result, this will break further down the chain
	//return asString(o); //return compiled Object definition string		
	//return getObjectType(o);

	//info(this.getTypes(), 245);
	//info(asString(o), 246);		
	return this;
}