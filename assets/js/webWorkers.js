function dedicatedWorker(){
	function getWorkerObject(){
		try{
			if(!! window.Worker) return window.Worker;
			else throw "no window.Worker Object Detected!";
		}catch(e){ console.error("Error: "+e); }
		return false;
	}
	function hasWorkerSupport(){
		if(getWorkerObject()){
			console.log("This Browser supports WebWorkers.");
			return true;
		} else {
			console.log("This Browser doesn't support WebWorkers.");
		}
		return false;
	}
	
	function workerMessageCallback(e){
		console.log('WorkerCallback: '+e.data.value);
		if(e.data.errorMessage){
			console.error('WorkerCallback: '+e.data.errorMessage);
		}
	}
	function workerErrorCallback(e){
		e.preventDefault(); //prevent default error handling
		console.error(e.message + ' (' + e.filename + ':' + e.lineno + ')' );
	}
	if(hasWorkerSupport()){
		var worker = new getWorkerObject('test.js');
		//Event Listeners seem to work work more consistantly 
		//with older browsers.
		worker.addEventlistener('message', workerMessageCallback, false); //==	worker.onmessage = workerCallback;
		worker.addEventlistener('error', workerErrorMessageCallback, false);
		worker.postMessage({ value: 500 });
	}
}

function sharedWorker(){
	
}
