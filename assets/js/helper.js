var device = {
		screen: {
			width: window.innerWidth, 
			height: window.innerHeight
		}
};

function includeJS(filename){
 var fileref=document.createElement('script');
 fileref.setAttribute("type","text/javascript");
 fileref.setAttribute("src", filename);
 //if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref);
}
function includeCSS(filename){
 var fileref=document.createElement("link");
 fileref.setAttribute("rel", "stylesheet");
 fileref.setAttribute("type", "text/css");
 fileref.setAttribute("href", filename)
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

//titanium Iframe execution code
function getIframeVar(_windowName, _id){
	var id = 'tiIpad';   if(typeof(id) != 'undefined')  	id = _id;
    if(typeof(_windowName) != 'undfined')
    	return document.getElementById(id).children[id].contentWindow[_windowName];
    return false;
}
function evalIframe(_code, _id){
	var id = 'tiIpad';   if(typeof(id) != 'undefined')  	id = _id;    
    return document.getElementById(_id).children[_id].contentWindow.eval(_code);
}

function getDateString(_dStr){
    var r = _dStr.replace('-', '/');
    r = r.replace('-', '/');
    return dateFormat(new Date(r), "mm/dd/yy hh:mm");
}

function debug(msg) {
  //alert(msg);
  console.log(msg);
}

function isEven(n){
	if((n/2).toString().indexOf('.') == -1)
		return true;
	return false;
}
function isEven(someNumber){
    return (someNumber%2 == 0) ? true : false;
}

function fPart(n){
	return n.ToString().substring(n.toString().indexOf('.')+1, n.toString().length);
}
function wPart(n){
	return n.toString().substring(0, n.toString().indexOf('.') -1);
}
function deg_rad(deg){
	return deg * Math.PI / 180;
}

//return unix timeStamp
function unix_time(){
	return Math.round((new Date()).getTime() / 1000);
}

function isDefined(v){
	if(typeof(v) == 'undefined')
		return false;
	return true;
}
function notNull(v){
    if(isDefined(v) && v != null)
        return true;
    return false;
}
function isArray(v){
	return v.constructor.toString().indexOf("Array") > -1;
}
function isString(v){
	if(typeof(v) == "string")
		return true;
	return false;
}
function isNum(v){
    if(notNull(v) && typeof(v) == 'number')
        return true;
    return false;
}
function isTrue(v){
	if(typeof(v) == 'string'){
		if(v.toLowerCase() == "true")
			return true;
	} else if(typeof(v) == 'boolean' && v)
		return v;
	return false;	
}
function isFunction(v){
	if(notNull(v) && typeof(v) == 'function')
		return true;
	return false;	
}
function isObject(v){
	if(notNull(v) && typeof(v) == "object")
		return true;
	return false;
}
function array_pop(a, v){
	var index = a.indexOf(v);
	if (index > -1) {
	  a.splice(index, 1);
	}
	return a;
}

function num_exists(a, i){
	return a.indexOf(i);
}

function print_obj(o) {
	//debug("{");
	for (key in o) {
		if ( typeof (o[key]) != "OBJECT" | typeof (o[key] != "object"))
			stat_tab("    " + key + " : " + o[key]);
		else
			stat_tab( typeof (o[key]));
		//else
		//print_obj(o[key]);
	}
	//debug("}");
}

function dump(e){
	var str = "";
	if(typeof e == 'object'){
		for(key in e){
			debug("key: "+key+", value:"+e[key]);
		}
	}
	else debug("Value: "+e);
	return str;
}

/* Strip Chars Usage-------------------------------------------
var StripAlpha = Strip.Chars('A-Z');
var StripNumeric = Strip.Chars('0-9');
var StripSymbols = Strip.Chars('**');
var StripCustom = Strip.Chars('Characters I want removed!');

debug(StripAlpha.FromString("@4Te*st1"));   = @4*1
debug(Strip.Chars('A-Z**').FromString("@4Te*st1")); = 41
debug(Strip.Chars('0-9*').FromString("@4Te*st1"));  = @Test
debug(Strip.Chars('A-Z*').FromString("@4Te*st1"));  = @41
 */
var Strip = {
	invalidChars: '',
	//var invalidSequance = ["\U00a5","\U00a3","\U20ac"]; //these are Unicode char sequances
	FromString: function(CharString){
		var OutputString = CharString;
		var i = 0;
		invalidChars = this.invalidChars;
		//debug(invalidChars);
	    function clearInvalidChar(){
			var OffendingCharAtPosition = OutputString.indexOf(invalidChars[i]);	
			if(OffendingCharAtPosition != -1){   		
		      if(OffendingCharAtPosition == (OutputString.length-1))
	    		  OutputString = OutputString.slice(0, (OffendingCharAtPosition));
			  else if(OffendingCharAtPosition == 0)
	    		  OutputString = OutputString.slice(1);  	
			  else if(OffendingCharAtPosition < (OutputString.length-1)){
	    		  var ls = OutputString.slice(0, (OffendingCharAtPosition));
	    		  OutputString = ls + OutputString.slice(OffendingCharAtPosition+1);
	    	   }
	    	   return true;
	        }
	        return false;
	    }
		for(i=0;i<invalidChars.length;i++){
	        while( clearInvalidChar() ){
	         //loop through until all existance of current invalid char is removed
	        }
		}
		return OutputString;
	},
	Chars: function(_chars){
	   this.invalidChars = '';
	   Numbers =  "0123456789";
	   AlphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   Symbols = "` []{}#%^*+=_|~<>.,?!'-:;()$&@/\\"+'"';
	   if(typeof(_chars) == 'string'){
	    if(_chars.indexOf("A-Z") != -1){
	       this.invalidChars += AlphaUpper+AlphaUpper.toLowerCase();
	    		  var ls = _chars.slice(0, (_chars.indexOf("A-Z")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("A-Z")+3);	       
	       //debug(_chars);
	    }
	    if(_chars.indexOf("0-9") != -1){
	    		  var ls = _chars.slice(0, (_chars.indexOf("0-9")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("0-9")+3);
	    		  //debug(_chars);
	       this.invalidChars += Numbers;
	    }
	    if(_chars.indexOf("**") != -1){
	    		  var ls = _chars.slice(0, (_chars.indexOf("**")));
	    		  _chars = ls + _chars.slice(_chars.indexOf("**")+2);	             
	             //debug(_chars);
	       this.invalidChars += Symbols;
	    }
	    this.invalidChars += _chars;
	   }
	   return this;
	}
};
//Remove All But Numeric
function Numeric(e){
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('A-Z**').FromString(e.source.value);
		}
	}
}
//Remove All But AlphaNumeric
function AlphaNumeric(e){
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('**').FromString(e.source.value);
		}
	}
}
//Remove All But PhoneChars
function Phone(e){
	Symbols = "`[]{}#%^*+=_|~<>.,?!':;$&@/\\"+'"';
	if(typeof(e.source) == 'object'){
		if(typeof(e.source.value) != 'undefined'){
			e.source.value = Strip.Chars('A-Z'+Symbols).FromString(e.source.value);
		}
	}
}


function stripslashes (str) {
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Ates Goral (http://magnetiq.com)
  // +      fixed by: Mick@el
  // +   improved by: marrtins
  // +   bugfixed by: Onno Marsman
  // +   improved by: rezna
  // +   input by: Rick Waldron
  // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
  // +   input by: Brant Messenger (http://www.brantmessenger.com/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: stripslashes('Kevin\'s code');
  // *     returns 1: "Kevin's code"
  // *     example 2: stripslashes('Kevin\\\'s code');
  // *     returns 2: "Kevin\'s code"
  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}

function mouse_info() {
	var cx = event.offsetX;
	var cy = event.offsetY;
	var sx = event.screenX;
	var sy = event.screenY;
	var msg = "cx: " + cx + " cy: " + cy + " sx: " + sx + " sy: " + sy;
	document.getElementById.Id("info").innerHTML = msg;
}

function dump_methods(v) {
	for (key in v) {
		if ( typeof (v[key]) == "function") {
			debug(key);
		}
	}
}

function dump_properties(v) {
	for (key in v) {
		if ( typeof (v[key]) != "function") {
			debug(key + " : " + v[key]);
		}
	}
}

function dump_coords(elem_id) {
	debug(elem_id + " coords");
	var elem = document.getElementById(elem_id);
	var msg = "oHeight: " + elem.offsetHeight + " oWidth: " + elem.offsetWidth + " oLeft: " + elem.offsetLeft + " oTop: " + elem.offsetTop;
	debug(msg);
	msg = "cHeight: " + elem.offsetHeight + " cWidth: " + elem.offsetWidth + " cLeft: " + elem.offsetLeft + " cTop: " + elem.offsetTop;
	debug(msg);
}

var _debounced = {};
function debounce(elem_id, callBack) {
	if (_debounced.elem_id | ( typeof (_debounced.elem_id) == "undefined") | typeof (_debounced.elem_id == "UNDEFINED")) {
		_debounced.elem_id = false;
		setTimeout(function() {
			_debounced.elem_id = true;
			callBack(elem_id);
		}, 100);
	}
}

function add_event(id, type, callBack) {
	try {
	  document.getElementById(id).addEventListener(type, callBack, true);
	} catch (e){
	  debug("add_event id="+id);
	}
}

function remove_event(id, type, callBack) {
	document.getElementById(id).removeEventListener(type);
}

function copyElem(parent_id, id, class_name) {
	var elem = document.getElementById(parent_id).cloneNode(true);
	if ( typeof (id) != "undefined" | typeof (id) != "UNDEFINED") {
		elem.id = id;
	}
	if ( typeof (class_name) != "undefined" | typeof (class_name) != "UNDEFINED") {
		elem.className = class_name;
	}
	return elem;
}

function getMatch(a, v) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] == v)
			return i;
	}
	return -1;
}

function getCookie(o){
	//path: '/', domain: 'jquery.com'
	if(typeof(o.time) == "undefined")
		$.cookie(o.label, o.value, {secure: true});
	else {
		//var date = new Date();
		//date.setTime(date.getTime() + (o.days * 24 * 60 * 60 * 1000));
		//o.time = date.toGMTString();
		$.cookie(o.label, o.value, {expires: o.time, secure: true});
	}
}
function setCookie(label){
	$.removeCookie(label);
}