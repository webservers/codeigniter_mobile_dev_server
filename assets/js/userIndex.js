var imgPath = "/uploads/graphics/";
if (typeof (app) == 'undefined') {
	app = {};
}
app.data = {};
app.data.casinos = null;

function openCasinoLink(casino_id){
	for (var i=0; i<app.data.casinos.length;i++){
		if(app.data.casinos[i].id == casino_id){
			window.open("https://"+app.data.casinos[i].website, "_blank");
		}
	}
}

function loadGame(o){
	app.data.game_id = o.id.split('d')[1];
	for(var i=0;i<app.data.games.length;i++){
		if(app.data.games[i].id == app.data.game_id){
			app.data.table_type = app.data.games[i].table_type;
			app.data.chip_values = app.data.games[i].chip_values;
			reset();
		}
	}
}
function loadGames(o){
	var casino_id = o.id.split('d')[1];
	app.data.casino_id = casino_id;
	app.controllers.games.getGamesFromCasino({casino_id: casino_id}, function(d){
		app.data.games = d;
		$("#casino_games").html("");
		var html = "";
		for(var i=0;i<app.data.games.length;i++){
			html += '<img id="game_id'+app.data.games[i].id+'"src="'+imgPath+app.data.games[i].thumbnail+'" width="180" height="80" onclick="loadGame(this)"/>';
			app.data.games[i].chip_values = app.data.games[i].chip_values.split(","); //convert to float array
			for(var k=0;k<app.data.games[i].chip_values.length;k++){
				app.data.games[i].chip_values[k] = app.data.games[i].chip_values[k]/1;
			}
		}
		$("#casino_games").html(html);
	});
	//openCasinoLink(casino_id);
}
function updateCasinos(){
	try{
		app.data.casinos = [];
		$("#casinos").html("");
		$("#casino_games").html("");
		$("#roulette_game").hide();
		var stateSelected = $("#state_selection").val();
		app.controllers.casinos.getCasinos({state: stateSelected}, function(o){
			app.data.casinos = o;
			var html = "";
			for (var i=0; i<app.data.casinos.length;i++){
				//console.log(key);
				html += '<img id="game_id'+o[i].id+'" src="'+imgPath+o[i].thumbnail+'" width="180" height="80" onclick="loadGames(this)"/>';
			}
			$("#casinos").html(html);
		});
	} catch(e){
		console.error(e);
	}
}
$(function(){
	$("#state_selection").change(function(){
		updateCasinos(); //o.target.value
	});
	updateCasinos();
	$("#percentBalance").val(percentOfBalance);
	$("#percentBalance").change(function(){
		percentOfBalance = $("#percentBalance").val()/1;
		alert("updated percent of balance "+percentOfBalance);
	});
	$("#percentTrigger").val(probabilityTrigger);
	$("#percentTrigger").change(function(){
		probabilityTrigger = $("#percentTrigger").val()/1;
		alert("updated probability trigger: "+probabilityTrigger);
	});
	$("#probabilityStop").val(probabilityStop);
	$("#probabilityStop").change(function(){
		probabilityStop = $("#probabilityStop").val()/1;
		alert("updated probability stop: "+probabilityStop);
	});	
	console.log("initialized");	
});