// Init SpeechSynth API
const synth = window.speechSynthesis;
// Init voices array
let voices = [];

function getVoices(){
	 voices = synth.getVoices();
	
	 // Loop through voices and create an option for each one
	 /*
		 * voices.forEach(voice => { // Create option element const option =
		 * document.createElement('option'); // Fill option with voice and
		 * language option.textContent = voice.name + '(' + voice.lang + ')'; //
		 * Set needed option attributes option.setAttribute('data-lang',
		 * voice.lang); option.setAttribute('data-name', voice.name);
		 * voiceSelect.appendChild(option); });
		 */
}

// Fix for duplication, run code depending on the browser
if (typeof InstallTrigger !== 'undefined') { //isFirefox
    getVoices();
}
if (!!window.chrome && !!window.chrome.webstore) { //isChrome
    if (synth.onvoiceschanged !== undefined) {
        synth.onvoiceschanged = getVoices;
    }
}

// Speak
var sentences = [];
function speak(str){

  // Check if speaking
  if (synth.speaking) {
    //debug('Already speaking can not say '+str);
	sentences.push(str);
    return;
  }
  if (str !== '') {
    // Get speak text
    const speakText = new SpeechSynthesisUtterance(str);

    // Speak end
    speakText.onend = e => {
      //debug('finished sentence '+sentences.length+" remaining.");
		if(sentences.length > 0){
			speak(sentences.pop());
		}
    };

    // Speak error
    speakText.onerror = e => {
      debug('Something went wrong with speech module');
    };

    // Selected voice
    // const selectedVoice = voiceSelect.selectedOptions[0].getAttribute(
    // 'data-name'
    // );

    // Loop through voices
    // voices.forEach(voice => {
    // if (voice.name === selectedVoice) {
    // speakText.voice = voice;
    // }
    // });

    // Set pitch and rate
    // speakText.rate = rate.value;
    // speakText.pitch = pitch.value;
    // Speak
    synth.speak(speakText);
  }
}

	// EVENT LISTENERS
/*
 * // Text form submit textForm.addEventListener('submit', e => {
 * e.preventDefault(); speak(); textInput.blur(); }); // Rate value change
 * rate.addEventListener('change', e => (rateValue.textContent = rate.value)); //
 * Pitch value change pitch.addEventListener('change', e =>
 * (pitchValue.textContent = pitch.value)); // Voice select change
 * voiceSelect.addEventListener('change', e => speak());
 */