<?php
include_once "includes/readdir.php";	//reads a directory into an array
include_once "includes/Arguments.php";  //parses POST,get,commandline parameters to a variable, false if not available
include_once "includes/json/json_include.php";

//action=delete&file=http://localhost/test.wav
//action=getWavFiles

//dynamic path generation
/*	$this_path = $_SERVER['PHP_SELF'];	//echo $this_path;
	$i = strrpos($this_path, '/');
	$d = substr($this_path,0,$i);	//echo $d;
	$path = "c:/wamp/www/bluemountain/voice_recorder_production_V1.01/";*/

$sys_path = '/home/128574/domains/bluemountaintechnologies.com/html/';
$url_path = 'http://bluemountaintechnologies.com/';
$path = 'demo/vc_CHP5/';
/*$sys_path = '/Applications/MAMP/htdocs/bluemountain/';
$url_path = 'http://localhost/bluemountain/';
$path = 'vc_CHP5/';*/
$soundPath = 'soundfiles/';
$downloadPath = 'downloads/';
$soundUrl = $url_path.$path.$soundPath;

$a = getArgument('action');
if($a == "getWavFiles"){
	//Getting all the wave files
	$dir = readDirectory($sys_path.$path.$soundPath);
	if(isset($dir['files'])){
		$files = $dir['files'];

		echo '<table class="soundsTable">';
		foreach ($files as $file){
			$b = strpos($file, ".wav");
			if($b === false){}
			else{
				echo '<tr>';
				echo "<td><a href='javascript:void(0)' class='button del' onclick='deleteSound(".'"'.$file.'")'."'></a></td>";
				echo "<td><a href='javascript:void(0)' class='button edit' onclick='vc.loadDraft(".'"'.$soundUrl.$file.'")'."'></a></td>";				
				echo "<td><a href='javascript:void(0)' onclick='vc.loadPlayback(".'"'.$soundUrl.$file.'")'."'>".$file."</a></td>";
				echo '</tr>';			
			}
			/*$b = strpos($file, ".mp3");
			if($b === false){}
			else{
				echo '<tr>';
				echo "<td><a href='#' class='button del' onclick='deleteSound(".'"'.$file.'")'."'></a></td>";
				echo "<td><a href='#' class='button edit' onclick='vc.loadSound(".'"'.$soundUrl.$file.'")'."'></a></td>";				
				echo "<td>".$file."</td>";
				echo '</tr>';			
			}*/			
		}
		echo '</table>';
	} else {
		echo '';
	}
} else if($a == "deleteWavFile"){
	$f = getArgument('file');
	//echo $f; //file is a url

	//$i = strrpos($f, '/');
	//$f = substr($f,$i);	//echo $d;
	$f = $sys_path.$path.$soundPath.$f;
	if(is_file($f)){
		if(unlink($f))
			echo '{success:true}';
		else
			echo '{success:false, msg:"Unable to delete file '.$f.'"}';
	} else {
		echo '{success:false, msg:"Bad File Name '.$f.'"}';
	}

} else if($a == "getZipFiles"){
	$dir = readDirectory($sys_path.$path.$downloadPath);
	if(isset($dir['files'])){
		$files = $dir['files'];

		foreach ($files as $file){
			$b = strpos($file, ".zip");
			if($b === false){}
			else{
				echo "<a href='".$url_path.$path.$downloadPath.$file."'>".$file."</a><br />";
			}
		}
	} else {
		echo '';
	}
}

?>