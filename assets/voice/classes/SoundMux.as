﻿package classes {
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import classes.WavHeaders;
	
	public class SoundMux{
		public function SoundMux(){
			// constructor code
		}
		public function Append(wav0:ByteArray, wav1:ByteArray):ByteArray{
			//get mic.raw in wav0
			//record new sound
			//append mic.raw to wav0
			//make all the calls to the wave generator
			var h0 = new WavHeaders();
			h0.read_header(wav0);
			var h1 = new WavHeaders();
			h1.read_header(wav1);
					
			var temp = new ByteArray();
			if ( (h0.sampleRate == h1.sampleRate) && (h0.channels == h1.channels) && (h0.bitsPerSample == h1.bitsPerSample) ){
				var newDataSize = wav0.length + wav1.length - (h0.headerSize + h1.headerSize);
				temp = h0.write_header(newDataSize);
				//trace("Header Size: "+temp.length);
				temp.writeBytes(wav0, h0.headerSize);
				temp.writeBytes(wav1, h1.headerSize);
				//trace("FileSizeCalc: "+newDataSize+" Actual: "+(temp.length-h0.headerSize));
			}
			return temp;			
		}
		public function Insert(wav0:ByteArray, wav1:ByteArray, tS:Number, tE:Number):ByteArray{
			//get mic.raw in wav0
			//record new sound
			//append mic.raw to wav0
			//make all the calls to the wave generator
			var h0 = new WavHeaders();
			h0.read_header(wav0);
			var h1 = new WavHeaders();
			h1.read_header(wav1);
			tS = tS / 1000;
			tE = tE / 1000;
			var temp = new ByteArray();
			if ( (h0.sampleRate == h1.sampleRate) && (h0.channels == h1.channels) && (h0.bitsPerSample == h1.bitsPerSample) ){
				var OffsetStart = Math.ceil(tS * h0.byteRate); //num bytes
				var OffsetEnd = Math.ceil(tE * h0.byteRate);	//offset from start
				trace( "%2"+(OffsetStart % 2) );
				if( (OffsetStart % 2) == 1 )
					OffsetStart = OffsetStart + 1;
					
				if( (OffsetEnd % 2) == 1 )
					OffsetEnd = OffsetEnd + 1;					
				trace("Header Size: "+temp.length);
				trace("tS: "+tS+" tE: "+tE);
				trace("OffsetStart: "+OffsetStart+" OffsetEnd:"+OffsetEnd);
				trace("wav0.length: "+wav0.length+" wav1.length: "+wav1.length);
				trace("byteRate: "+h0.byteRate);
				temp.clear();
				//var newDataSize = OffsetStart;
				//var newDataSize = (OffsetStart + wav1.length) - h0.headerSize;
				var newDataSize = ((wav0.length - (OffsetEnd - OffsetStart) ) + wav1.length) - (h0.headerSize + h0.headerSize);
				//(wav0.length - (OffsetEnd-OffsetStart)) + (wav1.length - h1.headerSize) - (h0.headerSize + h1.headerSize);
				temp = h0.write_header(newDataSize);				
				temp.writeBytes(wav0, h0.headerSize, OffsetStart); //only write the bytes upto the OffsetStart
				temp.writeBytes(wav1, h0.headerSize);
				temp.writeBytes(wav0, OffsetEnd+h0.headerSize);	//write the bytes after the OffsetEnd				
				h0.read_header(temp);
				trace("DataSizeCalc: "+h0.waveDataLength+" Actual: "+(temp.length-h0.headerSize));
			}
			return temp;			
		}
		public function Delete(wav0:ByteArray, tS:Number, tE:Number):ByteArray{
			var h0 = new WavHeaders();
			h0.read_header(wav0);
			tS = tS / 1000;
			tE = tE / 1000;
			var OffsetStart = Math.ceil(tS * h0.byteRate); //num bytes
			var OffsetEnd = Math.ceil(tE * h0.byteRate);	//offset from start
			//make sure times are appropriate
			if((OffsetStart > OffsetEnd) || (OffsetEnd > wav0.length)) return wav0;
			var temp = new ByteArray();				
			trace( "%2"+(OffsetStart % 2) );
			if( (OffsetStart % 2) == 1 )
				OffsetStart = OffsetStart + 1;
				
			if( (OffsetEnd % 2) == 1 )
				OffsetEnd = OffsetEnd + 1;					
			trace("Header Size: "+temp.length);
			trace("tS: "+tS+" tE: "+tE);
			trace("OffsetStart: "+OffsetStart+" OffsetEnd:"+OffsetEnd);
			trace("wav0.length: "+wav0.length);
			trace("byteRate: "+h0.byteRate);
			temp.clear();
			var newDataSize = ((wav0.length - (OffsetEnd - OffsetStart) )) - (h0.headerSize);
			temp = h0.write_header(newDataSize);				
			temp.writeBytes(wav0, h0.headerSize, OffsetStart); //only write the bytes upto the OffsetStart
			temp.writeBytes(wav0, OffsetEnd+h0.headerSize);	//write the bytes after the OffsetEnd				
			h0.read_header(temp);
			trace("DataSizeCalc: "+h0.waveDataLength+" Actual: "+(temp.length-h0.headerSize));
			return temp;			
		}		
	}	
}
