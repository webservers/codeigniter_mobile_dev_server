﻿package classes {

	import flash.events.*;
	import flash.net.*;		
	import flash.xml.*;				

	public class XMLLoader {
		
		private var onCompleted:Function;
		private var onError:Function;
		private var onProgress:Function;

		public function XMLLoader(URL:String, onCompleted:Function, onError:Function, onProgress:Function = null) {
			// event handlers
			this.onCompleted = onCompleted;
			this.onError = onError;
			this.onProgress = onProgress;
			
			var rand:Number = 100000 + (Math.random() * 1000000);
			if (URL.indexOf('http://') > -1) {
				if (URL.indexOf('?') > -1) {
					URL += '&cacheID='+rand;
				} else {
					URL += '?cacheID='+rand;					
				}				
			}
			
			var loader:URLLoader = new URLLoader();
			if (onProgress != null) loader.addEventListener(ProgressEvent.PROGRESS, ___progress);
			loader.addEventListener(Event.COMPLETE, ___loaded);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ___security);
			loader.addEventListener(IOErrorEvent.IO_ERROR, ____notfound);
			
			var request:URLRequest = new URLRequest(URL);
			try {
				loader.load(request);
			} catch (error:Error) {
				onError('Error while processing');
			}							
		}
		
		private function ___progress(e:ProgressEvent) {
			onProgress(Math.ceil((e.bytesLoaded * 100) / e.bytesTotal));
		}		
		
		private function ___loaded(e:Event) {
			var loader:URLLoader = URLLoader(e.target);
			try {
				var xml:XML = new XML(loader.data);
			} catch(e) {
				onError('Invalid configuration file.');
				return;
			}
			onCompleted(xml);
		}
		
		private function ___security(e:SecurityErrorEvent) {
			onError('Error while processing: security error.');
		}			
		
		private function ____notfound(e:IOErrorEvent) {
			onError('The configuration file is not found.');
		}								

	}
	
}
