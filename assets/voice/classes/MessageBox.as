﻿package classes {
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.text.TextFieldType;
	import flash.filters.GlowFilter;
	import fl.transitions.Tween;
	import fl.motion.easing.Elastic;
	import fl.transitions.easing.Regular;
	import flash.text.TextFieldAutoSize;
	import classes.MessageBoxButton;
	import classes.MessageBoxInput;	
	
	public class MessageBox extends Sprite {
		
		public static var useEmbeddedFonts:Boolean = false;
		
		private var boxWidth:Number;
		private var boxHeight:Number;

		public var backgroundColor:uint 	= 0xF90000;
		public var gradientColor:uint 		= 0x000000;
		public var textColor:uint 			= 0xFFFFFF;
		public var textBGColor:uint 		= 0x000000;
		public var padding:Number			= 8;
		public var upWithEffect:Boolean		= true;

		private var lines:Array;
		private var curLine:int;
		private var lockBG:Sprite;

		private var inputs:Object;

		public function MessageBox() {
			inputs		= {};
			lines 		= [];
			curLine		= 0;
			addEventListener(Event.ADDED_TO_STAGE, buildAll);
		}
		
		public function newLine() {
			lines.push([]);
			curLine = lines.length - 1;
		}
		
		private function addToLine(item:DisplayObject) {
			if (lines.length == 0) newLine();
			lines[curLine].push(item);
		}
		
		public function addStaticText(text:String, width:Number = 0, align:String = 'left') {
			var t:TextField = new TextField();
			
			t.autoSize		= TextFieldAutoSize.CENTER;
			t.wordWrap 		= true;
			t.embedFonts 	= useEmbeddedFonts;
			t.multiline 	= true;
			t.selectable	= false;
			t.text			= text;
			var tf:TextFormat = getTextFormat();
			tf.align		= align;
			t.setTextFormat(tf);
			t.defaultTextFormat = tf;
			t.width			= (width ? width : t.textWidth + 5);
						
			addToLine(t);
		}
		
		public function addInputText(name:String, width:Number, defText:String = '', align:String = 'left') {
			var txt:MessageBoxInput = new MessageBoxInput(defText, width, align, textColor, textBGColor);
			inputs[name] = txt;
			addToLine(txt);
		}		
		
		public function getTextByName(name:String):String {
			return inputs[name].text;
		}
		
		public function selectInputText(name:String) {
			if (inputs[name]) {
				inputs[name].selectAll();
			}
		}
		
		public function addButton(title:String, callBackFunc:Function) {
			var btn:MessageBoxButton = new MessageBoxButton(title, callBackFunc, textColor, textBGColor);
			addToLine(btn);
		}
		
		private function buildAll(e:Event) {
			var maxWidth:Number = 0;
			var curHeight:Number = padding;
			var curWidth:Number = 0;
			var lineWidth:Number = 0;
			var lineHeight:Number = 0;
			var item:DisplayObject;
			var l:int, i:int;
			
			// calc. max width
			for (l = 0; l < lines.length; l++) {
				lineWidth = 0;
				for (i = 0; i < lines[l].length; i++) {
					item = lines[l][i];
					lineWidth += item.width;
				}
				if (maxWidth < lineWidth) maxWidth = lineWidth;
			}
			
			
			for (l = 0; l < lines.length; l++) {
				lineHeight = 0;
				curWidth = 0;
				// calc. max height of line
				for (i = 0; i < lines[l].length; i++) {
					item = lines[l][i];
					if (lineHeight < item.height) lineHeight = item.height;
					curWidth += item.width;
				}
				curWidth = (maxWidth - curWidth) / 2;
				for (i = 0; i < lines[l].length; i++) {
					item = lines[l][i];
					item.x = curWidth + 5;
					item.y = curHeight + ((lineHeight - item.height) / 2);
					curWidth += item.width;
					addChild(item);
				}
				curHeight += lineHeight + 5; 
			}
			
			maxWidth += padding;
			
			// main background
			var bg1:Sprite = new Sprite();
			with (bg1.graphics) {
				lineStyle(0,0,0);
				beginFill(backgroundColor, 1);
				drawRoundRect(0, 0, maxWidth, curHeight, 10, 10);
				endFill();
			}
			addChild(bg1);
			setChildIndex(bg1, 0);
			
			// shadow
			var gf:GlowFilter = new GlowFilter(0x000000, 0.7, 5, 5, 2, 1, false, false);
			bg1.filters = [gf];
			
			// gradient background
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [gradientColor, gradientColor];
			var alphas:Array = [0.5, 0];
			var ratios:Array = [1,255];
			var matr:Matrix = new Matrix();				
			matr.createGradientBox(maxWidth, curHeight, (90 * Math.PI) / 180);			
			var bg2:Sprite = new Sprite();
			with (bg2.graphics) {
				lineStyle(0, 0, 0);
				beginGradientFill(fillType, colors, alphas, ratios, matr);
				drawRoundRect(0, 0, maxWidth, curHeight, 10, 10);
				endFill();
			}
			addChild(bg2);
			setChildIndex(bg2, 1);
			
			createBlockSprite();
			
			this.x = (stage.stageWidth / 2) - (maxWidth / 2);
			this.y = (stage.stageHeight / 2) - (curHeight / 2);
			
			lockBG.x = -this.x;
			lockBG.y = -this.y;
			
			if (upWithEffect) {
				this.alpha = 0;
				var tween:Tween;
				tween = new Tween(this, 'alpha', Regular.easeOut, 0, 1, 5);
				tween.start();
				tween = new Tween(this, 'y', Elastic.easeOut, this.y - 50, this.y, 5);
				tween.start();
			}
		}

		private function createBlockSprite() {
			var s:Sprite = lockBG = new Sprite();
			with (s.graphics) {
				lineStyle(0,0,0);
				beginFill(0x000000,1);
				drawRect(0,0,stage.stageWidth, stage.stageHeight);
				endFill();
			}
			s.alpha = 0;
			s.mouseEnabled = true;
			s.mouseChildren = false;
			addChild(s);
			setChildIndex(s, 0);
		}
		
		private function getTextFormat():TextFormat {
			var tf:TextFormat = new TextFormat();
			tf.color 	= textColor;
			tf.font 	= 'Arial';
			tf.size		= 12;
			return tf;
		}

	}
	
}
