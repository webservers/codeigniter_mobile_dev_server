﻿package classes {
	import flash.media.Sound;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;	
	import flash.media.SoundChannel;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	
	import flash.events.Event;
	
	import flash.utils.ByteArray;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;	

	public class Mp3Player {	
		public var isPlaying:Boolean = false;
		public var sound:Sound;		
		public var soundChannel:SoundChannel;
		
		// Event Handlers
		public var soundReady:Function = null;
		public var stopHandler:Function = null;
		public var playHandler:Function = null;
		
		// Last Error text
		public var error:String = '';
			
		// Internal properties
		private var timeInterval:Number;
		public var startTime:Number = 0;
		
		// Sound Volume
		private var volume:Number = 0.5;		
		// Sound Actibity Handler
		public var activityCallBack:Function = null;
		
		public function Mp3Player() {
			// constructor code
			startTime = 0;
		}
		public function getTime():Number{	//return the fixed scrubber position
			return startTime;
		}
		
		public function clearSound() {
			sound = null;
			soundChannel = null;
		}

		private function drawSpectrum() {
			// Getting Sound spectrum and drawing it
			var bytes:ByteArray = new ByteArray();
			SoundMixer.computeSpectrum(bytes, false, 0);			
			var s:Number = 0;
			var c:Number = 0;
			for (var i:uint = 0; i < 255; i++) {
				c = bytes.readFloat();
				if (c > s) s = c;
			}
			// Increasing sound spectrum by volume
			s /= volume;
			s *= 150;
			if (s > 100) s = 100;
			
			if (activityCallBack != null) activityCallBack(s);

		}

		public function setSound(w:ByteArray) {
			try {
				// Getting WAV headers
				trace("Mp3Player.setSound");			
			} catch(e:Error) {
				throw new Error(e.message);
				return;
			}
			// Loading generated SWF
			var loader:Loader = new Loader();
			loader.loadBytes(swfSource);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, soundProcessed);
		}
		
		public function setVolume(val:int) {
			if (val > 5) val = 5;
			volume = 1 / (6 - val);
			if (soundChannel) {
				var st:SoundTransform = new SoundTransform();
				st.volume = volume;
				soundChannel.soundTransform = st;
			}
		}
		
		public function playSound() {
			// Playing Sound
			if (isPlaying && soundChannel) {
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, this.SoundComplete);
				clearInterval(timeInterval);
				soundChannel.stop();
			}			
			
			var st:SoundTransform = new SoundTransform();
			st.volume = volume;				
			if (startTime != 0)
				soundChannel = sound.play(startTime);
			else
				soundChannel = sound.play();

			if (!soundChannel) {
				// Sound Device is unavaliabe
				throw new Error(Mp3PlayerInterface.err['13']);
			} else {
				soundChannel.soundTransform = st;
				soundChannel.addEventListener(Event.SOUND_COMPLETE, this.SoundComplete);
				isPlaying = true;				
				Playing();
				timeInterval = setInterval(Playing, 20);				
			}
		}

		private function Playing() {
			if (playHandler is Function) playHandler();
			drawSpectrum();
		}
		private function soundProcessed(e:Event) {
			// SWF with wav source is loaded. Creating Sound object
//			var soundClass:Class = LoaderInfo(e.target).applicationDomain.getDefinition('WAVPlayerSoundClass') as Class;
//			sound = new soundClass() as Sound;
			if (soundReady is Function) soundReady();
		}
		private function SoundComplete(e:Event) {
			// Sound completed, Resetting green line
			stopSound();
			if (stopHandler is Function) stopHandler();			
		}
		public function pauseSound() {
			// Pausing Sound
			if (isPlaying && soundChannel) {
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, this.SoundComplete);
				clearInterval(timeInterval);
				isPlaying = false;
				soundChannel.stop();
				startTime = soundChannel.position;
			}
		}
		
		public function stopSound() {
			// Stopping Sound
			if (soundChannel) {
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, this.SoundComplete);
				soundChannel.stop();				
			}				
			clearInterval(timeInterval);				
			isPlaying = false;
			startTime = 0;
		}
	}
}
