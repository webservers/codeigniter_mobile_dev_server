﻿package classes {
	import flash.display.Sprite;
	import flash.utils.*;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.GradientType;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.ColorTransform;
	import flash.external.*;
	import flash.display.Shape;
	import flash.utils.ByteArray;	
	import classes.WavHeaders;
	
	public class Oscillogram extends Sprite {
		
		// Bar width
		public var barWidth:Number = 438;
		// Oscillogram height
		public var barHeight:Number = 130;
		
		public var maxBits:Number = 16;
		// Border color
		public var lineColor:Number = 0x111111;
		// Sound line color
		public var lineSoundColor:Number = 0x000000;
		
		// Count of dividing lines on whole width
		public var x_lines:Number = 15;
		// Count of dividing lines on whole height
		public var y_lines:Number = 10;

		
		// Last Error text
		public var error:String = '';
			
		// Mouse properties
		private var timeSet:Sprite;			//mouse chasing line
		private var timeSetFixed:Sprite;	//play from line
		private var Box:Shape;				//Selection Box
		private var xClick:Number;			//mouse click x value
		private var drag:Boolean;
		
		//time properties
		private var startTime:Number = 0;	//position time value
		private var endTime:Number = 0;		//position time value
		
		// Shadow Perspective 
		private var pPixels:Number = 2;
		private var pCof:Number = 0.4;

		public var wav:ByteArray;
		public var headers:WavHeaders;
		
		public function Oscillogram() {
			drag = false;
			this.mouseChildren = false;
			headers = new WavHeaders();	
			headers.waveDataLength = 0;
		}
		//all times are in milliseconds
		public function soundLength():Number{
			return (headers.waveDataLength / headers.byteRate) * 1000;
		}
		private function getXOffset(__Time:Number):Number{
			return (__Time * barWidth) / soundLength();
		}
		private function getTimeOffset(__X:Number):Number{
			return (__X * soundLength()) / barWidth;
		}
		public function _getTime():Object{
			var r = new Object;			
			if(wav != null){
				if(startTime != endTime){
					r.startTime = getTimeOffset(startTime);
					r.endTime = getTimeOffset(endTime);
				} else {
					r.startTime = getTimeOffset(startTime);
					r.endTime = r.startTime;
				}
				return r;
			}
			r.startTime = 0;
			r.endTime = 0;
			return r;
		}
		public function clearTime():void{
			startTime = 0;
			endTime = 0;
			headers = new WavHeaders();	
			headers.waveDataLength = 0;			
		}		
		public function processWave(wav0:ByteArray):void {	
			if (wav0.length > 0) {
				wav = wav0;
				headers.read_header(wav);
				trace("Oscillogram.processWave");

				removeBox();
				startTime = 0;
				endTime = 0;
				// Defining count of dividing lines
				x_lines = Math.ceil(barWidth / 30);
				// hard-code, sorry
				y_lines = 16;
				draw();
				addListeners();
			}
		}
		public function draw(){
				drawBackground();
				// Drawing sound over background
				if(wav != null && wav.length > 1024){ //1024 is one sample of mic data
					drawSound();
					// Drawing TimeLine
					drawTimeLine();
				}
		}
		private function saveXpos(e:MouseEvent){
			removeBox();
			xClick = this.mouseX;
			drag = true;
		}
		public function MouseOver(e:MouseEvent){
			addListeners();
			trace("over");
		}
		public function MouseOut(e:MouseEvent){
			//removeListeners();
/*			trace(this.name);
			if(Box)
				trace(Box.name);
			trace("out"+e.target.parent.name);
			if(e.target.name != null)
				trace(e.target.name.indexOf("instance", 0));
				*/
			if(e.target.parent)
				trace(e.target.parent.name);
		}
		public function addListeners(){
			this.addEventListener(Event.ENTER_FRAME, moveTimeMouse);
			this.addEventListener(MouseEvent.MOUSE_UP, setTimeMouse);
			//stage.addEventListener(MouseEvent.MOUSE_UP, MouseOut); //trying to limit input errors using mouse up
			this.addEventListener(MouseEvent.MOUSE_DOWN, saveXpos);
		}
		public function removeListeners() {
			this.removeEventListener(Event.ENTER_FRAME, moveTimeMouse);
			this.removeEventListener(MouseEvent.MOUSE_UP, setTimeMouse);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, saveXpos);
		}
		private function removeBox():void{
			if(Box){
				this.removeChild(Box);
				Box = null;
			}			
		}
		private function drawBox(s:Number, e:Number):void{
			removeBox();
			Box = new Shape();
			Box.name = "alphaBox";
			Box.graphics.beginFill(0x777777,0.35);
			if(e>s){
				Box.x = s;			
				Box.graphics.drawRect(0, barHeight, e-s, -barHeight);
			} else {
				Box.x = e;
				Box.graphics.drawRect(0, barHeight, s-e, -barHeight);				
			}
			Box.graphics.endFill();
			this.addChild(Box);
		}
		private function moveTimeMouse(e:*) {
			// Moving line for target Click Selection
			if (this.mouseY > barHeight + (barHeight * 0.3)) return;
			timeSet.x = this.mouseX;
			if (this.mouseX < 0) timeSet.x = 0;
			if (this.mouseX > barWidth) timeSet.x = barWidth;
			// trace("barWidth:"+barWidth+" line.x:"+timeSet.x);
			drawTimeLineShadow(timeSet);
			
			//------ Box Draw -------
			if(drag){
				var s = this.mouseX;
				if( ((xClick-s)*-1 + xClick) < 0 )			s = 0;
				if( ((xClick-s)*-1 + xClick) > barWidth )	s = barWidth;
				trace("Diff: "+(s-xClick) );
				if( Math.abs((s-xClick)) >= 5)
					drawBox(s, xClick);
				else{
					removeBox();
				}
			}
		}	
		
		private function setTimeMouse(e:MouseEvent) {
			drag = false;			
			//if (this.mouseY > barHeight + (barHeight * 0.3)) return;
			if(Box){
				trace("BOX.x: "+Box.x+" Box.width: "+Box.width);
				timeSet.x = Box.x;
				timeSetFixed.x = Box.x;
				startTime = Box.x;
				endTime = Box.x+Box.width;
			} else {
				timeSetFixed.x = timeSet.x;
				startTime = timeSet.x;
				endTime = timeSet.x;
			}
			//trace(" s:"+this._getTime().startTime+" e:"+this._getTime().endTime);
			drawTimeLineShadow(timeSetFixed);
		}
		
		public function reSetLine() {
			//trace(" Sound completed, Resetting green line");			
			var t = _getTime();
			if(t.startTime < t.endTime){
				setLine(t.startTime);
			}
			else{
				timeSetFixed.x = 0;
				drawTimeLineShadow(timeSetFixed);
				setTime();
			}
		}
		
		public function setLine(time:Number) {
//			trace(time+" - "+_length);
			timeSetFixed.x = getXOffset(time);
//			var __x = (time * barWidth) / soundLength();
//			trace("time:"+time+" x:"+__x+" l:"+soundLength());
			drawTimeLineShadow(timeSetFixed);
		}
		public function setTime(){
			startTime = timeSetFixed.x;
			endTime = timeSetFixed.x;
		}
		public function setTimeSetLine(time:Number){
			var __X = getXOffset(time);
			if(Box){
				trace("X: "+__X+" ST: "+startTime+" ET: "+endTime);
				if(__X > endTime || __X < startTime){
					trace("out of bounds");
					removeBox();
				}
				else
					trace("in bounds");
					
			}
			setLine(time);
			setTime();			
		}

		private function drawTimeLine() {
			// Drawing Red and Green lines to control sound playing
			timeSet = new Sprite();
			var s1:Shape = new Shape();
			timeSet.addChild(s1);
			drawTimeLineShape(s1);
			
			var ct:ColorTransform = new ColorTransform()
			ct.color = 0xFFFFFF;
			timeSet.transform.colorTransform = ct;
			timeSet.y = barHeight;
			timeSet.x = 0;
			addChild(timeSet);
			drawTimeLineShadow(timeSet);
			
			timeSetFixed = new Sprite();
			var s2:Shape = new Shape();
			timeSetFixed.addChild(s2);
			drawTimeLineShape(s2);
			timeSetFixed.y = barHeight;
			timeSetFixed.x = 0;
			addChild(timeSetFixed);
			drawTimeLineShadow(timeSetFixed);
		}
		
		private function drawTimeLineShape(s:Shape) {
			
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x000000, 0x000000];
			var alphas:Array = [0, 0.4];
			var ratios:Array = [1,255];
			var matr:Matrix = new Matrix();							
			matr.createGradientBox(2, barHeight, (90 * Math.PI) / 180, 0, -barHeight);				
			with (s.graphics) {
				// arrow
				lineStyle(0, 0, 0);
				beginFill(0x000000, 1);
				moveTo(-5, 0);
				lineTo(0, -5);
				lineTo(5, 0);
				lineTo(-5, 0);
				endFill();
				// line
				lineStyle(2);
				lineGradientStyle(fillType, colors, alphas, ratios, matr);			
				moveTo(0, -5);
				lineTo(0, -barHeight);
			}
			
		}
		
		private function drawTimeLineShadow(s:Sprite) {
			s.graphics.clear();
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x000000, 0x000000];
			var alphas:Array = [0.3, 0];
			var ratios:Array = [1,200];
			var matr:Matrix = new Matrix();				
			
			var step:Number = 180 / barWidth;
			var hh:Number 	= barHeight * pCof;
			var difA:Number = (5 * pPixels) * Math.cos(((s.x * step) * Math.PI) / 180);
			var difL:Number = (hh * pPixels) * Math.cos(((s.x * step) * Math.PI) / 180);
			matr.createGradientBox(barWidth, hh, (90 * Math.PI) / 180, 0, 0);				
			
			with (s.graphics) {
				// arrow
				beginFill(0x000000, 0.3);
				lineStyle(0, 0, 0);
				moveTo(-5, 0);
				lineTo(-difA, 5);
				lineTo(5, 0);
				lineTo(-5, 0);
				endFill();
				// line
				lineStyle(2);
				lineGradientStyle(fillType, colors, alphas, ratios, matr);			
				moveTo(-difA, 5);
				lineTo(-difL, hh);
			}
			
		}
		
		private function drawSound() {
			// Drawing Sound Oscillogram
			headers.trace_header();
			var wavStep:Number = Math.ceil((headers.waveDataLength / 2) / (barWidth / 2));
			
			var curWavPos:Number = 0;
			var levels:Number = 0;			
			var level:Number = 0;
			var levelh1:Number = 0;
			var levelh2:Number = 0;
			// HalfHeight of bar
			var curX:Number = 0;
			var maxLevel:Number = 0;			
			var peaks:Array = [];
			wav.position = headers.headerSize; //skip the header
			
			// Defining peaks of sound
			while (true) {
				if (wav.bytesAvailable) {
					if (headers.bitsPerSample == 16) {
						levels += Math.abs(wav.readShort());	
					} else {
						levels += Math.abs(wav.readUnsignedByte());
					}
				}
				
				curWavPos++;
				if (curWavPos >= wavStep || !wav.bytesAvailable) {
					if((levels / curWavPos) > maxLevel)
						maxLevel = (levels / curWavPos);
					peaks.push(levels / curWavPos);
					curWavPos = 0;
					levels = 0;
				}
				if (!wav.bytesAvailable) break;
			}

			// Increasing on 10%
			maxLevel += maxLevel * 0.1;
			// The next line is required for the cases when sound is very quiet
			if (maxLevel < 3000) maxLevel = 3000;
			
			// Drawing definied list of peaks (TOP)
			this.graphics.lineStyle(0, 0, 0);
			this.graphics.beginFill(lineSoundColor,0.5);
			this.graphics.moveTo(curX, barHeight);
			var i:uint;
			for (i = 1; i < peaks.length; i+=2) {
				curX+=4;
							
				levelh1 = Math.ceil((peaks[i - 1] / maxLevel) * barHeight);
				// Cutting off incorrect level
				if (Math.abs(levelh1) > barHeight) levelh1 = barHeight;
				
				levelh2 = Math.ceil((peaks[i] / maxLevel) * barHeight);
				// Cutting off incorrect level
				if (Math.abs(levelh2) > barHeight) levelh2 = barHeight;
				if (levelh1 < 3) levelh1 = 0;
				if (levelh2 < 3) levelh2 = 0;
				this.graphics.curveTo(curX - 2, barHeight - levelh1, curX, barHeight - levelh2);
			}
			this.graphics.lineTo(curX,barHeight);
			this.graphics.lineTo(0,barHeight);
			this.graphics.endFill();

			// Shadow
			curX = 0;
			var shh:Number = (barHeight * pCof);
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x000000, 0x000000];
			var alphas:Array = [0.5, 0];
			var ratios:Array = [1,200];
			var matr:Matrix = new Matrix();				
			var step:Number = 180 / barWidth;
			var dif1:Number;
			var dif2:Number;
			matr.createGradientBox(barWidth, shh, (90 * Math.PI) / 180, 0, barHeight);			
			this.graphics.lineStyle(0, 0, 0);
			this.graphics.beginGradientFill(fillType, colors, alphas, ratios,matr);
			this.graphics.moveTo(curX, barHeight);			
			for (i = 1; i < peaks.length; i+=2) {
				curX+=4;
				
				levelh1 = Math.ceil((peaks[i - 1] / maxLevel) * shh);
				// Cutting off incorrect level
				if (Math.abs(levelh1) > shh) levelh1 = shh;
				
				levelh2 = Math.ceil((peaks[i] / maxLevel) * shh);
				// Cutting off incorrect level
				if (Math.abs(levelh2) > shh) levelh2 = shh;
				if (levelh1 < 3) levelh1 = 0;
				if (levelh2 < 3) levelh2 = 0;
				dif1 = (levelh1 * pPixels) * Math.cos((((curX - 2) * step) * Math.PI) / 180);
				dif2 = (levelh2 * pPixels) * Math.cos(((curX * step) * Math.PI) / 180);
				this.graphics.curveTo((curX - 2) - dif1, barHeight + levelh1, (curX) - dif2, barHeight + levelh2);
			}
			this.graphics.lineTo(curX,barHeight);
			this.graphics.lineTo(0,barHeight);
			this.graphics.endFill();
			peaks = null;
		}
		
		public function drawBackground() {
			while (this.numChildren > 0) removeChild(this.getChildAt(0));
			this.graphics.clear();
			var x:Number;
			var y:Number;
			var ys:Number = barHeight / y_lines;
			var xs:Number = barWidth / x_lines;
			
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x000000, 0x000000];
			var alphas:Array = [0, 0.2];
			var ratios:Array = [50,255];
			var matr:Matrix = new Matrix();				
			matr.createGradientBox(barWidth, barHeight, (90 * Math.PI) / 180, 0, 0);				
			this.graphics.lineStyle(1);
			this.graphics.lineGradientStyle(fillType, colors, alphas, ratios, matr);
			
			for (y = 0; y <= y_lines; y++) {
				this.graphics.moveTo(0, y * ys);
				this.graphics.lineTo(barWidth, y * ys);
			}
			for (x = 0; x <= x_lines; x++) {
				this.graphics.moveTo(x * xs, 0);
				this.graphics.lineTo(x * xs, barHeight);
			}			
			
			alphas = [0.2, 0];
			ratios = [1,200];
			
			matr.createGradientBox(barWidth, barHeight * pCof, (90 * Math.PI) / 180, 0, barHeight);				
			this.graphics.lineGradientStyle(fillType, colors, alphas, ratios, matr)
			var dy:Number;
			for (y = 0; y <= y_lines; y++) {
				dy = y * ys * pCof;
				this.graphics.moveTo(0 - (dy * pPixels), barHeight + dy);
				this.graphics.lineTo(barWidth + (dy * pPixels), barHeight + dy);
			}
			var step:Number = 180 / x_lines;
			var dif:Number;
			for (x = 0; x <= x_lines; x++) {
				dif = ((barHeight * pCof) * pPixels) * Math.cos(((x * step) * Math.PI) / 180);
				this.graphics.moveTo(x * xs, barHeight);
				this.graphics.lineTo((x * xs) - dif, barHeight + (barHeight * pCof));
			}
		}
	}
}
