﻿package classes {
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;

	
	public class MessageBoxButton extends Sprite {

		private var bg:Sprite;
		private var txt:TextField;
		
		public function MessageBoxButton(title:String, callBackFunc:Function, textColor:int = 0xDDDDDD, bgColor:int = 0x000000) {
			createTitle(title, textColor);
			createBG(callBackFunc, bgColor);
			addChild(bg);
			addChild(txt);
		}
		
		private function createTitle(title:String, textColor:int) {
			var t:TextField = txt = new TextField();
			t.embedFonts = MessageBox.useEmbeddedFonts;
			t.text = title;
			t.selectable = false;
			var tf:TextFormat = new TextFormat();
			tf.color 	= textColor;
			tf.font 	= 'Arial';
			tf.size		= 14;
			tf.align 	= 'center';
			tf.bold 	= true;
			t.setTextFormat(tf);
			t.defaultTextFormat = tf;
			t.width = t.textWidth + 20;
			t.height = t.textHeight + 5;
			t.mouseEnabled = false;
		}
		
		private function createBG(callBackFunc:Function, bgColor:int) {
			bg = new Sprite();
			bg.mouseEnabled = true;
			bg.mouseChildren = false;
			with (bg.graphics) {
				lineStyle(0,0,0);
				beginFill(bgColor, 1);
				drawRoundRect(0, 0, txt.width, txt.height, txt.height, txt.height);
				endFill();
			}
			bg.alpha = 0;
			bg.addEventListener(MouseEvent.CLICK, callBackFunc);
			bg.addEventListener(MouseEvent.MOUSE_OVER, onOver);
			bg.addEventListener(MouseEvent.MOUSE_OUT, onOut);
		}
		
		private function onOver(e:MouseEvent) {
			e.target.alpha = 0.4;
		}
		
		private function onOut(e:MouseEvent) {
			e.target.alpha = 0;
		}

	}
	
}
