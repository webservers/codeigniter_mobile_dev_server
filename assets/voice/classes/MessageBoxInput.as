﻿package classes {
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldType;
	
	public class MessageBoxInput extends Sprite {
		
		private var bg:Sprite;
		private var txt:TextField;

		public function MessageBoxInput(defText:String, width:Number, align:String = 'left', textColor:int = 0x000000, bgColor:int = 0xFFFFFF) {
			createTextField(defText, width, align, textColor);
			createBG(bgColor);
			addChild(bg);
			addChild(txt);
		}
		
		private function createTextField(defText:String, width:Number, align, textColor:int) {
			var t:TextField = txt = new TextField();
			t.type = TextFieldType.INPUT;
			t.embedFonts = MessageBox.useEmbeddedFonts;
			t.text = defText;
			var tf:TextFormat = new TextFormat();
			tf.color 	= textColor;
			tf.font 	= 'Arial';
			tf.size		= 12;
			tf.align 	= align;
			t.setTextFormat(tf);
			t.defaultTextFormat = tf;
			t.width = width + 10;
			t.height = t.textHeight + 5;
			t.x = 5;
		}
		
		private function createBG(bgColor:int) {
			bg = new Sprite();
			with (bg.graphics) {
				lineStyle(0,0,0);
				beginFill(bgColor, 0.5);
				drawRoundRect(0, 0, txt.width + 10, txt.height, txt.height, txt.height);
				endFill();
			}
		}
		
		public function get text() {
			return txt.text;
		}
		
		public function selectAll() {
			if (stage) stage.focus = txt;
			txt.setSelection(0, txt.text.length);
		}
	}
	
}
