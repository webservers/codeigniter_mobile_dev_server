﻿package classes {
	
	import classes.WavHeaders;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	public class AudioSWF {
		
		private var headers:WavHeaders;
		private var SWF_PART0:String = '46575309';
		private var SWF_PART1:String = '7800055F00000FA000000C01004411080000004302FFFFFFBF150B00000001005363656E6520310000BF14C7000000010000000010002E00000000080013574156506C61796572536F756E64436C6173730B666C6173682E6D6564696105536F756E64064F626A6563740C666C6173682E6576656E74730F4576656E744469737061746368657205160116031802160600050701020702040701050704070300000000000000000000000000010102080300010000000102010104010003000101050603D030470000010101060706D030D04900470000020201010517D0306500600330600430600230600258001D1D1D6801470000BF03';
		private var SWF_PART2:String = '3F131800000001000100574156506C61796572536F756E64436C61737300440B0800000040000000';
				
		
		public function AudioSWF() {
		}
		
		public function compile(wav:ByteArray):ByteArray {
			headers = new WavHeaders();
			headers.read_header(wav);
//			trace("AudioSWF.compile()");
			var dataLength:uint = wav.length;
			var swfSize:uint = dataLength+307;
			var totalSamples:uint = dataLength/headers.blockAlign;
			var output:ByteArray = new ByteArray();
			output.endian = Endian.LITTLE_ENDIAN;
			writeBytesFromString(output,this.SWF_PART0);
			output.writeUnsignedInt(swfSize);
			writeBytesFromString(output,this.SWF_PART1);
			output.writeUnsignedInt(dataLength+7);
			output.writeByte(1);
			output.writeByte(0);
			output.writeByte(getFormatByte());
			output.writeUnsignedInt(totalSamples);
			output.writeBytes(wav);
			writeBytesFromString(output,this.SWF_PART2);
			return output;
		}
			
		private function writeBytesFromString(byteArray:ByteArray,bytesHexString:String):void {
			var length:uint = bytesHexString.length;
			
			for (var i:uint = 0;i<length;i+=2) {
				var hexByte:String = bytesHexString.substr(i,2);
				var byte:uint = Number('0x'+hexByte);
				byteArray.writeByte(byte);
			}
		}
				
		private function getFormatByte():uint {
			var byte:uint = (headers.bitsPerSample == 0x10) ? 0x32 : 0x00;
			byte += (headers.channels-1);
			byte += 4*(Math.floor(headers.sampleRate/5512.5).toString(2).length-1);
			return byte;
		}	
	}
	
}