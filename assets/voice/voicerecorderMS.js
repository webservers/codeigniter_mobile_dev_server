var ShowDebug = false;
var theFirstLoading = true;

function jsPrivacyFunc(s){
	//if (ShowDebug) alert("jsPrivacyFunc: "+s);
	//MM_openBrWindow('soundcheck.html', 'security', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=262,height=320');
	vc.setTitle("<center>Flash Settings</center>", "#FFF");		
	vc.setPanelContentDefault();	
    swfObjectEmbed(250, 250, "soundcheck1", "soundcheck.swf", "vcPanelContent", null);		
	vc.showPanelContent(0);		
}
function jsSettingsFunc(s){
	//if (ShowDebug) alert("jsSettingsFunc: "+s);
	//MM_openBrWindow('micsettings.html', 'micsettings', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=262,height=320');
	vc.setTitle("<center>Flash Settings</center>", "#FFF");	
	vc.setPanelContentDefault();
	swfObjectEmbed(250, 250, "micsettings1", "micsettings.swf", "vcPanelContent", null);	
	vc.showPanelContent(0);	
}

//---------------------------------------------------------------------------------------
function patientID(){
	return " Jsmith";
}

function getFileName(){
	var d = new Date();
	var ms = Date.parse(d); //num millisecs since Jan1 1970
	var fn = ms+"sound.wav";
	return fn;
}





function swfObjectEmbed(w, h, id, movieName, containerId, flashvars){
	//var flashvars = {};
	var tempID = '_'+containerId;
	var swfVersionStr = "10.1.52";
	//<!-- xiSwfUrlStr can be used to define an express installer SWF. -->
	var xiSwfUrlStr = "";

	var params = {
		quality: "high",
		play: "true",
		loop: "true",
		wmode: "transparent",
		scale: "showall",
		menu: "true",
		devicefont: "false",
		salign: "",
		allowscriptaccess: "sameDomain"
	};
	var attributes = {
		id: id,
		name: id,
		align: "middle"
	};
		try{
			if( document.getElementById(tempID) == null){
				var cElem = document.getElementById(containerId);
				if(cElem != null){
					for(var i=0;i<cElem.childNodes.length;i++){
						if(cElem.childNodes[i].id == id){
							cElem.removeChild(document.getElementById(id));
							break;
						}
					}
					var nElem = document.createElement('div');		
					nElem.setAttribute('id', tempID);
					cElem.appendChild(nElem);
				}
			}
		} catch(e){
			alert(e);
		}	
	swfobject.embedSWF(movieName, tempID, w , h, swfVersionStr, xiSwfUrlStr, flashvars, params, attributes);
	return tempID;
}


//swfObjectEmbed(250, 250, id, "micsettings.swf", containerId, {});
//swfObjectEmbed(250, 250, id, "soundcheck.swf", containerId, {});
//swfObjectEmbed(568, 40, "voicerecorder", "voicerecorder.swf", "containerId", {confFile:"./config.xml"});


//XML Generation
function vcConfjs(o){
	var header = '<?xml version="1.0"?><voiceRecorder>\n';
	var footer = '</voiceRecorder>';
	
	var params = {
		sampleRate: "8000",
		startupFile: "",
		maxSeconds: "59999",	
		seekTime: "1",	
		jsOnLoadFunc: "vc.jsOnLoadFunc",
		jsErrorFunc: "vc.jsErrorFunc",
		jsGetFileName: "vc.jsBeforeSaveFunc",
		jsAfterSaveFunc: "vc.jsAfterSaveFunc",
		jsRecStartedFunc: "",
		jsRecCompletedFunc: "",
		jsPrivacyFunc: "jsPrivacyFunc",
		jsSettingsFunc: "jsSettingsFunc",
		jsSendFunc: "vc.b64Send",
		jsRetryFunc: "vc.jsRetryFunc",
		jsRequestUpload: "vc.jsBeforeSaveFunc",		
		jsGetPostParams: "vc.jsGetPostParams",		
		backgroundColor: "AEC6CE",
		gradientColor: "AEC6CE",
		toolBarColor: "FFFFFF",
		toolBarBGColor: "AEC6CE",
		soundLineColor: "006600",
		saveURL: "saveWav.php",
		saveLocal: "N",
		patientID: "patientID",
		jsGetPrivate: "vc.getPrivateButtonState",
		setPrivate: "vc.setPrivate",
		draftMode: "vc.getDraftMode"
	};

	//these are enumerated so they must be in this order
	var msg = new Array();
	msg.push("Playing");
	msg.push("Paused");
	msg.push("Stopped");
	msg.push("Recording");
	msg.push("Ready to Record");
	msg.push("Microphone is Unavailable!");
	msg.push("Saving ...");
	msg.push("Saved");
	msg.push("Loading... Please Wait");
	msg.push("ENTER FILE NAME");
	msg.push("Do you really want to continue?");
	msg.push("Yes");
	msg.push("No");
	msg.push("Continue");
	msg.push("CANCEL");
	msg.push("OK");
	msg.push("Processing ... ");
	msg.push("Retry upload... ");
	msg.push("Uploading... ");
	msg.push("Ready to Play");
	msg.push("Microphone is Muted!");	
	
	var err = new Array();
	err.push("Error while calling JS function: ");
	err.push("The Sound File is not found.");
	err.push("Error while saving!");
	err.push("Incorrect audio format.");
	err.push("Decode error: incorrect RIFF header.");
	err.push("Decode error: incorrect chunk size.");
	err.push("Decode error: this file is not PCM wave file.");
	err.push("Security Error.");
	err.push("The save handler is not found.");
	err.push("This application requires Adobe Flash Player 10.1 or newer.");
	err.push("Error while checking File Name!");
	err.push("Bad file name.");
	err.push("Sound Device is unavailable");
	err.push("Error Uploading.");
	
	var sXML = "";
	var build_XML = function(o){
		sXML = header;
		for (var key in params) {
			if(key in o)
				sXML += '<param name="'+key+'" value="'+o[key]+'"/>\n';
			else
				sXML += '<param name="'+key+'" value="'+params[key]+'"/>\n';
		}		
		for(i=0;i<msg.length;i++){
			if(i<9)
				sXML += '<msg code="0'+(i+1)+'" value="'+msg[i]+'" />\n';
			else
				sXML += '<msg code="'+(i+1)+'" value="'+msg[i]+'" />\n';
		}		
		for(i=0;i<err.length;i++){
			if(i<10)
				sXML += '<err code="0'+(i+1)+'" value="'+err[i]+'" />\n';
			else
				sXML += '<err code="'+(i+1)+'" value="'+err[i]+'" />\n';
		}
		sXML += footer;
		return sXML;
	}
	return build_XML(o);
}

function create_vc(o){
	var retryCounter = 0; 
	var max_Retry = 5000;
	var isFlashReady = false;
	var isLoading = false;	
	//this.isFlashReady = false;	this.isLoading = false;
	var w = 855;
	var h = 49;
	var id = "voicerecorder";	
	var movieName = "voicerecorder.swf";
	var containerId = "flashContent";
	var flashvars = {confFile:"./config.xml"};	
	var tempID = '_'+containerId;
	var loadURL = false;
	var getPrivate = false;
	var draftMode = false;
	var noDraftUpload = true;	
	this.setNoDraftUpload = function (_m) {
	    noDraftUpload = _m;
	}		
	var post_params = {
		FileContent: "$_f",
		FileName: getFileName(),
		UserID: 3,
		UserSessionID: "TonySessionTest",
		format:"json",
		Codec:"WAV PCM",
		SampleRate:8000,
		FileSize: "$_fs",
		CaseID: null,
		AppointmentID:7109,
		Private: "$_private"
	};

	if (typeof(o) != "undefined") {
		if (ShowDebug) alert("using parameters to override defaults");
		if(typeof(o.height) != 'undefined'){
			h = o.height;
		}
		if(typeof(o.width) != 'undefined'){
			w = o.width;
		}	
		if(typeof(o.id) != 'undefined'){
			id = o.id;
			//attributes.id = o.id;	attributes.name = o.id;
		}	
		if(typeof(o.movieName) != 'undefined'){
			movieName = o.movieName;
		}
		if(typeof(o.containerId) != 'undefined'){
			containerId = o.containerId;
		}
		if(typeof(o.xmlFile) != 'undefined'){
			if(o.xmlFile == null) flashvars = null;
			else flashvars = {confFile:o.xmlFile};
		}
		if(typeof(o.max_Retry) != 'undefined'){
			max_Retry = o.max_Retry;
		}
		if(typeof(o.post != 'undefined')){
			post_params = o.post;
		}
	}
	
	this.remove = function(){
		try{
			var d = document.getElementById(containerId);
			if(isFlashReady) d.removeChild(document.getElementById(id));
		} catch(e){
			alert(e);
		}
		return this;
	}
	
	this.embed = function(){
		tempID = swfObjectEmbed(w, h, id, movieName, containerId, flashvars);		
		return this;
	}
	this.hide = function(){		
		//$get("flashContent").style.display = "none";
		this.hidePanelContent();
		this.remove();
		isFlashReady = false;
		isLoading = false;
		loadURL = false;
		try{
			$get("dictationctl").style.display = "none";
			$get("documenttypectl").style.display = "none";
		} catch(e){ console.error("Error: "+e); }
	}
	this.show = function(){
		//$get("flashContent").style.display = "block";
		this.embed();
		try {
			$get("dictationctl").style.display = "block";
			$get("documenttypectl").style.display = "none";
		} catch(e){ console.error("Error: "+e); }		
	}

	
	this.getRecorder = function(){
		if (ShowDebug) alert("getRecorder: called: Ready="+isFlashReady);
		if(isFlashReady){
			var f = document[id];
			if(typeof(f) == "function" || typeof(f) == "object")
				return f;
			else if(typeof(window[id]) == "object"){
				return window[id];
			} else{
				if (ShowDebug) alert("Flash Object "+id+" Doesn't exist");
				return false;
			}
		} else if (ShowDebug) alert("Flash Object "+id+" isn't done Loading!\n or incorrect config.xml setting for jsOnLoadFunc");
		return false;
	}

	this.jsOnLoadFunc = function(){	
		isFlashReady = true;
		isLoading = false;
	    if (theFirstLoading) {
	        theFirstLoading = false;
	        if (typeof disableDictation == 'function')
	            disableDictation();
	    }
		if(loadURL){
			console.log("loading startup sound. ");
			 this.loadSound(loadURL);
		}
		loadURL = false;
		return patientID();
	}	
	
	this.jsErrorFunc = function(s){
		if (ShowDebug) alert(movieName+" Error: \n"+s);
		console.error(movieName+": "+s);
		return true;		
	}
	this.jsBeforeSaveFunc = function(L){
	    //	if (ShowDebug) alert('jsBeforeSaveFunc\n Local Mode? '+L);
        if(noDraftUpload)
            this.showPanelContentUploadOptions2();
        else
            this.showPanelContentUploadOptions();
		console.log("jsBeforeSave");
		return '1';
	}	
    this.setPostParam = function(p ,v ){
      post_params[p] = v;
    }	
	this.jsGetPostParams = function(o){
		var s = "";		//convert the object above to a string
		var b = true;	//have to not prepend & to string
		for(key in post_params){
			if(b){
				s+= key+"="+post_params[key];
				b = false;
			}else
				s+= "&"+key+"="+post_params[key];
		}
		if (ShowDebug) alert("post_params: "+s);
		return encodeURI(s);
		//return s;
	}
	
	this.jsRetryFunc = function(errStr){
		if (ShowDebug) alert("jsRetry"+" : "+errStr);
		//return 'saveLocal'; needs crosssite scripting hack
		
		if(retryCounter < max_Retry){		
			retryCounter++;
			return 'retry';	
		}
		else {
			retryCounter = 0;
			alert("Max Retries reached");
			return("unlock");
		}
	}
	this.jsRecCompletedFunc = function(){
		if (ShowDebug) alert('jsRecCompletedFunc');
		return true;
	}
	this.jsRecStartedFunc = function(){
		if (ShowDebug) alert('jsRecStartedFunc');
		return true;
	}
	this.jsAfterSaveFunc = function(){
		if (ShowDebug) alert('jsAfterSaveFunc');
		return true;
	}
	this.newSound = function(){
		if(this.getRecorder()){
			if( typeof(this.getRecorder().newSound) != 'function' ){
				//alert("flash is hidden!");
				//showing flash will reset it, with new sound
				//$get("flashContent").style.display = "block";				
				try {
					$get("dictationctl").style.display = "block";
					$get("documenttypectl").style.display = "none";
				} catch(e){ console.error("Error: "+e); }				
			} else {
				var success = this.getRecorder().newSound();
				if(success){	
					if (ShowDebug) alert("ReSetting Player");			
				}
				else{
					alert("Can't Reset While Recording a Sound");
				}
			}
		} else {
			this.embed();
		}
	}
	this.getDraftMode = function(){
		alert("getting Draft Mode: "+draftMode);
		return draftMode;
	}
	this.setDraftMode = function(_m){
		draftMode = _m;
	}	
	this.loadSoundWhenHidden = function(url){
		console.log("Recorder functions not available when hidden");
		loadURL = url;
		this.show();
	}
	this.loadSound = function(url) {
		if( typeof(this.getRecorder().loadSound) != 'function' ){
			this.loadSoundWhenHidden(url);
			return true;				
		}
		if(!isLoading){
			isLoading = true;
			var success = this.getRecorder().loadSound(url);
			if(success){	
				if (ShowDebug) alert("Loading Sound from url\n "+url);
			}
			else{
				alert("Can't Load a new sound when Playing or Recording a Sound");
				isLoading = false;
			}
		} else {
			alert( "Not done Loading Previous Sound!" );
		}
		return true;
	}
	this.loadPlayback = function(url){
		draftMode = false;
		this.loadSound(url);
	}
	this.loadDraft = function(url){
		draftMode = true;
		this.loadSound(url);
	}
	this.playSound = function(){
		if( typeof(this.getRecorder().playSound) == 'function' ){
			this.getRecorder().playSound();
			return true;
		}
		return false;
	}
	this.stopSound = function(){
		if( typeof(this.getRecorder().stopSound) == 'function' ){
			this.getRecorder().stopSound();
			return true;
		}
		return false;
	}
	this.pauseSound = function(){
		if( typeof(this.getRecorder().pauseSound) == 'function' ){
			this.getRecorder().pauseSound();
			return true;
		}
		return false;
	}
	this.saveSound = function(){
		if( typeof(this.getRecorder().saveRecordedWAV) == 'function' ){
			return this.getRecorder().saveRecordedWAV();
		}
		return false;
	}
	this.setPrivate = function(state){
		getPrivate = state;
		if (ShowDebug) alert("flash click set to " + state);
	}
	this.setPrivateButtonState = function(state){
		getPrivate = state;
		if( typeof(this.getRecorder().setPrivate) == 'function' ){
			this.getRecorder().setPrivate(state);
		}
	}
	this.getPrivateButtonState = function(){
		return getPrivate;
	}
	
	this.showPanelContent = function(offset){
		//var p = $("#flashContent").offset();
		//var op = $("#vcPanel1").offset({left:p.left+offset});	
		$get("vcPanel1").style.display = "inline-table";		
	}
	this.hidePanelContent = function(){
		$get("vcPanelContent").innerHTML = "";
		$get("vcPanel1").style.display = "none";		
	}
	this.resetPlayer = function(){
		this.hidePanelContent();
		this.remove();
		this.embed();
	}
	this.showPanelContentArrow = function(title, html){
		//this.setTitle("<div class='vcPanelArrow'></div><center><strong>"+title+"<strong></center>", "#FFF");
		this.setTitle("<center><strong>"+title+"<strong></center>", "#FFF");
		$get("vcPanelContent").innerHTML = html;	
		$get("vcPanel1").style.left = "676px";
	    this.showPanelContent(0);
	}
	this.setPanelContentDefault = function(){
		var s = '<div id="_vcPanelContent"></div><p align="center">';
		s += '<a href="javascript:void(0)" title="Apply" onclick="vc.resetPlayer();"><img src="Images/Apply.png" alt="Apply" border="0"></a>';
		s += '<a href="javascript:void(0)" title="Cancel" onclick="vc.hidePanelContent();"><img src="Images/Cancel.png" alt="Cancel" border="0"></a>';		
		s += '</p>';
		$get("vcPanelContent").innerHTML = s;
		$get("vcPanel1").style.left = "25px";
	}
	this.setTitle = function(s, c){
		//var s = "<center>TITLE</center>";
		//var c = "#FFF";
		$get("vcPanelTopRepeat1").innerHTML = s;
		$get("vcPanelTopRepeat1").style.color = c;
	}
	this.setUploadType = function(i){
		this.hidePanelContent();
		if (ShowDebug) alert("settingUploadType: "+i);
		this.setPostParam("UploadOption", i);
		this.saveSound();
	}
	this.showPanelContentUploadOptions = function(){
		var t = '<strong><a href="javascript:void(0)" onclick="vc.setUploadType(0)">Normal Upload</a></strong><hr width="140px"/>';
		 t += '<strong><a href="javascript:void(0)" onclick="vc.setUploadType(1)">Upload STAT</a></strong><hr width="140px"/>';
		 t += '<strong><a href="javascript:void(0)" onclick="vc.setUploadType(2)">Save as Draft</a></strong><hr width="140px"/>';
		this.showPanelContentArrow("Upload Options <a href='javascript:void(0)' style='color:#FF0000; text-decoration:none;' onclick='vc.hidePanelContent()'> X</a>", t);
	}
	this.showPanelContentUploadOptions2 = function () {
	    var t = '<strong><a href="javascript:void(0)" onclick="vc.setUploadType(0)">Normal Upload</a></strong><hr width="140px"/>';
	    t += '<strong><a href="javascript:void(0)" onclick="vc.setUploadType(1)">Upload STAT</a></strong><hr width="140px"/>';
	    this.showPanelContentArrow("Upload Options <a href='javascript:void(0)' style='color:#FF0000; text-decoration:none;' onclick='vc.hidePanelContent()'> X</a>", t);
	}	
	return this;
}