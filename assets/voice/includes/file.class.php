<?php
/*
Might want to detect windows because of the b mode in the future

if(strstr($_SERVER['SCRIPT_FILENAME'],':')){
    echo 'Windows<br>';
}else{
    echo 'Not Windows';
}

print_r($_SERVER['OS']);

$f = new file('f1.txt', 'r+');
echo $f->Size();

echo "Pos:".$f->Tell()."\n";
if(!$f->Append('BAC, 2009-01-05, 15.5'."\n")) echo $f->error;
echo "Pos:".$f->Tell()."\n";
*/

//feof � Tests for end-of-file on a file pointer
class file{
	public  $Fhand;			//file handle
	public	$Fname_path;	//filename & path
	private $Mode;			//file operation mode
	public	$error;			//stores the last error message
	function __construct($fileNamePath = '', $mode='r'){
		$this->clear();
		if($fileNamePath != ''){
			$this->Open($fileNamePath, $mode);
		}
	}
	function __destruct(){
		$this->Close();
		$this->clear();
	}
	function clear(){
		$this->Fhand = false;
		$this->Fname_path = '';
		$this->error = false;	
	}
	function Close(){
		if($this->Fhand) fclose($this->Fhand);
		$this->clear();		
	}
	function Open($fileNamePath, $mode){
	//for windows mode must include b for binary mode
		$this->Fname_path = $fileNamePath;
		$this->Mode = $mode;		
		$this->error = '';
		if(!file_exists($fileNamePath)){
			$this->Create();
		}
		//if($mode != 'w+' && !$is_file) $this->error = "non existing file ".$fileNamePath.", use mode w+ to create it";
		if($this->error == ''){
			$this->Fhand = fopen($fileNamePath, $mode);
			if($this->Fhand){
				return TRUE;
			}
			else{
				$this->error = "Open Failed for ".$fileNamePath." using mode ".$mode;
			}
		}
		return FALSE;		
	}
	function Create(){
		$mode = $this->Mode;
		$this->Fhand = fopen($this->Fname_path, 'w+');
		if($this->Fhand){
			return TRUE;
		} else {
			$this->error = "couldn't create non existing file ".$this->Fname_path;
		}
		$this->Close();
		return false;
	}	
	function Rename($newname){
		$fp = $this->Fname_path;
		$this->Close();
		if( rename($fp, $newname) ){
			$this->Open($newname, $this->Mode);
			return $this;
		}
		return false;
	}
	function Delete(){
		$fp = $this->Fname_path;
		if($this->Fhand){
			$this->Close();
			if( unlink($fp) )
				return true;
		}
		return false;
	}
	function Size(){
		if($this->Fname_path != ''){
			return filesize($this->Fname_path);
		}
		return false;
	}
	//-------------------- Basic Function ---------------------
	function Seek($offset){
	//int fseek ( resource $handle , int $offset [, int $whence = SEEK_SET ] )
    //  SEEK_SET - Set position equal to offset bytes.
    //  SEEK_CUR - Set position to current location plus offset.
    //  SEEK_END - Set position to end-of-file plus offset.
	// Upon success, returns 0; otherwise, returns -1. 
		if( $this->Fhand && is_int($offset) ){
			if( fseek( $this->Fhand, $offset, SEEK_SET) == -1 ){
				$this->error = "fseek error for offset ".$offset;
				return false;
			}
			return true;
		}
	}
	function Tell(){
		if( $this->Fhand ){
			return ftell($this->Fhand);
		}
		else return false;
	}
	function Append($Data, $length=0){
		//if($this->Mode == 'r+'){
			if (! (fseek($this->Fhand, 0, SEEK_END) == 0)){ // put pointer at end of file
				$this->error = 'fseek Failure';
			}
			return $this->Write($Data, $length);
		//} else {
		//	$this->error = "must use r+ or a+ mode to append";
		//}
		//return false;
	}
	function Write($Data, $length=0){
	//int fwrite ( resource $handle , string $string [, int $length ] )
	//fwrite() returns the number of bytes written, or FALSE on error. 
		if($this->Fhand){	
			if($length > 0){
				if ( !(fwrite($this->Fhand, $Data, $length)) ){
					$this->error = "fwrite Error";
					return false;					
				}
			} else {
				if ( !(fwrite($this->Fhand, $Data)) ){
					$this->error = "fwrite Error";
					return false;
				}
			}
			if ( !(fflush($this->Fhand) )){
				$this->error = "fflush failure";
				return false;
			}
		}
		return true;
	}
	function Read(){
	//string fread ( resource $handle , int $length )
		if($this->Size() > 0){
			return fread($this->Fhand, $this->Size());
		} else {
			$this->error = "Empty File: Read Error.";
		}
		return false;
	}	
	function GetLine($length=4096){
	//fgets  ( resource $handle  [, int $length  ] )
		return fgets($this->Fhand, $length); //stop reading at 4096 chars or a newline
	}
	function GetCsv($length=0, $delimiter=',', $enclosure='"', $escape='\\'){
	//false null array fgetcsv  ( resource $handle  [, int $length  [, string $delimiter = ','  [, string $enclosure = '"'  [, string $escape = '\\'  ]]]] )
		return fgetcsv($this->Fhand);
	}
}
?>