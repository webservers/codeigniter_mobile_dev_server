<?php
#------------------------------  Search Upload Dir ---------------------------------
#  returns HTML for a list control
#	 1. Get Dir listing into array
#	 2. Loop through Dir Listing Array
#		1. remove all characters except for the Search Term from current item
#		2. if search term doesn't exist in SearchTerm array add it.
#	 3. Loop through Search Term Array returning Search Terms in HTML form
#
function readDirectory($path){
	$DirListing = array();
	if (is_dir($path)) {
		if ($dh = opendir($path)) {
			while (($file = readdir($dh)) !== false) {
				if ( strcmp($file, ".") <> 0 and strcmp($file, "..") <> 0 ){	//remove . & .. from directiry listing
					if (is_dir( $path.'\\'.$file ) ){
						//$DirListing['dirs'][] = $file;				# save the current string to the dirlisting array
						$DirListing['dirs'][$path.'\\'.$file] = readDirectory($path.'\\'.$file);
					}
					else{
						$DirListing['files'][] = $file;				# save the current string to the dirlisting array
					}
				}
			}
			closedir($dh);
			return $DirListing;
		}
		//else $MsgErrorReadDir = $MsgErrorReadDir."Cannot Open $uploaddir<br />";
	}
	//else $MsgErrorReadDir = $MsgErrorReadDir."$uploaddir is not a Directory<br />";
	return false;
}
/*
	$DirListing = array();
	$SearchTerms[0] = "No Search Terms"; # I had to set this do to a problem with checking the boolean value returned by array_search()
	$SearchTerm;
	$i = 0;
	$MsgReadDir = '';	# captures script output
	$MsgErrorReadDir = ''; # captures script errors
	$MsgDebugReadDir = ''; # captures output for dubuging purposes
	
	# Open a known directory, and proceed to read its contents into Array
	if (is_dir($uploaddir)) {
		if ($dh = opendir($uploaddir)) {
			while (($file = readdir($dh)) !== false) {
				$DirListing[$i] = $file;				# save the current string to the dirlisting array
				$i++;
			}
			closedir($dh);
		}
		else
			$MsgErrorReadDir = $MsgErrorReadDir."Cannot Open $uploaddir<br />";
	}
	else
		$MsgErrorReadDir = $MsgErrorReadDir."$uploaddir is not a Directory<br />";
	
	# $DirListing Contains Somthing like
	#.
	#..
	#Product_Data_ATI_0.csv
	#Product_Data_ATI_1.csv
	#Product_Data_Nvidia_0.csv
	#Product_Data_Nvidia_1_0.csv
	#
	#
	#Loop Through The Directory Listing
	#$MsgDebugReadDir = $MsgDebugReadDir."<br /> Number of entries = ".count($DirListing)."<br />";
	#print_r ($DirListing);
	#print ("<br />");
		 
	for ($i=0;$i<count($DirListing);$i++){
		if ( strcmp($DirListing[$i], ".") <> 0 and strcmp($DirListing[$i], "..") <> 0 ){
		 $SearchTerm = substr($DirListing[$i],0,strrpos($DirListing[$i], "_"));
	
		 #Check the Search Term Array for a Match
	#	 $MsgDebugReadDir = $MsgDebugReadDir."<br /><br />SearchTerm = $SearchTerm<br />");
		 $key = array_search($SearchTerm, $SearchTerms);
		 # Add Search Term to List if no match
		 if ( $key == false )
		  $SearchTerms[count($SearchTerms)] = $SearchTerm;	# save the search Term to the last array element.
	#	  $MsgDebugReadDir = $MsgDebugReadDir."Key = $key<br />";
		}
	}
	
	#$MsgDebugReadDir = $MsgDebugReadDir."<br /><br />SearchTerms Length = ".count($SearchTerms)."<br />";
	#$MsgDebugReadDir = $MsgDebugReadDir."Here is the Search Term Listing <br />";
	#print_r ($DirListing);
	#print ("<br />");print ("<br />");
	#print_r ($SearchTerms);
	
	#Print the Search Terms and skip the first if their are some searchs to performe. 
	# Format the results to populate a list control
	# <option>value1</option>
	$ReadDirOutput = '';
	if ( count($SearchTerms) > 0 ){
		for ($i=1;$i<count($SearchTerms);$i++){
			$ReadDirOutput = $ReadDirOutput."<option>".$SearchTerms[$i]."</option>";
		}
	}
	print ($ReadDirOutput);
	return $ReadDirOutput;
}*/
?>
