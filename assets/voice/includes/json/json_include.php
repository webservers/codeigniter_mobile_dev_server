<?php
function toString($var=false){
	if (! function_exists('json_encode')){
		include_once 'JSON.php';
		$json = new Services_JSON();
		
		if($var)
			return $json->encode( $var );
		else
			return $json->encode( $this );
	} else {
		if($var)
			return json_encode( $var );
		else
			return json_encode( $this );	
	}
}

function toPrettyString(){
	include_once 'json_format.php';
	return json_format( $this->toString() );
}
?>