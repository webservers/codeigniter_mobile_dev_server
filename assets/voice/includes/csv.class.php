<?php
class csvFile{
	private $Header;	//holds the csv header in an array
	private $NumFields; //number of data items per row, defined by the header
	private $NumRows;	//number of lines
	private $f;			// the file class object, that does file IO
	private $Rows;		//hold file offsets to the beginning of each data Row
	function __construct($fileNamePath='', $mode=''){
		//should add support for adding a header, and appending it to the file
		$this->f = new file($fileNamePath, $mode);
		$this->Header = false;
		$this->SetHeader( $this->f->GetCsv() ); //a new file won't have data and returns false
		$this->read_rows();		
	}
	function __destruct(){
		$this->f->Close();
		unset($this->Rows);
		unset($this->Header);
	}
	function read_rows(){
		if( $this->Header ){ //no header no data ??
			$this->f->Seek(0);
			$this->Rows = array();
			$this->NumRows = 0;
			$this->f->GetCsv(); //seek past the header and ignore			
			$this->Rows[] = $this->f->Tell(); //set the first row offset location
			while( $this->f->GetCsv() ){
				$this->Rows[] = $this->f->Tell();
				$this->NumRows++;
			}
			//print_r($this->Rows); echo $this->NumRows;
			return true;
		}
		return false;
	}
	function SetHeader($ary){
	//must be write mode, will be if new file, because the file class will reopen in w+ mode
	//to create it.
	//
	//$ary = Array ( [0] => Description [1] => Date [2] => Cost [3] => Complete )
		if( is_array($ary) ){		
			$this->Header = $ary;
			$this->NumFields = count($this->Header); //get csv info
			return true;
		}
		return false;
	}
	function Num_Fields(){
		return $this->NumFields;
	}
	function Num_Rows(){
		return $this->NumRows;
	}
	function SeekRow($line){
		if($line > 0){
			$line--;	
			if($line <= $this->NumRows){
				$this->f->Seek($this->Rows[$line]);
				return $this->Rows[$line];
			}
		}
		return false;
	}
	function GetRow($line){
		if(	$this->SeekRow($line) !== false){
			return $this->f->GetCsv();
		}
		return false;
	}
	function GetHeader(){
		return $this->Header;
	}
	function SelectFieldLine($h, $line){
		$key = array_search($h, $this->Header);
		if($key !== false){	
			$r = $this->GetRow($line);
			return $r[$key];
		}
	}
	function SelectField($h){
		$r = array();
		$key = array_search($h, $this->Header);
		if($key !== false){	
			$this->f->Seek($this->Rows[0]); //seek start
		}
		while( $fields = $this->f->GetCsv() ){
			$r[] = $fields[$key];
		}
		return $r;
	}
	function SearchField($val, $h, $e, $limit=false, $descending=false, $mem=false){
	/* returns an array of line numbers that match
	   Array ( [0] => Array ( [0] => Online Banking payment to Crd 4664 [1] => 2011-03-22 [2] => -36.79 [3] => 1 [4] => 68 ) )
	
	   for mem=true: conservs memory by return line numbers only
	*/
		$lines = array();
		$key = array_search($h, $this->Header);
		if($key !== false){
			if($descending){
			 $i=1;
			} else {
			 $i=$this->NumRows;
			}
			for(;;){
				$l = $this->GetRow($i);
				//echo $l[$key].$e.'?'.$val.':'.compare($l[$key], $val, $e)."\n</br>";
				if(compare($l[$key], $val, $e)){
					if($mem)	$lines[] = $i;
					else{
						$l[] = $i; 						//add the file offset to the array, im using these as id values for html tags					
						$lines[] = $l;
					}
				}
				if($limit){
					if(count($lines) == $limit)
					 break;
				}
				if($descending){
				 $i++; if( $i<=$this->NumRows ) break;
				} else {
				 $i--; if( $i<1 ) break;
				}
			}
			return $lines;
		}
		return false;
	}
	function CsvString($ary){
		//the array is a key value pair, only values that are in the header are added
		$str = false;
		//was going to omit non allocated values, 
		if($this->NumFields == count($ary)){			//but just make sure their the same size then add the comma's
			for($i=0;$i<count($this->Header)-1;$i++){   //start with the first header field
				$key = $this->Header[$i];
				if(array_key_exists($key, $ary)){ //find it in the passed in variable
					$str .= '"'.$ary[$key].'",';  //create a string using the association
				} else {
					$str .= '" '.'",'; //if not exits in header append space
				}
			}
			if(array_key_exists($this->Header[$i], $ary)){   //no comma for the last item
				$str .= '"'.$ary[$this->Header[$i]].'"'."\n";
			} else {
				$str .= '" '.'"'."\n";
			}
			return $str;
		}	
	}
	function CsvString1($ary){
		$str = '';
		for($i=0;$i<count($this->Header)-1;$i++){   //start with the first header field
			$str .= '"'.$ary[$i].'",';  //create a string using the association
		}
			$str .= '"'.$ary[$i].'"'."\n";
		return $str;
	}
	function Append($ary){
		//the array is a key value pair, only values that are in the header are added
		$str = $this->CsvString($ary);
		//was going to omit non allocated values, but just make sure their the same size
		//then add the comma's
		if($str){
			$this->f->Append($str,strlen($str));
			$this->read_rows(); 	//this is a quick fix
			return $this->NumRows;
		}
		return false;
	}
	function UpdateByExactMatch($ary, $to){
	//match a record from the ary variable and replace with the to variable
			$row = $this->exact_match($ary);
			//var_dump($s);
			if($row){				
				$this->UpdateLineByNewFile($to, $row);
			}
		return false;
	}
	function UpdateByRowNum($ary, $row){
		if($row <= $this->NumRows){
			$this->UpdateLineByNewFile($ary, $row);
			return true;
		}
		return false;
	}
	function print_offsets(){
		for($i=0;$i<count($this->Rows);$i++){
			echo "Line: $i, Offset:".$this->Rows[$i]."\n";
		}	
	}
	function exact_match($ary){
		$line = false;
		if(count($this->Header) == count($ary)){
			$s = $this->SearchField($ary[$this->Header[0]], $this->Header[0], '=', true); //only matches the 1st field to start with	
			if(count($s)>=1){
				//var_dump($s);
				$r1 = array_values($ary);
				for($i=0;$i<count($s);$i++){
					$r2 = array_values($this->GetRow($s[$i]));
					$match = false;
					if(count($r1) == count($r2) ){
						$match = true;
						for($j=0;$j<count($r1);$j++){
							if(!compare($r1[$j], $r2[$j], '=')){
								$match = false;
							}
						}
					}
					if($match){
						$line = $s[$i];
						break;
					}
				}
			}
		}
		return $line; //returns Array ( [0] => Online Banking payment to Crd 4664 [1] => 2011-03-22 [2] => -36.79 [3] => 1 [4] => 68 )
	}
	function UpdateField($ary, $h, $row=0){
	/*
		updates multiple rows of one field value starting at $row
	*/	
		$key = array_search($h, $this->Header);
		if( $key === false ) return false;
		$nf = new file('csv.txt', 'r+'); //open new file seek to start on old, and write the header
		$crow = 0;
		$this->f->Seek(0);
		$tString = $this->f->GetLine(); //write the header
		while($crow != $this->NumRows){
			$nf->Write($tString); //echo $tString;
			if($crow >= $row){
			  //create the string from the array here
			  $r = $this->f->GetCsv(); $r[$key] = $ary[($crow-$row)]; //var_dump($r); die();
			  $tString = $this->CsvString1($r);
			} else {
			  $tString = $this->f->GetLine();
			}
			$crow++;
		}
		$nf->Write($tString); //echo $tString;
		$fn = $this->f->Fname_path;
		if($this->f->Delete()){
			$this->f = $nf->Rename($fn);
			$this->Header = false;
			$this->SetHeader( $this->f->GetCsv() ); //a new file won't have data and returns false
			$this->read_rows();
			return true;
		}
		return false;
	}
	function UpdateLineByNewFile($ary, $row){
		/*
		  rewrite the file and update calling the constructor
		  or delete the data and update the file, but can't seem to do that,
		  because the offsets change, unless each line is pre allocated, but
		  then your stuck

		  could write to a new file, deconstruct rename and construct.
		*/
		$str = $this->CsvString($ary);
		//var_dump($str);
		if($str){
			$crow = 0;
			$nf = new file('csv.txt', 'r+'); //open new file seek to start on old, and write the header
			$this->f->Seek(0);
			$nf->Write($this->f->GetLine());
			while($crow != $this->NumRows){
				$this->f->Seek($this->Rows[$crow]);
				if($crow == $row){
				  //update the line with the new data
				  $nf->Write($str);
				} else {
				  $nf->Write($this->f->GetLine());
				}
				$crow++;
			}
			$fn = $this->f->Fname_path;
			if($this->f->Delete()){
				$this->f = $nf->Rename($fn);
				$this->Header = false;
				$this->SetHeader( $this->f->GetCsv() ); //a new file won't have data and returns false
				$this->read_rows();
				return true;
			}
		}
		return false;
	}

				//I'll just append the data to the end of the file and erase the old contents
				//
				//I could use the code below and specify a string length for each header value
				//possibly by using maxlength function upon data inport, then padding, the strings
				//and terminiating in the c-style using char(0) aka \0, then I'd have the space
				//already allocated, and would have to write the differance in the line offset in bytes
				//but this would probally case incompatibility problems that a user would think would work with the files
				//and cause corruption after editing with a csv compatible editor. one solution to that
				//would be to have a different file extension that I could use for this file parser only.
				//The benifits would be fast parsing and updating
				//
				//I could then basically create my own header definition like
				//"headerText:size",headerText:size"\n
				//then I would be able to rewrite the file using the above scheme,
				//but I should extend the class in a seperate file so I'd have a compatible
				//csv implementation.
				//
				//GetCsv() returns a string in an array when trying to parse a line with only null chars and terminated on new line
				/*
				$start = $this->Rows[$line-1];
				$stop = $this->Rows[$line];
				$num_bytes = $stop - $start;
				echo "Line: $line, Offset:".$this->Rows[$line-1].", Num Bytes:".$num_bytes."\n";
				$this->f->Seek($this->Rows[$line-1]);
				for($i=0;$i<$num_bytes-1;$i++)
					$this->f->Write("\0");
				$this->f->Write("\n");
				//
				//$this->Append($ary);
				*/	
}
//try to append data between the header and the first data item!!
//$cf = new csvFile('vix.csv', 'r+');

//$cf = new csvFile('vix.csv', 'r');
//echo 'Num Rows:'.$cf->num_rows()."\n";
//print_r($cf->GetHeader());
// The Date Functions are slow because of strtotime()
//$r = $cf->SearchField('2010-02-01', 'Date', '='); print_r($r);
//$r = $cf->SearchField('2010-02-01', 'Date', '=', true); print_r($cf->GetRow($r[0]));

// Selection Functions
//print_r($cf->SelectField('Close'));
//echo $cf->SelectFieldLine('Close', 500);

?>