<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function httpError() {
	$HttpStatus = $_SERVER["REDIRECT_STATUS"];
	if ($HttpStatus == 200) {print "Document has been processed and sent to you.";
	}
	if ($HttpStatus == 400) {print "Bad HTTP request ";
	}
	if ($HttpStatus == 401) {print "Unauthorized - Iinvalid password";
	}
	if ($HttpStatus == 403) {print "Forbidden";
	}
	if ($HttpStatus == 500) {print "Internal Server Error";
	}
	if ($HttpStatus == 418) {print "I'm a teapot! - This is a real value, defined in 1998";
	}
}

function randomFileName() {
	return md5(time() . "tt");
}

function getJsonError() {
	$s = "";
	switch(json_last_error()) {
		case JSON_ERROR_CTRL_CHAR :
			$s = "Control character error, possibly incorrectly encoded\n";
			break;
		case JSON_ERROR_DEPTH :
			$s = "The maximum stack depth has been exceeded\n";
			break;
		case JSON_ERROR_NONE :
			$s = "No error has occurred\n";
			break;
		case JSON_ERROR_STATE_MISMATCH :
			$s = "Invalid or malformed JSON\n";
			break;
		case JSON_ERROR_SYNTAX :
			$s = "Syntax error";
			break;
		case JSON_ERROR_UTF8 :
			$s = "Malformed UTF-8 characters, possibly incorrectly encoded\n";
			break;
	}
	return $s;
}

function stringify($str) {
	//http://www.php.net/manual/en/json.constants.php
	$o = new stdClass();
	$o -> json = json_encode($str);
	// & 'JSON_FORCE_OBJECT' && 'JSON_ERROR_NONE' 'JSON_UNESCAPED_SLASHES'
	//$o->json = json_encode($str, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE); // & 'JSON_FORCE_OBJECT' && 'JSON_ERROR_NONE'
	$o -> error = $this -> getJsonError();
	return $o;
}

function switch_err($e) {
	$m = '';
	//$_FILES['userfile']['error']
	switch($e) {
		case UPLOAD_ERR_OK :
			$m = "There is no error, the file uploaded with success.";
			break;
		case UPLOAD_ERR_INI_SIZE :
			$m = "The uploaded file exceeds the upload_max_filesize of " + ini_get('upload_max_filesize') + " directive in php.ini";
			break;
		case UPLOAD_ERR_FORM_SIZE :
			$m = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
			break;
		case UPLOAD_ERR_PARTIAL :
			$m = "The uploaded file was only partially uploaded.";
			break;
		case UPLOAD_ERR_NO_FILE :
			$m = "No file was uploaded.";
			break;
		case UPLOAD_ERR_NO_TMP_DIR :
			$m = "Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3";
			break;
		case UPLOAD_ERR_CANT_WRITE :
			$m = "Failed to write file to disk. Introduced in PHP 5.1.0.";
			break;
		case UPLOAD_ERR_EXTENSION :
			$m = "A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.";
			break;
	}
	return $m;
}

function copyObjProperties($ob1, $ob2) {
	$vars = get_object_vars($ob2);
	$keys = array_keys($vars);
	$str = "";
	//var_dump($ary);
	for ($i = 0; $i < count($vars); $i++) {
		//$str = $ob2->$property;
		//echo "key: " . $keys[$i] . ", value: " . $vars[$keys[$i]];
		$ob1 -> $keys[$i] = $vars[$keys[$i]];
	}
	//var_dump($ob1);
	return $ob1;
}

function decodeParam($request) {
	$_json = json_decode(stripcslashes($request -> arguments));
	if ($_json === null) {
		//echo getJsonError(); //Syntax error '?message="test"'   '{message: "msg1", data: "test"}'
		$_json = json_decode($request -> arguments);
		if ($_json === null) {
			die('{"success":false, "message":"json Param not set"}');
		}
		//var_dump($_json);
		//var_dump($request->arguments);
		//var_dump($request->arguments->params);
	}
	return $_json;
}



function getHTTPError($e) {
	return $this -> switch_err($e);
}

function _urlEncode($params) {
	$url = "";
	try {
		if (gettype($params) == 'string') {//passed in string, means already url encoded
			$url .= $params;
		} else if (gettype($params) == 'object') {//object needs to be stringified
			$url .= '?';
			foreach ($params as $key => $param) {
				if (gettype($param) == 'object') {
					$url .= $key . '=' . json_encode($param) . '&';
				} else {
					$url .= $key . '=' . $param . '&';
				}
			}
			$url = substr($url, 0, strlen($url) - 1);
		}
		return $url;
	} catch(Exception $e) {
		echo 'Caught exception: ', $e -> getMessage(), "\n";
	}
}

function combineArrays($a1, $a2) {
	//$keys = array_keys($a2);
	//for($i =0;$i< count($a2);$i++){
	//$a1[$key]
	//$a1[] = $a2[$i];
	//}
	array_push($a1, $a2);
	return $a1;
}

function getFileSize($file) {
	try {
		$s = "";
		$fz = filesize($file);
		ini_set('precision', 6);
		if ($fz < 1000000)
			$s = ($fz / 1024) . "Kb";
		else
			$s = ($fz / 1048576) . "Mb";
	} catch(Exception $e) {

	}
	return $s;
}

function move($filename, $filenameNew){
	if( is_uploaded_file($filename) ){
		return move_uploaded_file($filename, $filenameNew);		
	}
	return rename($filename, $filenameNew);	
}

