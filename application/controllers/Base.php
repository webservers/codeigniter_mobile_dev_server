<?php

if (! defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Add Common Libraries here and everything will extend it
 */
// ini_set('post_max_size', '32M');
// ini_set('upload_max_filesize', '32M');
class Base extends CI_Controller
{

    public $per_page_default = 20;

    public $htmlPath = '';

    public $sys_path = '/var/www/html/';

    public $sys_dir = 'ci_dev';

    public $uploadDir = 'uploads';

    public $thumbDir = 'thumbs';

    public $imageDir = 'graphics';

    public $soundDir = 'audio';

    public $appsDir = 'apps';

    public $archiveDir = 'archives';

    public $validImageExtensions = array(
        'jpg',
        'jpeg',
        'png',
        'gif',
        'bmp',
        'ps'
    );

    public $validImageTypes = 'gif|jpg|png|jpeg|x-png|ps';

    public $validSoundExtensions = array(
        'wav',
        'mp3',
        'aif',
        'm4a',
        'wma',
        'flac'
    );

    public $validSoundTypes = 'wav|aif|mp3|wma|flac';

    public $validVideoExtensions = array(
        'mp4',
        'mov',
        'avi',
        'mpeg',
        'qt',
        'divx'
    );

    public $validVideoTypes = 'mp4|mov|avi|mpeg|qt|divx';

    public $validArchiveExtensions = array(
        'zip',
        '7z',
        'gz',
        'rar, tar, tgz'
    );

    public $validArchiveTypes = 'zip|7z|rar|gz|tar|tgz';

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('base');
        $this->load->helper('path');
        $this->load->database();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('user_agent');

        $this->load->library('email');

        // $this->load->library('log');
        $this->load->model('Fileuploads_model');
        $this->load->model('Session_model');
        $this->load->model('Logs_model');
        $this->load->model('User_model');
        $this->load->model('Data_model');
        /*
         * if(!isset($this->session->userdata['user_id']) ){
         * $this->session->set_userdata('validated', false);
         * $this->session->set_userdata('username', 'guest');
         * $this->session->set_userdata('type', 0);
         * $this->session->set_userdata('user_id', 4);
         * }
         */

        // have to declare this before updateing uploadDir
        // $config['base_url'] = 'http://bluemountaintechnologies.com/BMTmobile/uploads/';
        // $this->htmlPath = $this->config->item('base_url') . $this->uploadDir . '/'; //url path to uploads
        // $this->htmlPath = 'http://localhost/' . $this->sys_dir . '/' . $this->uploadDir . '/'; //url path to uploads
        $this->htmlPath = base_url() . $this->uploadDir . '/';
        // '/home/128574/domains/bluemountaintechnologies.com/html/uploads/'
        $this->uploadDir = $this->sys_path . $this->sys_dir . '/' . $this->uploadDir . '/';

        // '/home/128574/domains/bluemountaintechnologies.com/html/uploads/thumbs/'
        // $this->thumbDir = $this->uploadDir . $this->thumbDir . '/';
        // $this->imageDir = $this->uploadDir . $this->imageDir . '/';
        // $this->soundDir = $this->uploadDir . $this->soundDir . '/';
        // $this->appsDir = $this->uploadDir . $this->appsDir . '/';
        // $this->archiveDir = $this->uploadDir . $this->archiveDir . '/';

        // $this->sessions->encryption_key;
        // $this->sessions->userdata->session_id
        // $this->session->userdata->ip_address
        // $this->session->userdata->last_activity
        // $this->session->userdata->user_agent
    }

    public function sendEmail($name, $from, $to, $subj, $msg)
    {
        $this->email->from($from, $name);
        $this->email->to($to);
        // $this->email->cc('another@another-example.com');
        // $this->email->bcc('them@their-example.com');

        $this->email->subject($subj);
        $this->email->message($msg);

        $this->email->send();
    }

    private function parseUserAgent($aSess)
    {
        for ($i = 0; $i < count($aSess); $i ++) {
            if ($aSess[$i]->ip_address == '::1') {
                $aSess[$i]->ip_address = '127.0.0.1';
            }
            $this->agent->parseAgent($aSess[$i]->user_agent);
            // echo $Session->user_agent."\n";
            // $Session->session_id
            /*
             * if ($this -> agent -> is_mobile()) {
             * $aSess[$i] -> browser = $this -> agent -> mobile();
             * $aSess[$i] -> mobile = true;
             * } else if ($this -> agent -> is_browser()) {
             * $aSess[$i] -> browser = $this -> agent -> browser();
             * $aSess[$i] -> mobile = false;
             * //.' '.$this->agent->version();
             * } elseif ($this -> agent -> is_robot()) {
             * $agent = $this -> agent -> robot();
             * $aSess[$i] -> mobile = false;
             * } else {
             * $aSess[$i] -> browser = 'Unknown';
             * $aSess[$i] -> mobile = 'Unknown';
             * }
             */

            $aSess[$i]->platform = $this->agent->platform();
            $aSess[$i]->browser = $this->agent->browser();
            $aSess[$i]->mobile = false;
            if ($this->agent->is_mobile())
                $aSess[$i]->mobile = true;
            // $aSess[$i] -> browser = $this -> agent -> mobile();
        }
    }

    private function eachFileInDir($Dir, $validExtensions)
    {
        $o = new stdClass();
        $o->path = $this->uploadDir . $Dir;
        if (is_dir($o->path)) {
            $o->total = 0;
            $o->children = array();
            if ($dh = opendir($o->path)) {
                while (($file = readdir($dh)) !== false) {
                    // get file extension
                    // echo $file."\n";
                    if (($file != '.' && $file != '..')) {
                        $ext = strtolower(strrev(strstr(strrev($file), ".", TRUE)));
                        if (in_array($ext, $validExtensions, true)) {
                            $co = new stdClass();
                            $co->file_name = $file;
                            $co->path = $o->path . "/" . $file;
                            $co->url = $this->htmlPath . $Dir . '/' . $file;
                            // $co->url = $this->htmlPath
                            /*
                             * $fz = filesize($path."/".$file);
                             * ini_set('precision', 6);
                             * if($fz < 1000000)
                             * $co->size = ($fz / 1024)."Kb";
                             * else
                             * $co->size = ($fz / 1048576)."Mb";
                             */
                            $o->children[] = $co;
                            $o->total ++;
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $o;
    }

    private function eachDirInDir($Dir)
    {
        $o = new stdClass();
        $o->path = $this->uploadDir . $Dir;
        // echo $o->path;
        $o->children = array();
        if (is_dir($o->path)) {
            // $o->total = 0;
            if ($dh = opendir($o->path)) {
                while (($file = readdir($dh)) !== false) {
                    if (($file != '.' && $file != '..') && is_dir($o->path . '/' . $file)) {
                        $o->dir_name = $file;
                        // $co->path = $path . "/" . $file;
                        // $co->url = $this->htmlPath . $Dir . '/' . $file;
                        $o->children[] = $file;
                        // echo "FileName: ".$file.", isDir: true\n";
                    } else {
                        // echo "FileName: ".$file.", isDir: false\n";
                    }
                    // $o->total++;
                }
                closedir($dh);
            }
        }
        // unset($o->path);
        $o->path = $this->htmlPath . $Dir;
        if (is_array($o->children))
            $o->total = count($o->children);
        else
            $o->total = 0;
        return $o;
    }

    private function createJsonMSG($success, $status, $message = "")
    {
        $r = new stdClass();
        $r->success = $success;
        $r->message = $message;
        $r->status = $status;
        return $r;
    }

    private function getJsonSuccessObj()
    {
        return $this->createJsonMSG(true, 1, "");
    }

    private function getJsonFailureObj()
    {
        return $this->createJsonMSG(false, 0, "");
    }

    private function echoJSONSuccess($MSG = "")
    {
        // echo '{"success": "true"}';
        $o = $this->getJsonSuccessObj();
        $o->message = $MSG;
        echo json_encode($o);
    }

    private function echoJSONFailure($MSG = "")
    {
        // echo '{"success": "false"}';
        $o = $this->getJsonFailureObj();
        $o->message = $MSG;
        echo json_encode($o);
    }

    // now returns obj with perPage, currentPage,
    private function arrayPaging($ary, $url, $per_page = -1)
    {
        // $o = new stdClass();
        // $o->per_page = $perPage;
        // $o->total = count($ary);
        // $o->TotalPerPageRatio = $o->total / $o->per_page;
        // $o->num_pages = intval($o->TotalPerPageRatio) + 1;
        $pp = $this->per_page_default;
        if (is_int($per_page) && $per_page != - 1)
            $pp = $per_page;
        $len = 0;
        if (gettype($ary) == "object")
            $len = count($ary);
        if ($per_page == - 1)
            $pp = $len;
        $c_paginate['base_url'] = $url; //
        $c_paginate['per_page'] = $pp;
        $c_paginate['total_rows'] = $len;
        // $c_paginate['uri_segment'] = 3;
        // $c_paginate['num_links'] = 2;
        $c_paginate['use_page_numbers'] = TRUE; // <a href=\"http:\/\/bluemountaintechnologies.com\/BMTmobile\/index.php\/uploadJSON\/graphics\/2\">2<\/a>&nbsp;
        $c_paginate['page_query_string'] = FALSE; // http://example.com/index.php/test/page/20
                                                  // $c_paginate['page_query_string'] = TRUE; //http://example.com/index.php?c=test&m=page&per_page=20
        /*
         * $c_paginate['full_tag_open'] = '{';//'fto';
         * $c_paginate['full_tag_close'] = '}';//'ftc';
         * $c_paginate['first_link'] = '"first_link":';//'fl';
         * $c_paginate['next_link'] = '';//'nl';
         * $c_paginate['prev_link'] = '';//'pl';
         * $c_paginate['last_link'] = '"last_link":';//'ll';
         * $c_paginate['first_tag_close'] = '';
         * $c_paginate['last_tag_open'] = '';
         * $c_paginate['cur_tag_open'] = '"current_page":'; //'cto';
         * $c_paginate['cur_tag_close'] = ','; //'ctc';
         * $c_paginate['next_tag_open'] = '"next_link":'."'"; //'nto';
         * $c_paginate['next_tag_close'] = "',"; //'ntc';
         * $c_paginate['prev_tag_open'] = '"prev_link":'."'";//'pto';
         * $c_paginate['prev_tag_close'] = "',";//'ptc';
         * $c_paginate['num_tag_open'] = '"link":'."'" ;//'nto';
         * $c_paginate['num_tag_close'] = "',";
         */
        $this->pagination->initialize($c_paginate);
        // return $this->pagination->create_linksJSON(); //returns json string
    }

    /*
     * These encode a json tree for upload directories
     *
     */
    private function image()
    {
        $path = "graphics";
        $o = $this->createJsonMSG(true, 1, $message = "Image Files");
        $o->images = $this->eachFileInDir($path, $this->validImageExtensions);
        if (isset($o->images->children) && is_array($o->images->children)) {
            foreach ($o->images->children as $image) {
                $image->thumb_url = $this->htmlPath . "/" . $path . "/thumbs/thumb_" . $image->file_name;
                /*
                 * if (function_exists('getimagesize')){
                 * if (FALSE !== ($D = @getimagesize($image->path))){
                 * //$types = $this->validImageTypes;
                 * $image->width = $D['0'];
                 * $image->height = $D['1'];
                 * }
                 * }
                 */
                unset($image->path);
            }
        }
        // var_dump($o);
        // $o->paging = $this->arrayPaging($o->images, site_url('base/image'));
        /*
         * if($o->paging->current_page != 1){
         * $page = $o->paging->current_page;
         * //split the array according to page#
         * }
         */
        // echo json_encode($o);
        return $o;
    }

    private function audio()
    {
        $path = "sounds";
        $o = $this->createJsonMSG(true, 1, $message = "Audio Files");
        $o->sounds = $this->eachFileInDir($path, $this->validSoundExtensions);
        // $o->paging = $this->arrayPaging($o->sounds, site_url('base/audio'));
        /*
         * if($o->paging->current_page != 1){
         * $page = $o->paging->current_page;
         * //split the array according to page#
         * }
         */
        // echo json_encode($o);
        return $o;
    }

    private function archives()
    {
        $path = 'archives';
        $o = $this->createJsonMSG(true, 1, $message = "Archived Files");
        $o->archives = $this->eachFileInDir($path, $this->validArchiveExtensions);
        // var_dump($o);
        // $o->paging = $this->arrayPaging($o->archives, site_url('base/archives'));
        /*
         * if($o->paging->current_page != 1){
         * $page = $o->paging->current_page;
         * //split the array according to page#
         * }
         */
        // echo json_encode($o);
        return $o;
    }

    private function apps()
    {
        $path = $this->appsDir;
        // var_dump($this->eachDirInDir($path));
        $o = $this->createJsonMSG(true, 1, $message = "Project Directories");
        $o->apps = $this->eachDirInDir($path);
        // $o->paging = $this->arrayPaging($o->apps, site_url('base/apps'));
        return $o;
    }

    // Uploads --------------------------------------------------------------------------------------------------
    private function loadUploadLib($path)
    {
        $config = array();
        $config['upload_path'] = $path;
        $config['allowed_types'] = $this->validImageTypes . '|' . $this->validSoundTypes . '|' . $this->validVideoTypes . '|' . $this->validArchiveTypes;
        // var_dump($config['allowed_types']); return;
        $config['file_name'] = randomFileName();
        // $config['max_size'] = '100';
        // $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $this->load->library('upload', $config);
    }

    private function getContentType($file_type)
    {
        // image/jpg
        // audio/mpeg
        // audio/x-wav
        // application/zip
        if (stripos($file_type, 'image') !== false) {
            return 'image';
        }
        if (stripos($file_type, 'audio') !== false) {
            return 'audio';
        }
        if (stripos($file_type, 'application') !== false) {
            return 'app';
        }
        return 'unknown';
    }

    private function createThumbNail($img, $new_image, $width, $height)
    {
        $c_img_lib = array(
            'image_library' => 'gd2',
            'source_image' => $img['full_path'],
            'maintain_ratio' => TRUE,
            'width' => $width,
            'height' => $height,
            'new_image' => $new_image
        );
        $this->load->library('image_lib', $c_img_lib);
        $this->image_lib->resize();
    }

    private function createFileJSON($ul_data)
    {
        $o = new stdClass();
        $o->file_name = $ul_data['file_name'];
        $o->file_type = $ul_data['file_type'];
        $o->client_name = $ul_data['client_name'];
        $o->file_size = $ul_data['file_size'];
        return $o;
    }

    private function image_upload($Log_id, $ul_data)
    {
        // if($ul_data['is_image']){
        $o = $this->createFileJSON($ul_data);
        $o->path = $this->uploadDir . $this->imageDir . '/';

        // echo "Moving Upload file from " . $this->uploadDir . $ul_data['file_name'] . 'to destination ' . $this->uploadDir . $this->imageDir . '/' . $ul_data['file_name'];
        $e = move($this->uploadDir . $ul_data['file_name'], $o->path . $ul_data['file_name']);
        $o->file_size = getFileSize($o->path . $ul_data['file_name']);
        $o->attributes = new stdClass();
        $o->attributes->thumbNail = new stdClass();
        // = $this->thumbDir.'/thumb_'.$ul_data['file_name'];
        $o->attributes->thumbNail->path = $this->thumbDir;
        $o->attributes->thumbNail->file_name = '/thumb_' . $ul_data['file_name'];
        $o->attributes->thumbNail->width = 100;
        $o->attributes->thumbNail->height = 100;
        if (function_exists('getimagesize')) {
            if (FALSE !== ($D = @getimagesize($o->path . $ul_data['file_name']))) {
                // $types = $this->validImageTypes;
                $o->attributes->width = $D['0'];
                $o->attributes->height = $D['1'];
            }
        }
        $this->createThumbNail($ul_data, $o->attributes->thumbNail, $o->attributes->thumbNail->width, $o->attributes->thumbNail->height);
        $o->log_id = $Log_id;
        $o->user_id = $this->session->userdata('user_id');
        $this->Fileuploads_model->insert($o);
        $o->attributes = json_encode($o->attributes);
        return $o;
        // }
    }

    private function sound_upload($Log_id, $ul_data)
    {
        // if($ul_data['is_image']){
        $img = $this->upload->data();
        $o = $this->createFileJSON($img); // file name goes here
        $o->path = $this->uploadDir . $this->soundDir . '/';
        // echo "Moving Upload file from " . $this->uploadDir . $ul_data['file_name'] . 'to destination ' . $this->uploadDir . $this->soundDir . '/' . $ul_data['file_name'];
        $e = move($this->uploadDir . $ul_data['file_name'], $o->path . $ul_data['file_name']);
        $o->file_size = getFileSize($o->path . $ul_data['file_name']);
        $o->attributes = new stdClass();
        $o->attributes = json_encode($o->attributes);
        $o->user_id = $this->session->userdata('user_id');
        $o->log_id = $Log_id;
        $this->Fileuploads_model->insert($o);
        return $o;
        // }
    }

    private function archive_upload($Log_id, $ul_data)
    {
        // if($ul_data['is_image']){
        $ul_data = $this->upload->data();
        $o = $this->createFileJSON($ul_data);
        $o->path = $this->uploadDir . $this->archiveDir . '/';
        // echo "Moving Upload file from " . $this->uploadDir . $ul_data['file_name'] . 'to destination ' . $this->uploadDir . $this->appsDir . '/' . $ul_data['file_name'];
        // echo "Moving Upload file from " . $this->uploadDir . $ul_data['file_name'] . 'to destination ' . $this->uploadDir . $this->archiveDir . '/' . $ul_data['file_name'];
        $e = move($this->uploadDir . $ul_data['file_name'], $o->path . $ul_data['file_name']);
        $o->file_size = getFileSize($o->path . $ul_data['file_name']);
        $o->attributes = new stdClass();
        $o->attributes = json_encode($o->attributes);
        $o->log_id = $Log_id;
        $o->user_id = $this->session->userdata('user_id');
        $this->Fileuploads_model->insert($o);
        return $o;
        // }
    }

    private function do_upload($Log_id = null)
    {
        if (is_numeric($Log_id)) {
            $this->loadUploadLib($this->uploadDir); // set upload to /uploads dir and move after it's received
                                                    // $file_name =
            if (! $this->upload->do_upload('filecontent')) { // can't get content uptil upload is complete
                                                             // return strip_tags($this->upload->display_errors());
                $o = $this->createJsonMSG(false, 0, strip_tags($this->upload->display_errors()));
                echo json_encode($o);
            } else {
                $ul_data = $this->upload->data();
                //
                // echo $ul_data['file_type']."\n";
                // returns obj so that properties for other queries can be added before echo
                $o->files = $_FILES;
                switch ($this->getContentType($ul_data['file_type'])) {
                    case 'image':
                        $o->result = $this->image_upload($Log_id, $ul_data); // echo json string
                        break;
                    case 'audio':
                        $o->result = $this->sound_upload($Log_id, $ul_data);
                        break;
                    case 'application': // zip shows as application
                        $o->result = $this->archive_upload($Log_id, $ul_data);
                        break;
                }
                $o = createJsonMSG(true, 1, "successful upload");
                $o->log_id = $Log_id;
                echo json_encode($o);
                return true;
            }
        }
        return false;
    }

    /**
     * delete_upload
     *
     * @param $o o->path
     *            o->file_name o->file_type
     * @return void
     * @author brad steffy
     */
    private function delete_upload($o)
    {
        try {
            if (is_object($o) && is_numeric($o->id)) {
                $r = false;
                $r = $this->Fileuploads_model->deleteFileName($o->id);
                if ($r) {
                    // delete the actual file
                    $file_name = $o->path . $o->file_name;
                    switch ($this->getContentType($o->file_type)) {
                        case 'image':
                            $thumbGraphic = $o->path . 'thumbs/thumb_' . $o->file_name;
                            break;
                        case 'audio':
                            break;
                    }
                }
            }
            $file_type = 'image';
            if ($file_type == 'image') {
                $fileName = $_POST['file_name'];
                $graphic = $this->uploadDir . '/graphics/' . $fileName;
                $thumbGraphic = $this->uploadDir . '/graphics/thumbs/thumb_' . $fileName;
                // delete the actual files..
                if (is_file($graphic))
                    $r = $r && unlink($graphic);
                if (is_file($thumbGraphic))
                    $r = $r && unlink($thumbGraphic);
                if ($r) {
                    $this->echoJSONSuccess();
                } else {
                    $this->echoJSONFailure("failed to unlink file " . $thumbGraphic . " or " . $graphic);
                }
                return;
            } else {}
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    /*
     * PUBLIC METHODS Exposed to url
     */
    public function getImages()
    {
        $o = $this->image();
        echo json_encode($o);
    }

    public function getApps()
    {
        $o = $this->apps();
        echo json_encode($o);
    }

    public function getUploadDetail()
    {
        $r = $this->Fileuploads_model->getDetailByFileName();
        echo json_encode($r);
        // return $r;
    }

    public function getFiles()
    {
        $o = new stdClass();
        $o->files = $this->Fileuploads_model->get_entries();
        echo json_encode($o);
    }

    public function getArchives()
    {
        $o = $this->archives();
        echo json_encode($o);
    }

    /*
     *
     * Session Login logout Validate user
     *
     *
     */
    public function isValidated()
    {
        if ($this->session->userdata('validated'))
            return true;
        return false;
    }

    public function login()
    { // post: user_name, password or pass
        $o = $this->User_model->validate_user();
        if (is_object($o)) {
            // $this->log->write_log ($level= 'error', 'user type: '.$this->session->userdata('type').', name:'.$this->session->set_userdata('username'), FALSE);
            $this->session->set_userdata('validated', true);
            $this->session->set_userdata('username', $this->User_model->user_name);
            $this->session->set_userdata('type', $this->User_model->type);
            $this->session->set_userdata('user_id', $this->User_model->id);
            // $o = $this->session->user_data;
            // unset($o->user_name);
            unset($o->password);
            $r = $this->createJsonMSG(true, 1, "login successful");
            $r->session = $o;
            $r->session->session_id = $this->session->userdata['session_id'];
        } else {
            $r = $this->createJsonMSG(false, 0, 'Incorrect username or password!');
        }
        echo json_encode($r);
        return $r;
    }

    public function logout()
    {
        // $this->session->unset_userdata('username');
        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }

    /*
     * Log related message, timestamp, data, session_id, id
     *
     *
     */
    public function jsonLogsBySessionsActivity()
    {
        // "SELECT DISTINCT `Logs`.Session_id FROM `Logs`;"
        // "SELECT `session_id` FROM `ci_sessions`;"
        $r = $this->Log_model->LogsFromInactiveSession();
        $matches = array(); // Logs with active sessions
        $notMatches = array();
        // var_dump($r);
        // 0: Logs
        // 1: ci_sessions
        // Match the Session Id's
        for ($i = 0; $i < count($r[0]); $i ++) {
            for ($j = 0; $j < count($r[1]); $j ++) {
                if ($r[0][$i]->Session_id == $r[1][$j]->session_id) {
                    // echo "match: ".$i.", ".$r[0][$i]->Session_id."\n";
                    $matches[] = $r[0][$i]->Session_id;
                }
            }
        }
        // weed out the Id's that don't have a match,
        // these don't have an entry in the ci_sessions TABLE, because they expired
        for ($i = 0; $i < count($r[0]); $i ++) {
            if (array_search($r[0][$i]->Session_id, $matches) === false) {
                // echo "!match: ".$i.", ".$r[0][$i]->Session_id."\n";
                $notMatches[] = $r[0][$i]->Session_id;
            }
        }

        // Json Encoding an object provides the best results
        $o = new stdClass();
        $o->Logs = array();
        if (count($matches) > 0)
            $o->Logs['active'] = $matches;
        else
            $o->Logs['active'] = null;
        if (count($notMatches) > 0)
            $o->Logs['inActive'] = $notMatches;
        else
            $o->Logs['inActive'] = null;
        // var_dump($o);
        echo json_encode($o);
    }

    public function jsonActiveSessions()
    {
        $o = new stdClass();
        $o->Sessions = Array();
        $r = $this->Log_model->getSession_Ids();
        if (is_array($r)) {
            for ($i = 0; $i < count($r); $i ++) {
                // if($session_obj->id != null)
                $rs = $this->Session_model->getSessionEquals($r[$i]->session_id);
                if ($rs != null) {
                    $o->Sessions[] = $rs[0];
                }
            }
            // var_dump($o->Sessions);
            $this->parseUserAgent($o->Sessions);
            $o->success = true;
            $o->message = "";
            $o->status = 1;
        } else {
            $o->success = false;
            $o->message = "no data available";
            $o->status = 0;
        }
        echo json_encode($o);
    }

    public function jsonAllSessions()
    {
        $o = new stdClass();
        $o->Sessions = $this->Session_model->getSessions();
        $this->parseUserAgent($o->Sessions);
        // var_dump($o->Sessions);
        $o->success = true;
        $o->message = "";
        $o->status = 1;
        echo json_encode($o);
    }

    public function jsonAccess()
    {
        $data = $this->Log_model->get_last_ten_Access();
        // $this->load->view('db_output_test', $data);
        $o = new stdClass();
        $o->Access = $data;
        echo json_encode($o);
    }

    public function yourSession()
    {
        // $o = new stdClass(); //$o->Logs = $this->session->userdata;
        echo json_encode($this->session->userdata);
    }

    // Meant as a utility for garbage collection
    public function get_unused_sessions()
    {
        // $sessions = $this->Sessions_model->getSessions();
        // find where id not exist in logs
        // find where id not exist in Files
    }

    public function reset_database()
    {
        $this->Log_model->clear();
        $this->Fileuploads_model->clear();
        $this->Session_model->clear();
        $this->User_model->clear();
        $this->Data_model->clear();
        $r = $this->getJsonSuccessObj();
        $r->message = "Successfully cleared Database";
        echo json_encode($r);
    }

    // * delete_upload @param $o o->path o->file_name o->file_type
    public function delete_log()
    {
        // must match user and log id
        // $o->log_id = $this->Log_model->insert($this->session->userdata['session_id'])
        $r = $this->Log_model->delete();
        // var_dump($r);
        if ($r != false) {
            $r = $this->getJsonSuccessObj();
            echo json_encode($r);
        } else {
            $this->echoJSONFailure("id not set or already deleted");
        }
    }

    public function jsonClearLogs()
    {
        // this deletes everything
        if ($this->Log_model->clear())
            $this->echoJSONSuccess();
        else
            $this->echoJSONFailure("error clearing Logs");
    }

    /**
     * log
     * performs parameter parsing for log table, fileUploads table, the session_back table and handles the uploads getting moved to proper
     * directories depending on file type
     *
     * @param
     *            optional jsonMode=true
     * @return string
     */
    public function log($jsonMode = true)
    {
        $o = new stdClass();
        $str = "";
        // var_dump($_POST);
        if (isset($this->session->userdata['session_id'])) {
            $this->Session_model->insert_backup(); // no id from user_session table except session_id
            $o->log_id = $this->Log_model->insert($this->session->userdata['session_id']); // $this->session->userdata['session_id']
            if (is_numeric($o->log_id) && isset($_POST['filecontent'])) { // get's MSG, //echo "log_id:".$log_id;
                $r = $this->do_upload($o->log_id);
            }
            if ($jsonMode)
                $this->echoJSONSuccess("inserted log for session id " . $this->session->userdata['session_id']);
        } else if (isset($_POST['session_id'])) {
            $this->Session_model->insert_backup(); // no id from user_session table except session_id
            $o->log_id = $this->Log_model->insert($_POST['session_id']); // $this->session->userdata['session_id']
            if (is_numeric($o->log_id) && isset($_POST['filecontent'])) { // get's MSG, //echo "log_id:".$log_id;
                $r = $this->do_upload($o->log_id);
            }
            if ($jsonMode)
                $this->echoJSONSuccess("inserted log for session id " . $_POST['session_id']);
        } else {
            if (! isset($_POST['message']))
                $str .= ", message not set!";
            if (! isset($_POST['data']))
                $str .= ", data not set!";
            if ($jsonMode)
                $this->echoJSONFailure($str);
            else
                return "log_id is non numeric";
        }
    }

    public function getLogs()
    {
        // $this->db->join('comments', 'comments.id = blogs.id');
        $o = new stdClass();
        if (isset($_POST['session_id'])) {
            $o->logs = $this->Log_model->LogsFromSession(); // gets variables from post vars
        } else {
            $o->logs = $this->Log_model->getJoinSession4CurrentUser();
        }

        if (is_array($o->logs)) {
            foreach ($o->logs as $log) {
                $log->files = $this->Fileuploads_model->get_entriesByLogID($log->id);
            }
        }
        echo json_encode($o); // {"logs":null}
    }

    public function setData()
    {
        $this->Data_model->insert();
    }

    public function getData()
    {
        // $this->db->join('comments', 'comments.id = blogs.id');
        $o = new stdClass();
        // if( isset($_POST['user_id']) ){
        $o->data = $this->Data_model->get(); // gets variables from post vars
                                             // } else {
                                             // $o->data = $this->Data_model->getJoinSession4CurrentUser();
                                             // }
        echo json_encode($o); // {"logs":null}
    }

    // this is to become the delete request responder
    public function test()
    {
        // $sessions =
        $record = $this->Log_model->getLogByID(59, $this->session->userdata('user_id')); // join doesn't want to work with where
                                                                                         // echo json_encode($record); //
                                                                                         // {"id":"06282978573ec0800229f2e350882b39","message":"Flash Audio Recorder v5","timestamp":"2013-06-19 15:38:50","data":"no data","session_id":"06282978573ec0800229f2e350882b39","user_id":"4","ip_address":"50.152.86.190","user_agent":"Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko\/20100101 Firefox\/21.0","last_activity":"1371681519"}
        echo "Found Record with user " . $record->user_id . '</br>';
        echo "Looking up files . . . </br>";
        $record->files = $this->Fileuploads_model->get_entriesByLogID(59);
        print_r($record->files);
        // now have log,session,files
        if (is_array($record->files)) {
            for ($i = 0; $i < count($record->files); $i ++) {
                // $this->Fileuploads_model->deleteFileName($file_name);
            }
        }
    }
}
