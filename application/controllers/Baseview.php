<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once ('Base.php');

class Baseview extends Base {
//	$this->fileTagNameID = 'FileContent';
//	$this->data = array();
	
	function __construct() {
		parent::__construct();
		$this->data['controller'] = 'Baseview';
		$this->load->library('form_validation');
	}

	public function index() {		
		if ($this -> isValidated()) {
			//$this->data['body'] = 'Welcome ' . $this -> session -> userdata('username');
			switch( $this->session->userdata('type') ) {
				case 0 :
					//guest
					$this->data['view'] = 'User Dashboard';
					$this->load->view('templates/artisteer/userContent', $this->data);
					break;
				case 1 :
					//regular
					$this->data['view'] = 'Dev Dashboard';
					$this->load->view('templates/artisteer/devContent', $this->data);
					break;
				case 2 :
					//admin
					$this->data['view'] = 'Admin Dashboard';
					$this->load->view('templates/artisteer/adminContent', $this->data);
					break;
			}
			
		} else {
			$this->nonValidatedContent();
		}
	}

	public function nonValidatedContent() {
		$this->load->view('templates/artisteer/NonValidatedContent.php', $this->data);
	}

	public function login() {//post: user_name, password or pass
		$this->form_validation->set_rules('email', 'Email', 'required|max_length[32]|valid_email');
		$this->form_validation->set_rules('pass', 'Password', 'required|min_length[5]|max_length[32]');
		if ($this->form_validation-> run() == FALSE) {
			$this->data['error'] = 'Please fill in all fields';
			$this->nonValidatedContent();			
		} else {
			$o = $this -> User_model -> validate_user();			
			if (is_object($o)) {
				//$this->log->write_log ($level= 'error', 'user type: '.$this->session->userdata('type').', name:'.$this->session->set_userdata('username'), FALSE);
				$this -> session -> set_userdata('validated', true);
				$this -> session -> set_userdata('username', $this -> User_model -> user_name);
				$this -> session -> set_userdata('type', $this -> User_model -> type);
				$this -> session -> set_userdata('user_id', $this -> User_model -> id);
				$this -> index();
			} else {
				$this->data['error'] = 'Incorrect email or password!';
				$this -> nonValidatedContent();
			}
		}
	}

	public function signup() {
		$this -> form_validation -> set_rules('email', 'Email', 'required|valid_email');
		$this -> form_validation -> set_rules('emailconf', 'Confirm', 'required|valid_email|matches[email]');

		$this -> form_validation -> set_rules('pass', 'Password', 'required|matches[passconf]');
		$this -> form_validation -> set_rules('passconf', 'Password Confirmation', 'required');

		if ($this -> form_validation -> run() == FALSE) {
			$this->data['view'] = 'signup';
		} else {
		  
		  //insert email into Db and await validation, upon
            if (!$this -> User_model -> user_exists()) {
                $this -> User_model -> insert_user();
            	$this->data['view'] = 'login';
                $this->data['Message'] = 'Sign up was Successful';
				
                $name = "no reply";
                $from = "noreply@mail.wishbone-interactive.com";
                $to = $this->input->post('email');
                $subj = "Verify Email";
                $msg = 'please click the <a href="wishbone-interactive.com/index.php/baseview/validate_email">link</a> to verify your email and continue setting up your account.';
                $this->sendEmail($name, $from, $to, $subj, $msg);
			} else {
				$this->data['view'] = 'signup';
				$this->data['error'] .= 'User name exists';
			}
		}
		$this->data['error'] = validation_errors();
		$this->nonValidatedContent();
	}

	public function upload_form($output = null) {
		$data = array('error' => '', 'fileTagNameID' => $this -> fileTagNameID, 'callBackFunct' => 'base/log');
		$data['content'] = $this -> load -> view('templates/artisteer/uploadForm', $data, true);
		$this -> load -> view('templates/artisteer/Content', $data);
	}
	
	public function validate_email(){
	    $this->User_model->validate_email();
	}
}
