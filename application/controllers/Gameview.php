<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 Add Common Libraries here and everything will extend it 
*/
//ini_set('post_max_size', '32M');
//ini_set('upload_max_filesize', '32M');	

class Gameview extends CI_Controller {
    function __construct(){
        parent::__construct();
		$this->load->helper('form');
        $this->load->helper('url');
		$this->load->helper('base');
		$this->load->helper('path');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('user_agent');

		$this->load->model('Game_model');
		$this->load->model('Games_model');
		$this->load->model('Casinos_model');
	
    }
    
	private function parseUserAgent($aSess) {
		for ($i = 0; $i < count($aSess); $i++) {
			if ($aSess[$i] -> ip_address == '::1') {
				$aSess[$i] -> ip_address = '127.0.0.1';
			}
			$this -> agent -> parseAgent($aSess[$i] -> user_agent);
			//echo $Session->user_agent."\n";
			//$Session->session_id
/*			if ($this -> agent -> is_mobile()) {
				$aSess[$i] -> browser = $this -> agent -> mobile();
				$aSess[$i] -> mobile = true;
			} else if ($this -> agent -> is_browser()) {
				$aSess[$i] -> browser = $this -> agent -> browser();
				$aSess[$i] -> mobile = false;
				//.' '.$this->agent->version();
			} elseif ($this -> agent -> is_robot()) {
				$agent = $this -> agent -> robot();
				$aSess[$i] -> mobile = false;
			} else {
				$aSess[$i] -> browser = 'Unknown';
				$aSess[$i] -> mobile = 'Unknown';
			}*/
			
			$aSess[$i] -> platform = $this -> agent -> platform();
			$aSess[$i] -> browser = $this -> agent -> browser(); 			
			$aSess[$i] -> mobile = false;
			if ($this -> agent -> is_mobile()) $aSess[$i] -> mobile = true;
			//$aSess[$i] -> browser = $this -> agent -> mobile();
		}
	}

	private function createJsonMSG($success, $status, $message="") {
		$r = new stdClass();
		$r->success = $success;
		$r->message = $message;
		$r->status = $status;
		return $r;
	}	
	private function getJsonSuccessObj(){
		return $this->createJsonMSG(true,1,"");
	}
	private function getJsonFailureObj(){
		return $this->createJsonMSG(false,0,"");
	}

	private function echoJSONSuccess($MSG=""){
		//echo '{"success": "true"}';
		$o = $this->getJsonSuccessObj();
		$o->message = $MSG;
		echo json_encode($o);
	}
	private function echoJSONFailure($MSG=""){
		//echo '{"success": "false"}';
		$o = $this->getJsonFailureObj();
		$o->message = $MSG;
		echo json_encode($o);
	}

	public function reset_database(){			
		$this->Game_model->clear();
		$this->Games_model->clear();
		$this->Casinos_model->clear();
		$r = $this->getJsonSuccessObj();
		$r->message = "Successfully cleared Database";
		echo json_encode($r);		
	}
 	
	//game table
	public function newGameData(){
	    $id = $this->Game_model->insert();
	    if($id != false)
	        $this->echoJSONSuccess("added new game");
        else
            $this->echoJSONFailure("gameData insert error");
	}
	
	//returns all timestamps and id's from all games of user logged in
	public function getTimestamps(){
	    $o = new stdClass();
	    $o->data = $this->Game_model->getTimestamps(); //gets variables from post vars
	    echo json_encode($o);  //{"logs":null}
	}
	//returns game with id in post
	public function getGames(){
	    $o = new stdClass();
	    $o->data = $this->Game_model->getGames(); //gets variables from post vars
	    echo json_encode($o);  //{"logs":null}
	}
	
	//returns all games for user
	public function getGame(){
	    $o = new stdClass();
	    $o->data = $this->Game_model->get(); //gets variables from post vars
	    echo json_encode($o);  //{"logs":null}
	}
	
	//games table
	public function newGame(){
	    $config['upload_path']          = './uploads/graphics/';
	    $config['allowed_types']        = 'gif|jpg|png';
	    $config['max_size']             = 32768;
	    //$config['max_width']            = 1024;
	    //$config['max_height']           = 768;
	    
	    $id = $this->Games_model->insert();
	    if($id != false){
	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('thumbnail')){
	            $error = array('error' => $this->upload->display_errors());
	            $this->load->view('game_entry_form', $error);
	        } else {
	            $data = array('upload_data' => $this->upload->data());
	            //$this->load->view('upload_success', $data);
	        }
	       $this->echoJSONSuccess("added new game");
	    } else
	       $this->echoJSONFailure("game insert error");
	}

	public function getGamesFromCasino(){
	    $data = $this->Games_model->getGamesFromCasino(); //gets variables from post vars
	    if($data)
	        echo json_encode($data->result_object);  //{"logs":null}
	    else
	       $this->echoJSONFailure("no games for casino");
	}
	
	public function newCasino(){
	    $config['upload_path']          = './uploads/graphics/';
	    $config['allowed_types']        = 'gif|jpg|png';
	    $config['max_size']             = 32768;
	    //$config['max_width']            = 1024;
	    //$config['max_height']           = 768;
	    
	    $id = $this->Casinos_model->insert(); //gets variables from post vars
	    if($id != false){
	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('thumbnail')){
	            $error = array('error' => $this->upload->display_errors());
	            $this->load->view('casino_entry_form', $error);
	        } else {
	            $data = array('upload_data' => $this->upload->data());
	            $this->load->view('upload_success', $data);
	        }
	        $this->echoJSONSuccess("added new casino");
	    } else
            $this->echoJSONFailure("casino insert error");
	}
	
	public function getCasinos(){
	    $o = new stdClass();
	    $o->data = $this->Casinos_model->getCasinosFromState(); //gets variables from post vars
	    echo json_encode($o->data->result_object);  //{"logs":null}
	}
	
	public function getCasinosFromState(){
	    $o = new stdClass();
	    $o->data = $this->Casinos_model->getCasinosFromState(); //gets variables from post vars
	    if($o->data)
	       echo json_encode($o->data->result_object);  //{"logs":null}
	    else 
	        $this->echoJSONFailure("no casinos for state");
	}
}
