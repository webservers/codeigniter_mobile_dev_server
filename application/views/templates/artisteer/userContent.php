<?php include_once('header.php'); ?>
<?php include_once('userValidatedHeader.php'); ?>

<!-- Menu Code Here -->
<?php include_once('userTopMenu.php'); ?>
<div class="cleared reset-box"></div>
<div class="art-box art-sheet"><div class="art-box-body art-sheet-body"><div class="art-layout-wrapper"><div class="art-content-layout"><div class="art-content-layout-row">

<!-- SideBar Here 5deep-->
<!-- SideBar 1 Start -->
<div class="art-layout-cell art-sidebar1" id="LeftSidebar">
  <!-- Block -->
  <?php include_once('userBlock.php'); ?>
  <!-- VMenu -->
  <?php //include_once('VMenu.php'); ?>
<div class="cleared"></div></div>
<!-- SideBar 1 End -->

<!-- Content Here -->
<!-- Content Start -->
<div class="art-layout-cell art-content"><div class="art-box art-post"><div class="art-box-body art-post-body"><div class="art-post-inner art-article">
    <div class="art-postcontent"><div class="art-content-layout"><div class="art-content-layout-row"><div class="art-layout-cell layout-item-0" style="width: 100%;" id="Content">
        <!-- Page Content Here... 8deep-->
        <h3>Games For Casino</h3>
		<div id="casino_games" class="casino_games">
		</div><br><br>
		<div id="bet_tables" class="bet_tables"></div><br><br>
		<div id="roulette_game" class="hide">
			  <div class="appVarInput">
    			  <label>Percent Win Trigger <input type="text" id="percentTrigger" style="width: 40px;"/></label>
    			  <label style="margin-left: 10px;">Percent Balance per Bet <input type="text" id="percentBalance" style="width: 40px;"/></label>
    			  <label style="margin-left: 10px;">Probability Stop <input type="text" id="probabilityStop" style="width: 60px;"/></label>
			  </div>
              <div id="bet_table" class="bet_table">
                  <div id="roulette_table_container"></div>
                    <div id="table_labels" class="table_labels">
                      <label id="balance"></label>
    	                <br><label id="amountLostSinceLastWin"></label>
       	                <label id="payout"></label>
       	                <label id="betTotal"></label>
    	                <label id="profitOnWin"></label>
    	            </div>
                    <div id="chips_container" class="chips_container"></div>
                    <div class="table_buttons_container">
    			        <input type="button" class="repeat_last_button table_button" id="repeat_last" onClick="repeat_last_bet()"/>
    			        <input type="button" class="twox_button table_button" id="twoX" onClick="twoX()"/>                
    	                <input type="button" class="remove_all_button table_button" id="remove_all" onClick="remove_all_bets(true)"/>			
    			        <input type="button" class="remove_last_button table_button" id="remove_last" onClick="remove_last_bet()"/>
    	                <!--<input type="button" class="spin_button table_button" id="spin"/>-->
    					<input type="button" class="gear_button table_button" id="settings" onClick="enterBalance()"/>
    	                <!-- <input type="button" class="settings_button table_button button_border" id="settings"/> -->
                    </div>
              </div>
            <br><br>
            <div id="stat_app"></div>              
		</div>
    </div></div></div></div><!-- art-postcontent -->
	<div class="cleared"></div></div>
	<div class="cleared"></div></div></div>
	<div class="cleared"></div></div>
<!-- Content End -->

</div></div></div>
<?php include_once('footer.php'); ?>