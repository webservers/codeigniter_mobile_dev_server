<?php include_once('header.php'); ?>
<?php include_once('devValidatedHeader.php'); ?>

<!-- Menu Code Here -->
<?php include_once('TopMenu.php'); ?>
<!-- <center><img src="http://localhost/phoneGap/assets/images/header.png" width="800" height="130"></center> -->
<div class="cleared reset-box"></div>
<div class="art-box art-sheet"><div class="art-box-body art-sheet-body"><div class="art-layout-wrapper"><div class="art-content-layout"><div class="art-content-layout-row">

<!-- SideBar Here -->
<!-- SideBar 1 Start -->
<div class="art-layout-cell art-sidebar1" id="LeftSidebar">
  <!-- Block -->
  <?php include_once('devBlock.php'); ?>
  <!-- VMenu -->
  <?php //include_once('VMenu.php'); ?>
<div class="cleared"></div></div>
<!-- SideBar 1 End -->

<!-- Content Here -->
<!-- Content Start -->
<div class="art-layout-cell art-content"><div class="art-box art-post"><div class="art-box-body art-post-body"><div class="art-post-inner art-article">
    <div class="art-postcontent"><div class="art-content-layout"><div class="art-content-layout-row"><div class="art-layout-cell layout-item-0" style="width: 100%;" id="Content">
        <!-- Page Content Here... -->		
		<?php if(isset($content)) echo $content; ?>
    </div></div></div>
</div><div class="cleared"></div></div><div class="cleared"></div></div></div><div class="cleared"></div></div>
<!-- Content End -->

</div></div></div><div class="cleared"></div>
<?php include_once('footer.php'); ?>