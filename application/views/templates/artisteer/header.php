<?php
	function e($v){ echo $v; }
	//$urlParts = explode('/', $_SERVER['REQUEST_URI']);
	//$baseDir = $urlParts[1];
	$baseDir = "/";
	$ThemeDir = "artx-Blue/";
	$AssetDir = $baseDir."assets/";
	$UploadDir = $baseDir."uploads/"; //$uploadDir from upload.php
	$MobileAppDir = $UploadDir."apps/";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Roulette Assistant</title>
</head>
<body>
<div id="art-page-background-glare-wrapper"><div id="art-page-background-glare"></div></div>
 <div id="art-main"><div class="cleared reset-box"></div>
<link type="text/css" href="<?php e($AssetDir); ?>jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet"/>
<link type="text/css" href="<?php e($AssetDir); ?>css/common.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php e($AssetDir.$ThemeDir); ?>style.css" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" href="<? e($AssetDir.$ThemeDir); ?>style.ie6.css" type="text/css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<? e($AssetDir.$ThemeDir); ?>style.ie7.css" type="text/css" media="screen" /><![endif]-->
<style type="text/css">
   .art-post .layout-item-0 { padding-right: 10px;padding-left: 10px; padding-right: 10px;padding-left: 10px; }
   .ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
   .ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
</style>
<link type="text/css" href="<?php e($AssetDir); ?>css/green_boxes.css" rel="stylesheet"/>

<script type="text/javascript" src="<?php e($AssetDir); ?>jquery-ui-1.12.1/jquery.js"></script>
<script type="text/javascript" src="<?php e($AssetDir); ?>jquery-ui-1.12.1/jquery-ui.js"></script>
<!--<script type="text/javascript" src="<?php e($AssetDir.$ThemeDir); ?>script.js"></script> <!-- artisteer JS -->
<script type="text/javascript">
var AssetDir = "<?php e($AssetDir); ?>";
var ThemeDir = "<?php e($ThemeDir); ?>";
var UploadDir = "<?php e($UploadDir); ?>";
var MobileAppDir = "<?php e($MobileAppDir); ?>";
var SessionID = "0000";
</script>
