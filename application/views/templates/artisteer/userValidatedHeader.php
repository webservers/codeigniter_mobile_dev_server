<script type="text/javascript" src="<?php e($AssetDir); ?>js/helper.js"></script>
<script type="text/javascript" src="<?php e($AssetDir); ?>js/date.js"></script>
<script type="text/javascript" src="<?php e($AssetDir); ?>js/dateformat.js"></script>
<script type="text/javascript" src="<?php e($AssetDir); ?>js/controllerData.js"></script> <!-- needs jQuery to for networking code -->
<script type="text/javascript" src="<?php e($AssetDir); ?>js/gui.js"></script>
<script type="text/javascript" src="<?php e($AssetDir); ?>js/userIndex.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/app.css">
<link rel="stylesheet" type="text/css" href="/assets/css/stat.css">
<!--<script type="text/javascript" src="/assets/jquery.mobile-1.4.5/jquery.min.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="/assets/js/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css" />-->
<script type="text/javascript" src="/assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/jquery-ui-1.12.1/jquery-ui.min.css" />
<!--<script type="text/javascript" src="/assets/js/socketio.js"></script>-->
<script type="text/javascript" src="/assets/canvasjs-3.2.6/canvasjs.min.js"></script>
<script type="text/javascript" src="/assets/js/math.js"></script> 
<script type="text/javascript" src="/assets/js/rand_set.js"></script>
<script type="text/javascript" src="/assets/js/appGui.js"></script>             
<script type="text/javascript" src="/assets/js/appVars.js"></script>
<script type="text/javascript" src="/assets/js/speech.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/table.css">
<script type="text/javascript" src="/assets/js/tableGUI.js"></script>
<script type="text/javascript" src="/assets/js/game.js"></script>
<script type="text/javascript" src="/assets/js/wheel.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/ios-switch/switch.css">
