<?php
/*
game--
data:json
game_id: int
id: int ai
timestamp: timestamp
user_id: int
win_loss: double
amount_wagered: double
{
beginning_balance: double,
ending_balance: double
bets: [{}],
numbers: []
}
 */
class _Game{
    public $game_id = "";
    public $data = "";
    public $user_id = "";
    public $timestamp = null;
    public $win_loss = 0;
    public $amount_wagered = 0;
    public $casino_id = 0;
    function __construct(){
        if(isset($_POST['game_id']))
            $this->game_id = $_POST['game_id'];
        if(isset($_POST['data']))
            $this->data = $_POST['data']; //insert null for data by default
        if(isset($_POST['win_loss']))
            $this->win_loss = $_POST['win_loss']; //insert null for data by default
        if(isset($_POST['amount_wagered']))
            $this->amount_wagered = $_POST['amount_wagered']; //insert null for data by default
        if(isset($_POST['casino_id']))
            $this->casino_id = $_POST['casino_id']; //insert null for data by default
        $date = new DateTime('now'); //date('Y-m-d H:i:s');        
        $this->timestamp = $date->format('Y-m-d H:i:s'); //'2021-09-29 04:01:54' 
        //$date->getTimestamp(); 
    }
}

class Game_model extends CI_Model {   
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    public function formatQueryResult($q, $returnfirstArg=false){
        try{
            //populate $v with result()
            $v = false;
            if(is_object($q))	$v = $q->result();
            else return false;
            
            if(is_array($v) && count($v) > 0){
                if($returnfirstArg){
                    return $v[0];
                } else {
                    return $v;
                }
            }
        } catch( Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function insert(){
        try{
        	$o = new _Game();
        	$o->user_id =  $this->session->userdata('user_id');
            if( !isset($_POST['data']) ) return false;
            if( !isset($_POST['casino_id']) ) return false;
            //var_dump($o);
            $this->db->insert('game', $o);
			return $this->db->insert_id();
            //return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function update(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $this->db->update('game', new _Game(), array('id' => $_POST['id']));
			return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
	public function delete(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('DELETE WHERE `id` = '.$_POST['id']);
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;		
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE Data;');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
    public function get(){
        try{
            if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('SELECT * FROM `game` WHERE `id` = '.$_POST['id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getTimestamps(){
        try{
           // if( !isset($_POST['user_id']) ) return false;
           // $q = $this->db->query('SELECT timestamp, id WHERE `user_id` EQUALS '.$_POST['user_id']);
            $q = $this->db->query('SELECT `timestamp`, `id` FROM `game` WHERE `user_id` = '.$this->session->userdata('user_id'));
            $this->formatQueryResult($q);
            return $q->result_object;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getGames(){
        try{
            $q = $this->db->query('SELECT * FROM `game` WHERE `user_id` = '.$this->session->userdata('user_id'));
            $this->formatQueryResult($q);
            return $q->result_object;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
?>
