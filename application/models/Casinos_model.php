<?php
/*
casino--
data:json
{
name: varchar(30)
website: varchar(80)
jurisdiction: varcar(30)
thumbnail: varchar(30)
}
 */
class _Casino{
    public $id = "";
    public $name = "";
    public $underlying_casino = "";
    public $thumbnail = "";
    function __construct(){
        if(isset($_POST['id']))
            $this->id = $_POST['id'];
        if(isset($_POST['cname']))
            $this->name = $_POST['cname']; //insert null for data by default
        if(isset($_POST['underlying_casino']))
            $this->underlying_casino = $_POST['underlying_casino']; //insert null for data by default
        if(isset($_FILES['thumbnail']))
            $this->thumbnail = $_FILES['thumbnail']['name']; //insert null for data by default
    }
}

class _CasinoState{
    public $state = "";
    public $website = "";
    public $casino_id = "";
    function __construct(){
        if(isset($_POST['state']))
            $this->state = $_POST['state']; //insert null for data by default
        if(isset($_POST['website']))
            $this->website = $_POST['website']; //insert null for data by default
        //if(isset($_FILES['casino_id']))
        //    $this->casino_id = $_POST['casino_id']; //insert null for data by default
    }
}

class Casinos_model extends CI_Model {   
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    public function formatQueryResult($q, $returnfirstArg=false){
        try{
            //populate $v with result()
            $v = false;
            if(is_object($q))	$v = $q->result();
            else return false;
            
            if(is_array($v) && count($v) > 0){
                if($returnfirstArg){
                    return $v[0];
                } else {
                    return $v;
                }
            }
        } catch( Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function insert(){
        try{
        	$o = new _Casino();
            if( !isset($_POST['cname']) ) return false;
            //if( !isset($_POST['state']) ) return false;
            $this->db->insert('casinos', $o);
    
            //add state and website           
            if( isset($_POST['website']) | !isset($_POST['state']) ){
                $o = new _CasinoState();
                $o->casino_id = $this->db->insert_id();
                $this->db->insert('casino_states_available', $o);
            }
			return $this->db->insert_id();
            //return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function update(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $this->db->update('casinos', new _Casino(), array('id' => $_POST['id']));
			return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
	public function delete(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('DELETE WHERE `id` = '.$_POST['id']);
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;		
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE casinos');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
    public function get(){
        try{
            if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('SELECT * WHERE `id` = '.$_POST['id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getCasinos(){
        try{
            $q = $this->db->query('SELECT * FROM casinos');
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getCasinosFromState(){
        try{
            if( !isset($_POST['state']) ) return false;
            $q = $this->db->query('SELECT * FROM `casino_states_available` JOIN `casinos` ON casino_states_available.casino_id = `casinos`.id WHERE casino_states_available.state = "'.$_POST['state'].'"');
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
?>
