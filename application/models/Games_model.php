<?php
/*
games--
id: int ai
name: varchar(60)
table_type: varchar(20)
vendor: varchar(60)
thumbnail: varchar(120)
wheel_type: varchar(60)
freetospin: bool
min_bet: double
chip_values: text
max_bets: text
{
   maxBet: {
    1_1: int, ...
   }
}
 */
class _Games{
    public $name = "";
    public $vendor = "";
    public $table_type = "";
    public $thumbnail = "";
    public $wheel_type = "";
    public $free_to_spin = "";
    public $min_bet = "";
    public $chip_values = "";
    public $max_bet1 = "";
    public $max_bet2 = "";
    public $max_bet5 = "";
    public $max_bet6 = "";
    public $max_bet8 = "";
    public $max_bet11 = "";
    public $max_bet17 = "";
    public $max_bet35 = "";
    public $table_max = 0;
    
    function __construct(){
        if(isset($_POST['cname']))
            $this->name = $_POST['cname'];
        if(isset($_POST['vendor']))
            $this->vendor = $_POST['vendor'];
        if(isset($_POST['table_type']))
            $this->table_type = $_POST['table_type'];
        if(isset($_POST['wheel_type']))
            $this->wheel_type = $_POST['wheel_type'];
        if(isset($_FILES['thumbnail']))
            $this->thumbnail = $_FILES['thumbnail']['name'];
        if(isset($_POST['freetospin']))
            $this->free_to_spin = $_POST['freetospin'];
        if(isset($_POST['min_bet']))
            $this->min_bet = $_POST['min_bet'];
        if(isset($_POST['chip_values']))
            $this->chip_values = $_POST['chip_values'];
        if(isset($_POST['max_bet1']))
            $this->max_bet1 = $_POST['max_bet1'];
        if(isset($_POST['max_bet2']))
            $this->max_bet2 = $_POST['max_bet2'];
        if(isset($_POST['max_bet5']))
            $this->max_bet5 = $_POST['max_bet5'];
        if(isset($_POST['max_bet6']))
            $this->max_bet6 = $_POST['max_bet6'];
        if(isset($_POST['max_bet8']))
            $this->max_bet8 = $_POST['max_bet8'];
        if(isset($_POST['max_bet11']))
            $this->max_bet11 = $_POST['max_bet11'];
        if(isset($_POST['max_bet17']))
            $this->max_bet17 = $_POST['max_bet17'];
        if(isset($_POST['max_bet35']))
            $this->max_bet35 = $_POST['max_bet35'];
        if(isset($_POST['table_max']))
            $this->table_max = $_POST['table_max'];
    }
}

class Games_model extends CI_Model {   
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    public function formatQueryResult($q, $returnfirstArg=false){
        try{
            //populate $v with result()
            $v = false;
            if(is_object($q))	$v = $q->result();
            else return false;
            
            if(is_array($v) && count($v) > 0){
                if($returnfirstArg){
                    return $v[0];
                } else {
                    return $v;
                }
            }
        } catch( Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function insert(){
        try{
        	$o = new _Games();
            $this->db->insert('games', $o);
			return $this->db->insert_id();
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function update(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $this->db->update('games', new _Game(), array('id' => $_POST['id']));
			return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
	public function delete(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('DELETE WHERE `id` EQUALS '.$_POST['id']);
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;		
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE Data;');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
    public function get(){
        try{
            if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('SELECT * WHERE `id` EQUALS '.$_POST['id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getGamesFromCasino(){
        try{
            if( !isset($_POST['casino_id']) ) return false;
            $q = $this->db->query('SELECT * FROM `casino_games_available` JOIN `games` ON casino_games_available.game_id = `games`.id WHERE casino_games_available.casino_id = '.$_POST['casino_id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
?>
