<?php

class _Data{
    public $message = "";
    public $data = "";
    public $user_id = "";
    public $timestamp = null;
    function __construct(){
        if(isset($_POST['message']))
            $this->message = $_POST['message'];
        if(isset($_POST['user_id']))
            $this->user_id = $_POST['user_id'];
        if(isset($_POST['data']))
            $this->data = $_POST['data']; //insert null for data by default
        $date = new DateTime();
        $this->timestamp = $date->getTimestamp();
    }
}

class Data_model extends CI_Model {   
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    public function insert($user_id=false){
        try{
        	$o = new _Data();
        	if($user_id) $o->$user_id = $user_id;
            if( !isset($_POST['data']) ) return false;
            $this->db->insert('data', $o);
			return $this->db->insert_id();
            //return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function update(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $this->db->update('data', new _Data(), array('id' => $_POST['id']));
			return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
	public function delete(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('DELETE WHERE `id` EQUALS '.$_POST['id']);
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;		
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE Data;');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
    public function get(){
        try{
            if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('SELECT * WHERE `id` EQUALS '.$_POST['id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function getTimestamps(){
        try{
            if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('SELECT timestamp WHERE `id` EQUALS '.$_POST['id']);
            $this->formatQueryResult($q);
            return $q;
        } catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
?>
