<?php
//include_once('Session_model.php');
//class Fileuploads_model extends Session_model {   
class Fileuploads_model extends CI_Model {  
   function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    function get_entries(){
        try{
            //$q = $this->db->get('Logs', 10); //last 10  entries
            $q = $this->db->get('FileUploads');
            return $this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    function get_entriesByLogID($log_id){
        try{
            //$q = $this->db->get('Logs', 10); //last 10  entries
            //$q = $this->db->get('FileUploads');
            $q = $this->db->query("SELECT * FROM FileUploads WHERE `log_id` = '".$log_id."'");
            return $this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }	
    function insert($fields){
        try{        	
            $this->db->insert('FileUploads', $fields);
            return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    function getDetailByFileName(){
        try{
        	if(!isset($_POST['file_name'])) return false;
            $q = $this->db->query("SELECT * FROM FileUploads WHERE `file_name` = '".$_POST['file_name']."'");
            return $this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }	
    function deleteFileName($file_name){
        try{
        	if(is_string($file_name)){
				$q = $this->db->delete('FileUploads', array('file_name'=>$file_name ));
				if ($this->db->affected_rows() > 0)	return TRUE;
			}
			return FALSE;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
    function deleteFileID($file_id){
        try{
        	if(is_numeric($file_id)){
				$q = $this->db->delete('FileUploads', array('id'=>$file_id ));
				if ($this->db->affected_rows() > 0)	return TRUE;
			}
			return FALSE;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }	
    function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE FileUploads;');
            //$this->db->query('TRUNCATE TABLE Access;');
            return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
}
?>
