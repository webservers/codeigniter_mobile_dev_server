<?php
class _Log{
	public $message = "";
	public $data = "";
	public $session_id = "";
    function __construct(){
        if(isset($_POST['message']))
        	$this->message = $_POST['message'];
		if(isset($_POST['session_id']))
			$this->session_id = $_POST['session_id'];
//		else if($this->session->userdata['session_id'] != "")
//        	$this->session_id = $this->session->userdata['session_id'];//$_POST['session_id'];//$sID['session_id'];
        if(isset($_POST['data']))
        	$this->data = $_POST['data']; //insert null for data by default
    }
}
class _Access{
    public $ipAddress = null;
    public $userAgent = null;
    public $requestMethod = null;
    function __construct(){   
        $this->ipAddress = $_SERVER['REMOTE_ADDR']; //"192.168.1.1";
        $this->userAgent = $_SERVER['HTTP_USER_AGENT']; //"Mozilla";
        $this->requestMethod = $_SERVER['REQUEST_METHOD']; //"GET";
    }
}

//TODO add Pagination to DB Results...
//TODO add Cacheing to Computed Results, with Expire
include_once('User_model.php');
class Logs_model extends User_model {   
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    public function formatQueryResult($q, $returnfirstArg=false){
        try{
            //populate $v with result()
            $v = false;
            if(is_object($q))	$v = $q->result();
            else return false;
            
            if(is_array($v) && count($v) > 0){
                if($returnfirstArg){
                    return $v[0];
                } else {
                    return $v;
                }
            }
        } catch( Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    public function get_entries(){
        try{
        	$query = $this->db->get_where('Logs', array('id' => $id), $limit, $offset);
            $q = $this->db->get('Logs');
            //return $q->result();
            $this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function insert($session_id=false){
        try{
        	$o = new _Log();
			if($session_id) $o->session_id = $session_id;
            if(!isset($_POST['message']) || !isset($_POST['data'])) return false;
            $this->db->insert('Logs', $o);
			return $this->db->insert_id();
            //return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
    public function update(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $this->db->update('Logs', new _Log(), array('id' => $_POST['id']));
			return true;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }
	public function delete(){
        try{
        	if( !isset($_POST['id']) ) return false;
            $q = $this->db->query('DELETE WHERE `id` EQUALS '.$_POST['id']);
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;		
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE Logs;');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }	
    public function getSession_Ids(){
        try{
            $q = $this->db->query("SELECT DISTINCT `Logs`.session_id FROM `Logs`;");			//get's a list of session id's
            //$q = $this->db->query("SELECT * FROM `Logs` ORDER BY `Logs`.Session_id ASC;");
            return $this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
	public function getJoinSession4CurrentUser(){
		$this->db->select('*');
		$this->db->from('Logs');
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->join('user_sessions', 'Logs.session_id = user_sessions.id');
		return $this->formatQueryResult( $this->db->get() );
		//var_dump($q);
		//$this->db->join('FileUploads', 'Logs.id = FileUploads.log_id');
		//$r = $this->formatQueryResult( $this->db->query("SELECT * FROM `user_sessions` WHERE `session_id` = '".$session_id."'") );
		//echo $q['logs'];
			//foreach($q->logs as $log){
				//echo "n";
				//$session_id = $log->session_id;
				//echo $session_id."\n";
				//$r = $this->formatQueryResult( $this->getSessionEquals($session_id) );
				//echo $r;
			//}		
		//var_dump( $this->formatQueryResult($q) );
	}
	public function getLogByID($_id){
        try{
        	//SELECT * FROM `Logs` JOIN `user_sessions` ON `Logs`.`session_id` = `user_sessions`.`session_id` WHERE `id` = 59;
        	//if(is_numeric($_id)) $id = $_id; else $id = $_POST['id'];
			$this->db->select('*');
			$this->db->from('Logs');
			$this->db->join('user_sessions', 'Logs.session_id = user_sessions.id');
			$this->db->where('Logs.id', $_id);
        	//$q = $this->db->get_where('Logs', array('id' => $_id));
        	$q = $this->db->get();
            return $this->formatQueryResult($q, true);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
	} 
	public function LogsFromSession(){
        try{
        	//SELECT * FROM `Logs` JOIN `user_sessions` ON `Logs`.`session_id` = `user_sessions`.`session_id` WHERE `id` = 59;
        	//if(is_numeric($_id)) $id = $_id; else $id = $_POST['id'];
			$this->db->select('*');
			$this->db->from('Logs');
			$this->db->where('Logs.session_id', $_POST['session_id']);
        	//$q = $this->db->get_where('Logs', array('id' => $_id));
        	$q = $this->db->get();
            return $this->formatQueryResult($q, true);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
	}	
}
?>
