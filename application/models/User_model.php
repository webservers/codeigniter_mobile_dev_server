<?php
//include_once('Session_model.php');
//class User_model extends Session_model {
class User_model extends CI_Model {
    public $user_name = 'guest';
    public $user_password = '';
    public $id = 4;
	public $type = 0;
    public $email_validated = 0;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
		//$this->load->library('encrypt');
		//$this->encrypt->set_cipher(MCRYPT_BLOWFISH);		//http://us2.php.net/manual/en/mcrypt.ciphers.php
		
		if($this->input->post('email'))
			$this->user_name  = $this->input->post('email'); 
		if($this->input->post('pass')){
			//should only do this if the url == login
			//$this->user_password = $this->encrypt->encode($this->input->post('pass'));
			$this->user_password = md5($this->input->post('pass'));
		}
		if($this->input->get('email'))
			$this->user_name  = $this->input->get('email'); // please read the below note			
		if($this->input->get('pass')){
			//should only do this if the url == login
			//$this->user_password = $this->encrypt->encode($this->input->post('password'));
			$this->user_password = md5($this->input->get('pass'));
		}
    }
    function get_user_type($user_id){
		$query = $this->db->get_where('user', array('id'=>$user_id ) )->result();
		if(isset($query[0]->type))
			return $query[0]->type;
		return false;
	}
    function get_userById($user_id){
		$query = $this->db->get_where('user', array('id'=>$user_id ) )->result();
		if(isset($query[0])) return $query[0];
		return false;
	}	
    function get_users(){
        $query = $this->db->get('user');
        $r = $query->result();
		//foreach($r as $t){ $t->user_password = $this->encrypt->decode($t->user_password); }
		return $r;
    }
    function get_user(){
    	//$o = new stdClass();
		if($this->user_name != null){
			//$this->user_name = $this->session->sessiondata['user_name'];
			$query = $this->db->get_where('user', array('user_name'=>$this->user_name ) );
			$r = $query->result();
			if( isset($r[0]) ){
				//$r[0]->user_password = $this->encrypt->decode($r[0]->user_password); //decrypt all passwords
				return $r[0];
			}
		}
		return false;
    }	
	function user_exists(){
		if(!$this->get_user())
			return false;
		return true;
	}
	function validate_user(){
		$o = new stdClass();
		if($this->user_name != null){
			$query = $this->db->get_where('user', array('user_name'=>$this->user_name ) );
			$r = $query->result();
			//if( isset($r[0]) && $this->input->post('pass') == $this->encrypt->decode($r[0]->user_password)){
			if( isset($r[0]) && md5($this->input->post('pass')) == $r[0]->user_password ){
				//$this->type = $r[0]->type;
				//$this->id = $r[0]->id;
				$o->user_name = $this->user_name;
				$o->password = md5($this->input->post('pass'));
				$o->type = $r[0]->type;
				$o->id = $r[0]->id;
				return $o;
			} else	if( isset($r[0]) && md5($this->input->post('password')) == $r[0]->user_password ){
				//$this->type = $r[0]->type;
				//$this->id = $r[0]->id;
				$o->user_name = $this->user_name;				
				$o->password = md5($this->input->post('password'));
				$o->type = $r[0]->type;
				$o->id = $r[0]->id;
				return $o;
			}			
		}	
		return false;
	}
    function insert_user(){
    	if(!isset($this->type)) $this->type = 1;
    	$this->id = null;
        return $this->db->insert('user', $this);
    }

    function update_user_pass(){
		if($this->user_name != null){
			$r = $this->get_user();			
			if( isset($r[0]) ){
				$this->id = $r[0]->id;
				$this->db->update('user', $this, array('id'=>$this->id ));
				return $this->get_user();
			}
		}
		return false;
    }
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE user;');
			$this->formatQueryResult($q);
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }	
	function delete_user(){
		if($this->user_name != null){
			return $this->db->delete('user', array('user_name'=>$this->user_name ));
		}
		return false;
	}

	function signup(){
		$this->insert_user();
	}
	
	function validate_email(){
	    $this->db->update('user', $this, array('email_validated'=> 1 ));
	}
}

?>
