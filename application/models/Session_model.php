<?php

class Session_model extends CI_Model {
	private $session_id = "";
    function __construct(){
		parent::__construct();
                $this->load->library('session');
		if( isset($_POST['session_id']) ){
			$this->session_id = $_POST['session_id'];
		} else if(isset($this->session->userdata['session_id'])){
			$this->session_id = $this->session->userdata['session_id'];
		}				
    }
	public function formatQueryResult($q, $returnfirstArg=false){
		try{
			//populate $v with result()
			$v = false;
			if(is_object($q))	$v = $q->result();
			else return false;
						
			if(is_array($v) && count($v) > 0){
				if($returnfirstArg){
					return $v[0];
				} else {
					return $v;
				}
			}
		} catch( Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function getCiSessionEquals(){
        try{			
			return $this->formatQueryResult( $this->db->query("SELECT * FROM `ci_sessions` WHERE `session_id` = '".$this->session_id."'") );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }		
		
	}
    public function getCiSessions(){
        try{
            return $this->formatQueryResult( $this->db->query('SELECT * FROM ci_sessions;') );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
	public function getSessionEquals($session_id){
        try{        	
			return $this->formatQueryResult( $this->db->query("SELECT * FROM `user_sessions` WHERE `id` = '".$session_id."'") );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }		
		
	}
    public function get_last_ten_Access(){
        try{
            return $this->formatQueryResult( $this->db->get('user_sessions', 10) );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return false;
    }	
    public function getSessions(){
        try{
           return $this->formatQueryResult( $this->db->query('SELECT * FROM user_sessions;') );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }
	public function isBackedUp(){

		try{
			$qr = $this->db->query("SELECT * FROM `user_sessions` WHERE `id` = '".$this->session_id."'");
			if($qr->num_rows == 0)
				return false;
			return true;
		} catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }			
	}	
	public function insert_backup(){
		try{
			if(!$this->isBackedUp()){
				$o = new stdClass();
				$o->id = $this->session_id;
				$o->ip_address = $this->session->userdata['ip_address'];
				$o->user_agent = $this->session->userdata['user_agent'];
				$o->last_activity = $this->session->userdata['last_activity'];
				if(isset($this->session->userdata['user_id']))
					$o->user_id = $this->session->userdata['user_id'];
				else $o->user_id = 4; //guest user
				//var_dump($o);
				return $this->formatQueryResult( $this->db->insert('user_sessions', $o) );
			}
			return false;				
		} catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
	}
    public function clear(){
        try{
            $q = $this->db->query('TRUNCATE TABLE user_sessions;');
			$q = $this->formatQueryResult($q);
            $q1 = $this->db->query('TRUNCATE TABLE ci_sessions;');
			$q1 = $this->formatQueryResult($q1);
			
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		return false;
    }	
}
?>
