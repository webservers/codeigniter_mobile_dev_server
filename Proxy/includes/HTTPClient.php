<?php
/*
var _URL = "http://maps.googleapis.com/maps/api/geocode/json";
    //_URL += "?address="+_address.replace(/(\s)+/g, "+")+',+';
    //_URL += _city.replace(/(\s)+/g, "+")+',+';
    //_URL += _state;
    //_URL += "&sensor=false";

//_URL = "http://maps.googleapis.com/maps/api/geocode/json?address=590+Blandon+Rd,+Fleetwood,+PA&sensor=false";
 */

  
class HTTPClient{
    var $UserAgent = "";
    var $ch = false;
    var $Verbose = true;
    public function UserAgent($ua){
        $this->UserAgent = $ua;
    }
    function __construct(){
        //User Agent Header ---------------------------------------------------
        $this->UserAgent = "Mozilla/5.0 (iPad; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/75";
        //'Googlebot/2.1 (+http://www.google.com/bot.html)';
        /*Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.6) Gecko/20050225 Firefox/1.0.1
        Mozilla/4.0 (compatible; MSIE 5.0; Windows 2000) Opera 6.03 [en]
        Mozilla/5.0 (compatible; Konqueror/3.1-rc3; i686 Linux; 20020515)
        Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1)
        Googlebot/2.1 (+http://www.google.com/bot.html)
		Mozilla/5.0 (iPad; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/75
		Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 
		*/
        
        $this->CurlOpenConnection();
    }
    function __destruct(){
        $this->CurlCloseConnection();
    }    
    private function CurlOpenConnection(){
     if( $this->ch = curl_init() ){
        curl_setopt($this->ch, CURLOPT_VERBOSE, $this->Verbose);    //print debug messages
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->UserAgent);
        return $this->ch;
     }
     else
        echo(" cURL_init error");
    }
    
    function cURL_login($url, $fields){
        curl_setopt($this->ch, CURLOPT_URL, $url);            // set URL and other appropriate options
        curl_setopt ($this->ch, CURLOPT_POSTFIELDS, $fields); // SET POST PARAMETERS : FORM VALUES FOR EACH FIELD
        curl_setopt ($this->ch, CURLOPT_POST, 1);                 // ENABLE HTTP POST
        curl_setopt ($this->ch, CURLOPT_COOKIEJAR, 'cookie.txt'); // IMITATE CLASSIC BROWSER'S BEHAVIOUR : HANDLE COOKIES
        
    # Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
    # not to print out the results of its query.
    # Instead, it will return the results as a string return value
    # from curl_exec() instead of the usual true/false.
        curl_setopt ($this->ch, CURLOPT_RETURNTRANSFER, 1);
    
        $store = curl_exec ($this->ch);       // EXECUTE 1st REQUEST (FORM LOGIN)
    //  $store = curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL); //returns posted url
    //  $store = curl_getinfo($this->ch, CURLINFO_HEADER_OUT);    //returns nothing?
        return $store;
    }
    function cURL_get($url){
        curl_setopt($this->ch, CURLOPT_FAILONERROR, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, FALSE);    //this tells curl to download the returning url
        curl_setopt($this->ch, CURLOPT_MAXREDIRS, 0);     //which might have to be recursive  
        curl_setopt($this->ch, CURLOPT_HEADER, FALSE);    // don't include header
        curl_setopt($this->ch, CURLOPT_HTTPGET, TRUE);    // GET operation
        curl_setopt($this->ch, CURLOPT_URL, $url);    // SET DOWNLOAD URL
        $content = curl_exec ($this->ch);             // EXECUTE (FILE DOWNLOAD) REQUEST
        if($content === false){
            echo 'Curl error: '.curl_error($this->ch)."\n";
            echo "Using Link:".$url;
        }
        return $content;
    }
    function CurlCloseConnection(){
        curl_close ($this->ch);   // CLOSE CURL
        $this->ch = false;
    }
    function cURL_post($url, $fields){
        //curl_setopt ($this->ch, CURLOPT_FOLLOWLOCATION, 1);   //this tells curl to download the returning url
        curl_setopt ($this->ch, CURLOPT_MAXREDIRS, 2);        //which might have to be recursive
        curl_setopt($this->ch, CURLOPT_URL, $url);            // set URL and other appropriate options
        curl_setopt ($this->ch, CURLOPT_POSTFIELDS, $fields); // SET POST PARAMETERS : FORM VALUES FOR EACH FIELD
        curl_setopt ($this->ch, CURLOPT_POST, 1);                 // ENABLE HTTP POST
        curl_setopt ($this->ch, CURLOPT_COOKIEJAR, 'cookie.txt'); // IMITATE CLASSIC BROWSER'S BEHAVIOUR : HANDLE COOKIES
        curl_setopt ($this->ch, CURLOPT_RETURNTRANSFER, 1);   //return results as a string
        $store = curl_exec ($this->ch);
        //curl_close ($this->ch); // CLOSE CURL*/     
        return $store;  
    }
}
?>