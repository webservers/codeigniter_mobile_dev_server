<?php
include_once 'includes/Arguments.php';
$address = getArgument('address');
$city = getArgument('city');
$state = getArgument('state');

if($address && $city && $state){
    include_once 'includes/HTTPClient.php';
    //echo $hc->cURL_get('http://api.appcelerator.net/p/v1/geo?d=f&q=590+blandon+rd,+fleetwood,+PA');
    //echo $hc->cURL_get('http://maps.googleapis.com/maps/api/geocode/json?address=590+Blandon+Rd,+Fleetwood,+PA&sensor=false');
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=';
    $url .= $address.',+';
    $url .= $city.',+';
    $url .= $state;
    $url .= '&sensor=false';
    
    $hc = new HTTPClient();    
    echo $hc->cURL_get($url);
}
else {
    $r = new stdClass();
    $r->success = false;
    echo json_encode($r);
}
?>