<?php
include_once 'includes/HTTPClient.php';
include_once 'includes/Arguments.php';

$Debug = false;
function urldecode_to_array ($url) {
  $ret_ar = array();
 
  if (($pos = strpos($url, '?')) !== false)         // parse only what is after the ?
    $url = substr($url, $pos + 1);
  if (substr($url, 0, 1) == '&')                    // if leading with an amp, skip it
    $url = substr($url, 1);

  $elems_ar = explode('&', $url);                   // get all variables
  for ($i = 0; $i < count($elems_ar); $i++) {
    list($key, $val) = explode('=', $elems_ar[$i]); // split variable name from value
    $ret_ar[urldecode($key)] = urldecode($val);     // store to indexed array
  }

  return $ret_ar;
}
function echoJsonError(){
	if($Debug){
		switch(json_last_error()){
			case JSON_ERROR_CTRL_CHAR:
				echo "Control character error, possibly incorrectly encoded\n";
				break;
			case JSON_ERROR_DEPTH:
				echo "The maximum stack depth has been exceeded\n";
				break;
			case JSON_ERROR_NONE:
				echo "No error has occurred\n";
				break;
			case JSON_ERROR_STATE_MISMATCH:
				echo "Invalid or malformed JSON\n";
				break;
			case JSON_ERROR_SYNTAX:
				echo "Syntax error";
				break;
			case JSON_ERROR_UTF8:
				echo "Malformed UTF-8 characters, possibly incorrectly encoded\n";
				break;
		}
	}
}
function jsonMSG($success, $message){
	    $r = new stdClass();
	    $r->success = $success;
		$r->message = $message;
	    echo json_encode($r);	
}

function _urlEncode($params) {
	$url = "";
	try{
		if (gettype($params) == 'string') {
			$url .= $params;
		} else if (gettype($params) == 'object') {
			$url .= '?';
			foreach ($params as $key => $param) {
				if(gettype($param) == 'object'){					
					$url .= $key . '=' . json_encode($param) . '&';
				} else {
					$url .= $key . '=' . $param . '&';
				}
			}
			$url = substr($url, 0, strlen($url) - 1);
		}
		return $url;
	} catch(Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}


function ACK(){
	//Get all parameters and form jsonString With It
	$Data = null;
	$jsonParams = "";
	//echo "dumping args";
	foreach($_POST as $key=>$arg){
		echo("key: ".$key.", Type: ".gettype($arg).", Value: ".$arg."\n");
		//if($key == 'base64json')	echo(base64_decode($arg)); //gives spades and nonASCII chars
		//var_dump($arg);
	}
	foreach($_GET as $key=>$arg){
		echo("key: ".$key.", Type: ".gettype($arg).", Value: ".$arg."\n");
		//var_dump($arg);
	}
	//echo 'Request Worked<html>';
}
ACK();



?>