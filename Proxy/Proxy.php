<?php
include_once 'includes/HTTPClient.php';
include_once 'includes/Arguments.php';
//$address = getArgument('address');
/* Needs to be able to set url, and copy parameters
 * either from Get or Post
 * and set the user Agent.

 * //Ti wrapper implementtion
 * Ajax.Send({
	method : 'POST',
	params : {
		sessionId : session.sessionId,
		userId : session.userId,
		apiKey : session.apiKey,
	    data: JSON.stringify(BlankData)
	},
	cache : false,
	timeout : 10000,
	url : "http://bluemountaintechnologies.com/BMTmobile/Proxy/jsonUploader.php",
	onDone : function(j) {//got the json response
	},
	onFail : function(x, eObj) {

	},
	debug : false
});
 
 //Jquery test
 $.ajax({
	 dataType: 'text',
	 url: "http://localhost/phoneGap/proxy/proxy.php",
	 cache: false,
	 type: 'GET',
	 async: true,
	 data: 'json='+JSON.stringify({
		 "url": "http//finance.yahoo.com",
		 "method": 'GET',
		 "userAgent": 'Mozilla',
		 "params": "s=BAC"
	 }),
	 timeOut: 1,
	 error: function(jqXHR, textStatus, errorThrown){
	 //XHR_errorHandler(jqXHR, textStatus, errorThrown);
	 },
	 success: function(data, textStatus, jqXHR){
	 console.log("success: "+textStatus+", data: "+data);
	 }
 });
 */
function httpError(){
  $HttpStatus = $_SERVER["REDIRECT_STATUS"] ;
  if($HttpStatus==200) {print "Document has been processed and sent to you.";}
  if($HttpStatus==400) {print "Bad HTTP request ";}
  if($HttpStatus==401) {print "Unauthorized - Iinvalid password";}
  if($HttpStatus==403) {print "Forbidden";}
  if($HttpStatus==500) {print "Internal Server Error";}
  if($HttpStatus==418) {print "I'm a teapot! - This is a real value, defined in 1998";}	
}
 
function echoJsonError() {
	switch(json_last_error()) {
		case JSON_ERROR_CTRL_CHAR :
			echo "Control character error, possibly incorrectly encoded\n";
			break;
		case JSON_ERROR_DEPTH :
			echo "The maximum stack depth has been exceeded\n";
			break;
		case JSON_ERROR_NONE :
			echo "No error has occurred\n";
			break;
		case JSON_ERROR_STATE_MISMATCH :
			echo "Invalid or malformed JSON\n";
			break;
		case JSON_ERROR_SYNTAX :
			echo "Syntax error";
			break;
		case JSON_ERROR_UTF8 :
			echo "Malformed UTF-8 characters, possibly incorrectly encoded\n";
			break;
	}
}

//urldecode()
//urlencode($str)
//htmlentities()
//rawurlencode() //RFC3986
//rawurldecode()
/* have to convert from thisServerArgs to thatServerArgs
 * via URL parameters??? it works for JSON.stringify and passing in 
 * object literal taken care of by json_encode
 */
function _urlEncode($params) {
	$url = "";
	try{
		if (gettype($params) == 'string') { //passed in string, means already url encoded
			$url .= $params;
		} else if (gettype($params) == 'object') { //object needs to be stringified
			$url .= '?';
			foreach ($params as $key => $param) {
				if(gettype($param) == 'object'){					
					$url .= $key . '=' . json_encode($param) . '&';
				} else {
					$url .= $key . '=' . $param . '&';
				}
			}
			$url = substr($url, 0, strlen($url) - 1);
		}
		return $url;
	} catch(Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}
$request = new stdClass();
$request->arguments = getArgument('json');
	$_json = json_decode(stripcslashes($request->arguments));
	if($_json === null){
		//echoJsonError(); //Syntax error '?message="test"'   '{message: "msg1", data: "test"}'
		$_json = json_decode($request->arguments);
		if($_json === null){
			//echoJsonError();
			die('{"success":false, "message":"json Param not set"}');
		}
		//var_dump($_json);
		//var_dump($request->arguments);
		//var_dump($request->arguments->params);
	}

$params = '';
if(isset($_json->params)){
	if(is_string($_json->params))
		$params = $_json->params;
	else if (strtolower($_json->method) == 'get') {
		$params = _urlEncode($_json->params);
	} else {
		$params = _urlEncode($_json->params);
		$params = substr(_urlEncode($_json->params), 1); //remove the ? for post
	}
}


function ACK_ScriptParams(){
	//Get all parameters and form jsonString With It
	$Data = null;
	$jsonParams = "";
	echo "dumping args";
	foreach($_POST as $key=>$arg){
		echo("key: ".$key.", Value: ".$arg);
		//if($key == 'base64json')	echo(base64_decode($arg)); //gives spades and nonASCII chars
		//var_dump($arg);
	}
	foreach($_GET as $key=>$arg){
		echo("key: ".$key.", Value: ".$arg);
		//var_dump($arg);
	}
	echo 'Request Worked<html>';
}
function ACK_jsonProxyParams(){
	//Get all parameters and form jsonString With It
	global $params;
	echo("type: ".gettype($params).", value: ".$params);
}

function urldecode_to_array($url) {
	$ret_ar = array();

	if (($pos = strpos($url, '?')) !== false)// parse only what is after the ?
		$url = substr($url, $pos + 1);
	if (substr($url, 0, 1) == '&')// if leading with an amp, skip it
		$url = substr($url, 1);

	$elems_ar = explode('&', $url);
	// get all variables
	for ($i = 0; $i < count($elems_ar); $i++) {
		list($key, $val) = explode('=', $elems_ar[$i]);
		// split variable name from value
		$ret_ar[urldecode($key)] = urldecode($val);
		// store to indexed array
	}

	return $ret_ar;
}

/* create a default message for success/fail
 * 
 */
function jsonMSG($success, $message) {
	$r = new stdClass();
	$r->success = $success;
	$r->message = $message;
	echo json_encode($r);
}

function doProxyRequest(){
	global $request;
	global $_json;
	global $params;
	if ( isset($_json->debug) && $_json->debug) {
		if ( isset($_json->url)) {
			echo 'url:' . $_json->url;
			if ( isset($_json->userAgent)) {
				echo 'Set User Agent' . $_json->userAgent;
			}
			if (isset($_json->params))	var_dump($_json->params);
	
			if (strtolower($_json->method) == 'get') {
				echo 'GET:';
			} else {
				echo 'POST:';
			}
		}
	}	
	$hc = new HTTPClient();
	if ($_json == NULL || $_json === false) {
		//echoJsonError();
	} else {
		if ( isset($_json->url)) {
			if ( isset($_json->userAgent)) {
				$hc->UserAgent($_json->userAgent);
			}
			if (strtolower($_json->method) == 'get') {
				if (isset($_json->params)) {
					$_json->url .= $params;
				}
				echo $hc->cURL_get($_json->url);
			} else {
				echo $hc->cURL_post($_json->url, $params);
			}
		} else {
			jsonMSG(false, "parameters not set");
		}
	}
}

//ACK_jsonProxyParams();
doProxyRequest();
?>
